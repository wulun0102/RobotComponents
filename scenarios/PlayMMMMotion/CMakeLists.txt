set(SCENARIO_CONFIG_COMPONENTS
    config/MMMPlayerApp.cfg
    )

# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario_from_configs("MMMScenario" "${SCENARIO_CONFIG_COMPONENTS}" "config/global.cfg")
