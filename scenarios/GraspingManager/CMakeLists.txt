# Add your components below as shown in the following example:
#
# set(SCENARIO_COMPONENTS
#    ConditionHandler
#    Observer
#    PickAndPlaceComponent)

#set(SCENARIO_COMPONENTS
#    )

# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario("GraspingManager" "${SCENARIO_COMPONENTS}")

set(SCENARIO_CONFIGS

    config/GraspingManagerApp.cfg
    config/SimpleGraspGeneratorApp.cfg
    config/GraspSelectionManager.cfg
    config/SimpleRobotPlacementApp.cfg
    config/MotionPlanningServerApp.cfg
    config/GraspingManagerTestApp.cfg
)

# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario_from_configs("GraspingManager" "${SCENARIO_CONFIGS}")
