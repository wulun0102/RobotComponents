armarx_component_set_name("SimpleGraspGeneratorApp")

set(COMPONENT_LIBS
    SimpleGraspGenerator
)


set(EXE_SOURCE main.cpp SimpleGraspGeneratorApp.h)

armarx_add_component_executable("${EXE_SOURCE}")
