/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::GraspingManagerTest
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <RobotComponents/components/GraspingManager/GraspingManager.h>
#include <RobotAPI/libraries/core/LinkedPose.h>
#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>
#include <ArmarXCore/util/json/JSONObject.h>

namespace armarx
{
    class GraspingManagerTestPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        GraspingManagerTestPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("objectToGrasp", "", "the object for which the grasp should be planned");
        }
    };

    /**
     * @class GraspingManagerTest
     * @brief A brief description
     *
     * Detailed Description
     */
    class GraspingManagerTest :
        virtual public armarx::ManagedIceObject,
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "GraspingManagerTest";
        }

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new GraspingManagerTestPropertyDefinitions(
                    getConfigIdentifier()));
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override
        {
            usingProxy("GraspingManager");
            usingProxy("WorkingMemory");
            usingProxy("PriorKnowledge");
            usingProxy("RobotStateComponent");
            srand(IceUtil::Time::now().toMicroSeconds());
        }

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override
        {
            std::string objectName = getProperty<std::string>("objectToGrasp").getValue();
            wm = getProxy<memoryx::WorkingMemoryInterfacePrx>("WorkingMemory");
            RobotStateComponentInterfacePrx robot = getProxy<RobotStateComponentInterfacePrx>("RobotStateComponent");
            while (true)
            {
                auto object = wm->getObjectInstancesSegment()->getEntityByName(objectName);
                //                Eigen::Quaternionf q(
                //                    0.5174515247344971,
                //                    -0.5286158323287964,
                //                    0.4808708131313324,
                //                    -0.4707148373126984);
                //                Eigen::Vector3f pos(4130.22705078125,
                //                                    7142.34765625,
                //                                    1170.102416992188);
                //                pos(0) += (rand() % 1000) - 500;
                //                pos(1) += (rand() % 1000) - 500;
                //                //            pos(2) += (rand() % 1500) - 1000;
                //                if (object)
                //                {
                //
                //                    wm->getObjectInstancesSegment()->removeEntity(object->getId());
                //                }
                //
                //                wm->getObjectInstancesSegment()->addObjectInstance(objectName, objectName,
                //                        new LinkedPose(q.toRotationMatrix(),
                //                                       pos, GlobalFrame, robot->getSynchronizedRobot()),
                //                        new memoryx::MotionModelStaticObject(robot));


                prior = getProxy<memoryx::PriorKnowledgeInterfacePrx>("PriorKnowledge");
                getProxy(gm, "GraspingManager");

                ARMARX_INFO << "initializing object";
                object = wm->getObjectInstancesSegment()->getEntityByName(objectName);
                ARMARX_CHECK_EXPRESSION(object);

                ARMARX_INFO << "object initialized, generating grasping trajectory";
                auto result = gm->generateGraspingTrajectory(object->getId());
                sleep(2);
                //
                ARMARX_INFO << "Got result:";
                ARMARX_INFO << "=== Pose Trajectory ===";
                JSONObjectPtr poseTraj = new JSONObject();
                poseTraj->setVariant("posetrajectory", new Variant(result.poseTrajectory));
                ARMARX_INFO << "Pose Trajectory: " << poseTraj->asString(true);
                //                TrajectoryPtr platformTraj = TrajectoryPtr ::dynamicCast(result.poseTrajectory);
                //                for (const Trajectory::TrajData& point : *platformTraj)
                //                {
                //                    ARMARX_INFO << "ts " << point.getTimestamp() << ": ";
                //                    ARMARX_INFO << VAROUT(point.getPositions());
                //                }

                ARMARX_INFO << "=== Joint Trajectory ===";
                JSONObjectPtr jointTraj = new JSONObject();
                jointTraj->setVariant("jointtrajectory", new Variant(result.poseTrajectory));
                ARMARX_INFO << "Joint Trajectory: " << jointTraj->asString(true);
                //                TrajectoryPtr jointTraj = TrajectoryPtr ::dynamicCast(result.configTrajectory);
                //                for (const Trajectory::TrajData& point : *jointTraj)
                //                {
                //                    ARMARX_INFO << "ts " << point.getTimestamp() << ": ";
                //                    ARMARX_INFO << point.getPositions();
                //                }
            }
        }

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override
        {

        }

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override
        {

        }

    private:
        memoryx::PriorKnowledgeInterfacePrx prior;
        memoryx::WorkingMemoryInterfacePrx wm;
        GraspingManagerInterfacePrx gm;
    };
}

