/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::GraspingManager::GraspingManagerRemoteStateOfferer
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspingManagerRemoteStateOfferer.h"

using namespace armarx;
using namespace GraspingManager;

// DO NOT EDIT NEXT LINE
GraspingManagerRemoteStateOfferer::SubClassRegistry GraspingManagerRemoteStateOfferer::Registry(GraspingManagerRemoteStateOfferer::GetName(), &GraspingManagerRemoteStateOfferer::CreateInstance);



GraspingManagerRemoteStateOfferer::GraspingManagerRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < GraspingManagerStatechartContext > (reader)
{
}

void GraspingManagerRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void GraspingManagerRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void GraspingManagerRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string GraspingManagerRemoteStateOfferer::GetName()
{
    return "GraspingManagerRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr GraspingManagerRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new GraspingManagerRemoteStateOfferer(reader));
}



