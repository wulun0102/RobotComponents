armarx_component_set_name("GraspingManager")

set(COMPONENT_LIBS
    ArmarXCoreInterfaces ArmarXCore ArmarXCoreStatechart ArmarXCoreObservers)

# Sources

set(SOURCES
GraspingManagerRemoteStateOfferer.cpp
./GraspingPipeline.cpp
./GraspGenerator.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)

set(HEADERS
GraspingManagerRemoteStateOfferer.h
GraspingManager.scgxml
./GraspingPipeline.h
./GraspGenerator.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
./GraspingPipeline.xml
./GraspGenerator.xml
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.xml
)

armarx_add_component("${SOURCES}" "${HEADERS}")
