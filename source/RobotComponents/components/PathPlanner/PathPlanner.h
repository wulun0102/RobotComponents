/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents::PathPlanner
* @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
* @date       2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <RobotComponents/interface/components/PathPlanner.h>

#include <MemoryX/core/entity/AbstractEntityWrapper.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>

#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/Robot.h>

//include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

namespace armarx
{
    /**
     * @brief Holds properties for PathPlanner.
     * @see PathPlanner
     */
    class PathPlannerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PathPlannerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory component");
            //            defineOptionalProperty<std::string>("DebugDrawerName", "DebugDrawerUpdates", "Name of the DebugDrawer component");
        }
    };


    /**
     * @defgroup Component-PathPlanner PathPlanner
     * @ingroup RobotComponents-Components
     * @brief Abstract base class for path planners.
     * This class offers basic methods for planning paths in the plane.
     * A path consists of a sequence of 3D vectors containing position (mm) and orientation around the z axis (rad).
     * In addition object and agent management is implemented here.
     *
     * A reference implementation of the A* path planner can be found here: \ref armarx::AStarPathPlanner
     */

    /**
     * @ingroup Component-PathPlanner
     * @brief The PathPlanner class
     */
    class PathPlanner:
        virtual public Component,
        virtual public PathPlannerBase
    {
    public:
        PathPlanner();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr
            {
                new PathPlannerPropertyDefinitions{getConfigIdentifier()}
            };
        }

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        //from slice interface PathPlannerBase
        void setCollisionObjects(const ::armarx::ObjectPositionBaseList& list, const ::Ice::Current& = Ice::emptyCurrent) override;
        void addCollisionObjects(const ::armarx::ObjectPositionBaseList& list, const ::Ice::Current& = Ice::emptyCurrent) override;
        void clearCollisionObjects(const ::Ice::Current& = Ice::emptyCurrent) override;
        void setAgent(const ::memoryx::AgentInstanceBasePtr& newAgent, const std::string& agentColModelName, const ::Ice::Current& = Ice::emptyCurrent) override;
        void setSafetyMargin(::Ice::Float margin, const ::Ice::Current& = Ice::emptyCurrent) override;


        ::armarx::Vector3BaseList getPath(const ::armarx::Vector3BasePtr&, const ::armarx::Vector3BasePtr&, const ::Ice::Current& = Ice::emptyCurrent) const override = 0;

        bool isPositionValid(armarx::Vector3 position) const;

    protected:
        VirtualRobot::RobotPtr agent;
        VirtualRobot::CollisionModelPtr agentCollisionModel;
        float agentZCoord;

        float safetyMargin;

        memoryx::WorkingMemoryInterfacePrx workingMemoryPrx;
        memoryx::CommonStorageInterfacePrx commonStoragePrx;
        memoryx::GridFileManagerPtr fileManager;

        struct CollisionObjectData
        {
            memoryx::ObjectClassBasePtr object;
            VirtualRobot::CollisionModelPtr colModel;
        };

        std::vector<CollisionObjectData> objects;

        //        armarx::DebugDrawerInterfacePrx debugDrawer;
    };

    using PathPlannerPtr = ::IceInternal::Handle< ::armarx::PathPlanner>;

}

