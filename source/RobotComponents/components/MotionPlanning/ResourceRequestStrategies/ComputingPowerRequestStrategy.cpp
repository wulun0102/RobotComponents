/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "ComputingPowerRequestStrategy.h"

namespace armarx
{
    bool cprs::And::shouldAllocateComputingPower(const Ice::Current&)
    {
        return std::all_of(requestStrategies.begin(), requestStrategies.end(),
                           [](ComputingPowerRequestStrategyBasePtr & s)
        {
            return s->shouldAllocateComputingPower();
        }
                          );
    }

    void cprs::ElapsedTime::allocatedComputingPower(const Ice::Current&)
    {
        if (skipping)
        {
            allocatedLastTime = Clock::now();
        }
        else
        {
            allocatedLastTime += std::chrono::seconds {timeDeltaInSeconds};
        }
    }

    void cprs::TotalNodeCount::allocatedComputingPower(const Ice::Current&)
    {
        if (skipping)
        {
            allocateNextTime = nodeCount + nodeCountDelta;
        }
        else
        {
            allocateNextTime += nodeCountDelta;
        }
    }

    void cprs::TaskStatus::updateNodeCount(Ice::Long count, const Ice::Current&)
    {
        for (auto& strat : strategyPerTaskStatus)
        {
            strat.second->updateNodeCount(count);
        }
    }

    void cprs::TaskStatus::updateTaskStatus(armarx::TaskStatus::Status newStatus, const Ice::Current&)
    {
        const auto it = strategyPerTaskStatus.find(newStatus);
        current = (it == strategyPerTaskStatus.end()) ? nullptr : it->second.get();

        for (auto& strat : strategyPerTaskStatus)
        {
            strat.second->updateTaskStatus(newStatus);
        }
    }

    void cprs::TaskStatus::allocatedComputingPower(const Ice::Current&)
    {
        for (auto& strat : strategyPerTaskStatus)
        {
            strat.second->allocatedComputingPower();
        }
    }

    void cprs::TaskStatus::updateNodeCreations(Ice::Long nodesCreated, Ice::Long tries, const Ice::Current&)
    {
        for (auto& strat : strategyPerTaskStatus)
        {
            strat.second->updateNodeCreations(nodesCreated, tries);
        }
    }

    void cprs::NoNodeCreated::updateNodeCreations(Ice::Long nodesCreated, Ice::Long tries, const Ice::Current&)
    {
        for (std::size_t i = 0; i < static_cast<std::size_t>(tries); ++i)
        {
            backlog.at((currentBacklogIndex + i) % backlogSize) = i < static_cast<std::size_t>(nodesCreated) ? 0 : 1;
        }

        currentBacklogIndex = (currentBacklogIndex + tries) % backlogSize;
    }

    void cprs::NoNodeCreated::allocatedComputingPower(const Ice::Current&)
    {
        const auto perc = std::accumulate(backlog.begin(), backlog.end(), 1.f) / backlog.size();
        const auto usedTimeDelta = timeDeltaInSeconds / (1.f + sigma * perc);
        ARMARX_CHECK_EXPRESSION(usedTimeDelta >= 0);

        if (skipping)
        {
            allocatedLastTime = Clock::now();
        }
        else
        {
            allocatedLastTime += std::chrono::seconds {static_cast<std::size_t>(usedTimeDelta)};
        }
    }

    void cprs::CompoundedRequestStrategy::setCurrentStateAsInitialState(const Ice::Current&)
    {
        for (auto& s : requestStrategies)
        {
            s->setCurrentStateAsInitialState();
        }
    }

    void cprs::CompoundedRequestStrategy::updateNodeCount(Ice::Long count, const Ice::Current&)
    {
        for (auto& s : requestStrategies)
        {
            s->updateNodeCount(count);
        }
    }

    void cprs::CompoundedRequestStrategy::updateTaskStatus(armarx::TaskStatus::Status newStatus, const Ice::Current&)
    {
        for (auto& s : requestStrategies)
        {
            s->updateTaskStatus(newStatus);
        }
    }

    void cprs::CompoundedRequestStrategy::allocatedComputingPower(const Ice::Current&)
    {
        for (auto& s : requestStrategies)
        {
            s->allocatedComputingPower();
        }
    }

    void cprs::CompoundedRequestStrategy::updateNodeCreations(Ice::Long nodesCreated, Ice::Long tries, const Ice::Current&)
    {
        for (auto& s : requestStrategies)
        {
            s->updateNodeCreations(nodesCreated, tries);
        }
    }

}
