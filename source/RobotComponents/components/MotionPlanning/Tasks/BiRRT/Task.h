/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/components/MotionPlanning/Tasks/MotionPlanningTask.h>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/BiRRT/Task.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>

namespace armarx::birrt
{
    class Task : public virtual MotionPlanningTaskWithDefaultMembers,
        public virtual TaskBase
    {
    public:
        Task(const CSpaceBasePtr& cspace,
             const VectorXf& startCfg,
             const VectorXf& goalCfg,
             const std::string& taskName = "BiRRTTask",
             //general
             Ice::Long maximalPlanningTimeInSeconds = 300,
             float dcdStep = 0.01f);

        // MotionPlanningTaskControlInterface interface
    public:
        void abortTask(const::Ice::Current&) override;
        Path getPath(const::Ice::Current&) const override;

        // MotionPlanningTaskBase interface
    public:
        void run(const RemoteObjectNodePrxList&, const::Ice::Current&) override;

        SimoxCSpacePtr getOriginalCSpace() const;

    protected:

        Task() = default;

        mutable std::mutex mtx;
        Path solution = {{}, "BiRRT-Path"};
        std::atomic_bool taskIsAborted {false};
    private:
        template<class Base, class Derived> friend class ::armarx::GenericFactory;

    };

}

namespace armarx
{
    using BiRRTTask = birrt::Task;
    using BiRRTTaskPtr = IceUtil::Handle<BiRRTTask>;
    using BiRRTTaskHandle = RemoteHandle<MotionPlanningTaskControlInterfacePrx>;

}
