/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/DataStructures.h>
#include "../../util/HashingUtil.h"

ARMARX_OVERLOAD_STD_HASH((armarx::addirrtstar::NodeId), (arg.workerId, arg.numberOfNode))

namespace armarx::addirrtstar
{
    static const std::string ADDIRRTSTAR_TREE_UPDATE_TOPIC_NAME = "ADDIRRTStarTreeUpdateTopic";

    /**
    * @brief Returns the update's worker id.
    * @param u The update.
    * @return The update's worker id.
    */
    template<class T = std::size_t>
    T getUpdatesWorkerId(const Update& u)
    {
        ARMARX_CHECK_EXPRESSION(u.workerId >= 0);
        return static_cast<std::size_t>(u.workerId);
    }

    /**
    * @brief Returns the update's sub id's predecessor (number of update for its worker -2).
    * @param u The update.
    * @return The update's sub id's predecessor.
    */
    template<class T = std::size_t>
    T getUpdatesPreviousUpdateSubId(const Update& u)
    {
        const auto workerId = getUpdatesWorkerId(u);
        ARMARX_CHECK_EXPRESSION(workerId < u.dependetOnUpdateIds.size());
        return static_cast<T>(u.dependetOnUpdateIds.at(workerId));
    }

    /**
    * @brief Returns the update's sub id (number of update for its worker -1).
    * @param u The update.
    * @return The update's sub id.
    */
    template<class T = std::size_t>
    T getUpdatesSubId(const Update& u)
    {
        return getUpdatesPreviousUpdateSubId(u) + 1;
    }

    /**
    * @brief Returns whether the update update depends on dependency.
    * @param update The update.
    * @param dependency The dependency to check.
    * @return Whether the update update depends on dependency.
    */
    inline bool updateDependsOn(const Update& update, const Update& dependency)
    {
        return update.dependetOnUpdateIds.at(getUpdatesWorkerId(dependency)) >= getUpdatesSubId<Ice::Long>(dependency);
    }

    /**
    * @brief Generates standard add parameters from a dcd stepsize.
    * @param dcdStepsize The dcd stepsize.
    * @return The add parameters.
    */
    inline AdaptiveDynamicDomainParameters generateADDParamsFromDCDStepsize(float dcdStepsize)
    {
        AdaptiveDynamicDomainParameters result;
        result.alpha = 0.05f;
        result.initialBorderRadius = 20 * dcdStepsize;
        result.minimalRadius = 5 * dcdStepsize;
        return result;
    }

    /**
    * @brief Returns a node id as string.
    * @param id The node id.
    * @return The node id as string.
    */
    std::string nodeId2String(const NodeId& id);
}

