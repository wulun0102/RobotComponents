/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/interface/components/MotionPlanning/Tasks/CPRSAwareMotionPlanningTask.h>

#include "MotionPlanningTask.h"

namespace armarx::cprs
{
    class CPRSAwareMotionPlanningTask;
    /**
     * @brief An ice handel for a CPRSAwarePlanningTask.
     */
    using CPRSAwareMotionPlanningTaskPtr = IceInternal::Handle<CPRSAwareMotionPlanningTask>;

    /**
     * @brief Implementation of the slice interface CPRSAwarePlanningTaskBase.
     */
    class CPRSAwareMotionPlanningTask:
        virtual public MotionPlanningTaskWithDefaultMembers,
        virtual public CPRSAwareMotionPlanningTaskBase
    {
    public:
        /**
         * @brief ctor
         * @param startCfg the start point
         * @param cspace the planning cspace
         * @param dcdStep the dcd step size
         * @param maximalPlanningTimeInSeconds the maximal time in seconds
         * @param planningComputingPowerRequestStrategy the used cprs
         */
        CPRSAwareMotionPlanningTask(
            const VectorXf& startCfg,
            const VectorXf& goalCfg,
            const CSpaceBasePtr& cspace,
            Ice::Float dcdStep,
            Ice::Long maximalPlanningTimeInSeconds,
            const cprs::ComputingPowerRequestStrategyBasePtr& planningComputingPowerRequestStrategy,
            const std::string& taskName
        ):
            MotionPlanningTaskBase(taskName),
            MotionPlanningTaskWithDefaultMembers(startCfg, goalCfg, cspace, dcdStep, maximalPlanningTimeInSeconds, taskName),
            CPRSAwareMotionPlanningTaskBase(
                taskName, startCfg, goalCfg, cspace, dcdStep, maximalPlanningTimeInSeconds,
                planningComputingPowerRequestStrategy)
        {
        }

    protected:
        /**
         * @brief ctor used for object factories
         */
        CPRSAwareMotionPlanningTask() = default;
    };
}

