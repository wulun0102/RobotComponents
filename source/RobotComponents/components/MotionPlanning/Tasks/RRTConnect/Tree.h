/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <deque>
#include <vector>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/RRTConnect/DataStructures.h>

namespace armarx::rrtconnect
{
    struct NodeType
    {
        using ConfigType = VectorXf;

        NodeType(ConfigType cfg, NodeId parent):
            cfg {std::move(cfg)},
            parent(parent)
        {}

        ConfigType cfg;
        NodeId parent;
    };

    class Tree
    {
    public:
        using ConfigType = VectorXf;

        Tree(): nodes(1)
        {
        }

        Tree(std::size_t maximalWorkerCount): nodes(maximalWorkerCount)
        {
        }

        /**
         * @brief Sets the maximal worker count to the given value.
         * This value has to be set before the tree's useage. and should never be set twice.
         * @param count The new count.
         */
        void setMaximalWorkerCount(std::size_t count)
        {
            nodes.resize(count);
        }

        void setRoot(const ConfigType& root);

        const std::deque<std::deque<NodeType>>& getNodes() const
        {
            return nodes;
        }

        std::vector<ConfigType> getReversedPathTo(NodeId nodeId) const;
        std::vector<ConfigType> getPathTo(const NodeId& nodeId) const;
        const NodeType& getNode(const NodeId& nodeId) const
        {
            ARMARX_CHECK_EXPRESSION(static_cast<std::size_t>(nodeId.workerId) < nodes.size());
            ARMARX_CHECK_EXPRESSION(static_cast<std::size_t>(nodeId.nodeSubId) < nodes.at(nodeId.workerId).size());
            return nodes.at(nodeId.workerId).at(nodeId.nodeSubId);
        }

        void addNode(const ConfigType& cfg, const NodeId& parent, std::size_t workerId)
        {
            ARMARX_CHECK_EXPRESSION(workerId < nodes.size());
            nodes.at(workerId).emplace_back(cfg, parent);
            ++size;
        }

        NodeId getNearestNeighbour(const ConfigType& cfg);

        std::size_t getSize() const
        {
            return size;
        }

        void applyUpdate(const PerTreeUpdate& u, Ice::Long workerId)
        {
            for (const auto& n : u.nodes)
            {
                addNode(n.config, n.parent, workerId);
            }
        }

    private:
        static const NodeId ROOT_NODE_ID;
        std::deque<std::deque<NodeType>> nodes;
        std::size_t size = 0;
    };
}

