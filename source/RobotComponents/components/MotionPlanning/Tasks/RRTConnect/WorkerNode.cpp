/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "WorkerNode.h"
#include "../../CSpace/CSpace.h"
namespace armarx::rrtconnect
{
    void WorkerNode::onInitComponent()
    {
        abortRequest = false;
        usingTopic(topicName);
        offeringTopic(topicName);
        updater.setWorkerId(workerId);
        updater.setTrees({treeFromStart, treeFromGoal});
        ARMARX_CHECK_EXPRESSION(!workerThread.joinable());
    }

    void WorkerNode::onConnectComponent()
    {
        globalWorkers = getTopic<TreeUpdateInterfacePrx>(topicName);
        if (!workerThread.joinable())
        {
            workerThread = std::thread
            {
                [this]
                {
                    try
                    {
                        workerTask();
                    }
                    catch (Ice::Exception& e)
                    {
                        ARMARX_ERROR_S << "The worker thread of worker " << workerId << " had an Ice::Exception! \nStack trace:\n"
                                       << e.ice_stackTrace();
                    }
                }
            };
        }
    }

    void WorkerNode::onExitComponent()
    {
        abort();
        workerThread.join();
    }

    Update WorkerNode::getUpdate(Ice::Long updateId, const Ice::Current&) const
    {
        std::lock_guard<std::mutex> lock {updateMutex};
        ARMARX_VERBOSE_S << "[worker " << workerId
                         << "] request for update " << updateId << " (of " << localUpdates.size() << ")";
        return localUpdates.at(updateId);
    }

    void WorkerNode::setWorkerNodes(const WorkerNodeBasePrxList& workers, const Ice::Current&)
    {
        ARMARX_CHECK_EXPRESSION(!doApplyUpdates);
        {
            std::lock_guard<std::mutex> lock {updateMutex};
            this->workers = workers;
            updater.setWorkerCount(workers.size());
        }
        doApplyUpdates = true;
    }

    void WorkerNode::updateTree(const Update& u, const Ice::Current&)
    {
        std::lock_guard<std::mutex> lock {updateMutex};
        updater.addPendingUpdate(u);
    }

    void WorkerNode::applyUpdates()
    {
        if (doApplyUpdates)
        {
            std::unique_lock<std::mutex> lock {updateMutex};
            updater.applyPendingUpdates(
                lock,
                [this](Ice::Long workerId, Ice::Long updateId)
            {
                return getRemoteUpdate(workerId, updateId);
            }
            );
            ARMARX_CHECK_EXPRESSION(lock);
        }
    }

    void WorkerNode::ice_postUnmarshal()
    {
        treeFromStart.setMaximalWorkerCount(workerCount);
        treeFromGoal.setMaximalWorkerCount(workerCount);
        treeFromStart.setRoot(startCfg);
        treeFromGoal.setRoot(goalCfg);
    }

    void WorkerNode::workerTask()
    {
        cspace->initCollisionTest();

        //init sampler
        const auto cspaceDims = cspace->getDimensionsBounds();
        std::vector<std::pair<float, float>> dimensionBounds {};
        dimensionBounds.reserve(getDimensionality());
        std::transform(
            cspaceDims.begin(), cspaceDims.end(),
            std::back_inserter(dimensionBounds),
            [](const FloatRange & dim)
        {
            return std::make_pair(dim.min, dim.max);
        }
        );

        sampler.reset(
            new CuboidSampler
        {
            typename CuboidSampler::DistributionType{dimensionBounds.begin(), dimensionBounds.end()},
            typename CuboidSampler::GeneratorType{std::random_device{}()}
        }
        );

        //util functions
        auto cspaceDerived = CSpacePtr::dynamicCast(cspace);
        auto steer = [cspaceDerived, this](const ConfigType & from, const ConfigType & to)
        {
            ARMARX_CHECK_EXPRESSION(static_cast<std::size_t>(cspaceDerived->getDimensionality()) == from.size());
            ARMARX_CHECK_EXPRESSION(from.size() == to.size());

            return dcdSteer(from, to, DCDStepSize, [cspaceDerived](const ConfigType & cfg)
            {
                return cspaceDerived->isCollisionFree(cfg);
            });
        };
        //init trees
        treeFromStart.setMaximalWorkerCount(workerCount);
        treeFromGoal.setMaximalWorkerCount(workerCount);
        treeFromStart.setRoot(startCfg);
        treeFromGoal.setRoot(goalCfg);

        prepareNextUpdate();

        //plan
        while (true)
        {
            if (abortRequest)
            {
                break;
            }
            //update
            applyUpdates();

            //plan
            auto& primTree = getPrimaryTree();
            auto& secnTree = getSecondaryTree();

            ConfigType randomCfg(getDimensionality());
            (*sampler)(randomCfg.data());

            const auto nearId = primTree.getNearestNeighbour(randomCfg);
            const auto& nearNode = primTree.getNode(nearId);

            const auto reachCfg = steer(nearNode.cfg, randomCfg);
            if (reachCfg != nearNode.cfg)
            {
                //add node
                const auto reachId = addNodeToPrimaryTree(reachCfg, nearId);

                const auto nearSecondId = secnTree.getNearestNeighbour(reachCfg);
                const auto& nearSecondNode = secnTree.getNode(nearSecondId);

                const auto reachSecondCfg = steer(nearSecondNode.cfg, reachCfg);
                if (reachSecondCfg != nearSecondNode.cfg)
                {
                    //add node
                    const auto reachSecondId = addNodeToSecondaryTree(reachSecondCfg, nearSecondId);
                    if (reachSecondCfg == reachCfg)
                    {
                        //found path. set path + exit
                        const auto startTreeId = fromStartIsPrimaryTree ? reachId : reachSecondId;
                        const auto goalTreeId  = fromStartIsPrimaryTree ? reachSecondId : reachId;

                        VectorXfSeq p = treeFromStart.getPathTo(startTreeId);
                        p.pop_back();//pop the middle node (it would be added 2 times)

                        VectorXfSeq pPart2 = treeFromGoal.getReversedPathTo(goalTreeId);
                        std::move(pPart2.begin(), pPart2.end(), std::back_inserter(p));
                        task->setPath({p, "Path"});
                        break;
                    }
                }
            }
            sendUpdate();
            swapTrees();
        }
        //cleanup
        if (!abortRequest)
        {
            globalWorkers->abort();
        }
        task->workerHasAborted(workerId);
    }

    void WorkerNode::sendUpdate()
    {
        std::lock_guard<std::mutex> lock {updateMutex};
        //set dependent updates + id
        ARMARX_CHECK_EXPRESSION(!localUpdates.empty());
        ARMARX_CHECK_EXPRESSION(static_cast<std::size_t>(workerId) < updater.getAppliedUpdateIds().size());
        localUpdates.back().workerId = workerId;
        localUpdates.back().dependetOnUpdateIds = updater.getAppliedUpdateIds();
        //current update id = #updates -1
        ARMARX_CHECK_EXPRESSION(static_cast<std::size_t>(workerId) < localUpdates.back().dependetOnUpdateIds.size());
        localUpdates.back().dependetOnUpdateIds.at(workerId) = static_cast<Ice::Long>(localUpdates.size()) - 2;

        globalWorkers->updateTree(localUpdates.back());
        //next update
        prepareNextUpdate();
    }

    void WorkerNode::prepareNextUpdate()
    {
        localUpdates.emplace_back();
        localUpdates.back().updatesPerTree.resize(2);
    }

    NodeId WorkerNode::addNode(const WorkerNode::ConfigType& cfg, const NodeId& parent, bool addToPrimaryTree)
    {
        cspace->initCollisionTest();
        auto& tree = addToPrimaryTree ? getPrimaryTree() : getSecondaryTree();
        tree.addNode(cfg, parent, workerId);

        const auto treeId = addToPrimaryTree ? getPrimaryTreeId() : getSecondaryTreeId();
        ARMARX_CHECK_EXPRESSION(static_cast<std::size_t>(treeId) < localUpdates.back().updatesPerTree.size());
        localUpdates.back().updatesPerTree.at(treeId).nodes.emplace_back(NodeCreationUpdate {cfg, parent});

        ARMARX_CHECK_EXPRESSION(workerId < static_cast<Ice::Long>(tree.getNodes().size()));
        return {workerId, static_cast<Ice::Long>(tree.getNodes().at(workerId).size()) - 1}; //wid + index of new node
    }
}
