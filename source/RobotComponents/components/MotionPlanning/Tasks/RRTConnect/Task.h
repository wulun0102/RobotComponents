/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <mutex>
#include <chrono>
#include <condition_variable>

#include <ArmarXCore/core/system/FactoryCollectionBase.h>


#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>
#include <ArmarXCore/interface/core/RemoteObjectNode.h>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/RRTConnect/Task.h>
#include "../../util/Metrics.h"

#include "WorkerNode.h"
#include "../MotionPlanningTask.h"

namespace armarx::rrtconnect
{
    class Task;
    using TaskPtr = IceInternal::Handle<Task>;

    class Task:
        public virtual MotionPlanningTaskWithDefaultMembers,
        public virtual TaskBase
    {
    public:
        /**
        * @brief A task using the rrtconnect algorithm.
        * @param cspace The used cspace.
        * @param startCfg The start configuration.
        * @param goalCfg The goal configuration.
        * @param dcdStep The step size used for discrete collision checking.
        * @param maximalPlanningTimeInSeconds The maximal planning time in seconds.
        * @param workerCount The maximal number of computation nodes used.
        */
        Task(//problem
            const CSpaceBasePtr& cspace,
            const VectorXf& startCfg,
            const VectorXf& goalCfg,
            const std::string& taskName = "RRTConnectTask",
            //general
            Ice::Long maximalPlanningTimeInSeconds = 300,
            float dcdStep = 0.01f,
            //management
            Ice::Long workerCount = 4
        );

        //PlanningControlInterface
        /**
        * @brief Aborts planning.
        */
        void abortTask(const Ice::Current& = Ice::emptyCurrent) override;
        /**
        * @return The found path. (empty if no path is found)
        */
        Path getPath(const Ice::Current& = Ice::emptyCurrent) const override;

        void workerHasAborted(Ice::Long workerId, const Ice::Current& = Ice::emptyCurrent) override;

        //PlanningTaskBase
        /**
        * @brief Runs the task.
        * @param remoteNodes The list of \ref RemoteObjectNodeInterfacePrx used to distribute work to computers.
        */
        void run(const RemoteObjectNodePrxList& remoteNodes, const Ice::Current& = Ice::emptyCurrent) override;

        void setPath(const Path& path, const Ice::Current& = Ice::emptyCurrent) override;

        void postEnqueueing() override;

    protected:
        /**
        * @brief Checks for illegal parameters
        */
        void checkParameters();

        /**
        * @brief Ctor used by object factories.
        */
        Task() = default;

        //            TaskStatus::Status taskStatus = eNew;

        mutable std::mutex mtx;
        mutable std::condition_variable doneCV;

        Path solution;

        std::vector<bool> workerAbortedFlags;

    private:
        template<class Base, class Derived> friend class ::armarx::GenericFactory;
    };
}

namespace armarx
{
    using RRTConnectTask = rrtconnect::Task;
    using RRTConnectTaskPtr = IceUtil::Handle<RRTConnectTask>;
    using RRTConnectTaskHandle = RemoteHandle<MotionPlanningTaskControlInterfacePrx>;
}
