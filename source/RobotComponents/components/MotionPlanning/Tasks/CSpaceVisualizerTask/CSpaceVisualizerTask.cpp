/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CSpaceVisualizerTask.h"

#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>


using namespace armarx;


CSpaceVisualizerTask::CSpaceVisualizerTask(const SimoxCSpaceWith2DPoseBasePtr& cspace, const VectorXf& robotPlatform2DPose, const std::string& taskName)
{
    ARMARX_CHECK_EXPRESSION(robotPlatform2DPose.size() == 3) << "The platform pose needs to contain x, y, alpha";
    this->startCfg = robotPlatform2DPose;
    this->goalCfg = VectorXf(3, 0);
    this->cspace = cspace->clone();
    SimoxCSpaceWith2DPosePtr space = SimoxCSpaceWith2DPosePtr::dynamicCast(this->cspace);
    if (space)
    {
        space->setPoseBounds(Vector3fRange {{
                startCfg.at(0) - 10000,
                startCfg.at(1) - 10000,
                -M_PI
            },
            {
                startCfg.at(0) + 10000,
                startCfg.at(1) + 10000,
                M_PI
            }
        });
    }
    else
    {
        throw LocalException() << "The cspace needs to be a SimoxCSpaceWith2DPose";
    }

    this->taskName = taskName;

}

Path armarx::CSpaceVisualizerTask::getPath(const Ice::Current&) const
{
    Path p;
    p.pathName = "RobotPose";
    p.nodes.push_back(this->startCfg);
    //    p.nodes.push_back(this->goalCfg);
    return p;
}

void CSpaceVisualizerTask::run(const armarx::RemoteObjectNodePrxList&, const Ice::Current&)
{
    setTaskStatus(TaskStatus::ePlanning);
    cspace->initCollisionTest();
    setTaskStatus(TaskStatus::eDone);
}

void CSpaceVisualizerTask::ice_postUnmarshal()
{

}
