/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/interface/components/MotionPlanning/Tasks/MotionPlanningTask.h>
#include <ArmarXCore/core/util/distributed/AMDCallbackCollection.h>

namespace armarx
{
    class MotionPlanningTaskCI:
        virtual public MotionPlanningTaskControlInterface
    {
    public:
        //        virtual void abortTask(const Ice::Current& = Ice::emptyCurrent) = 0;
        //        virtual Path getPath(const Ice::Current& = Ice::emptyCurrent) const = 0;
        //        virtual TaskStatus::Status getTaskStatus(const Ice::Current& = Ice::emptyCurrent) const = 0;


        void waitForFinishedPlanning_async(const AMD_MotionPlanningTaskControlInterface_waitForFinishedPlanningPtr& cb, const Ice::Current& = Ice::emptyCurrent) override
        {
            waitForFinishedPlanning.addCallback(cb);
        }
        void waitForFinishedRunning_async(const AMD_MotionPlanningTaskControlInterface_waitForFinishedRunningPtr& cb, const Ice::Current& = Ice::emptyCurrent) override
        {
            waitForFinishedRunning.addCallback(cb);
        }
        /**
        * @brief Returns whether the task is currently planning.
        * @return Whether the task is currently planning.
        */
        bool isRunning(const Ice::Current& = Ice::emptyCurrent) const override;

        /**
        * @brief Returns whether the task has finished planning.
        * @return Whether the task has finished planning.
        */
        bool finishedRunning(const Ice::Current& = Ice::emptyCurrent) const override;
    protected:
        AMDCallbackCollection<> waitForFinishedPlanning;
        AMDCallbackCollection<> waitForFinishedRunning;
    };

    class MotionPlanningMultiPathTaskCI:
        virtual public MotionPlanningMultiPathTaskControlInterface
    {
    public:
        //        virtual Ice::Long getPathCount(const Ice::Current& = Ice::emptyCurrent) const = 0;
        //        virtual Path getNthPath(Ice::Long n, const Ice::Current& = Ice::emptyCurrent) const = 0;
        //        virtual PathSeq getAllPaths(const Ice::Current& = Ice::emptyCurrent) const = 0;
        Path getPath(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return getNthPath(0);
        }
    };

    class MotionPlanningWithCostTaskCI:
        virtual public MotionPlanningWithCostTaskControlInterface
    {
    public:
        PathWithCost getPathWithCost(const Ice::Current& = Ice::emptyCurrent) const override = 0;
        Path getPath(const Ice::Current& = Ice::emptyCurrent) const override
        {
            auto p = getPathWithCost();
            return {std::move(p.nodes), std::move(p.pathName)};
        }
    };

    class MotionPlanningMultiPathWithCostTaskCI:
        virtual public MotionPlanningMultiPathWithCostTaskControlInterface,
        virtual public MotionPlanningMultiPathTaskCI,
        virtual public MotionPlanningWithCostTaskCI
    {
    public:
        //        virtual Ice::Long getPathCount(const Ice::Current& = Ice::emptyCurrent) const = 0;
        //        virtual PathWithCost getBestPath(const Ice::Current& = Ice::emptyCurrent) const = 0;
        //        virtual PathWithCost getNthPathWithCost(Ice::Long, const Ice::Current& = Ice::emptyCurrent) const = 0;
        //        virtual PathWithCostSeq getAllPathsWithCost(const Ice::Current& = Ice::emptyCurrent) const = 0;

        //default impl
        Path getPath(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return MotionPlanningWithCostTaskCI::getPath();
        }

        PathWithCost getPathWithCost(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return getNthPathWithCost(0);
        }
        Path getNthPath(Ice::Long n, const Ice::Current& = Ice::emptyCurrent) const override
        {
            auto p = getNthPathWithCost(n);
            return {std::move(p.nodes), std::move(p.pathName)};
        }
        PathSeq getAllPaths(const Ice::Current& = Ice::emptyCurrent) const override;

    };
}
