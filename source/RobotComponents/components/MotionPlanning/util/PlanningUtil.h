/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <string>
#include <chrono>

#include <RobotComponents/interface/components/MotionPlanning/DataStructures.h>

namespace armarx::TaskStatus
{
    /**
     * @brief Returns the given task status as a string.
     * @param status The status to return as a string.
     * @return The given task status as a string.
     */
    std::string toString(Status status);

    //status information
    /**
     * @brief Returns whether the given task status describes a state where a path is planned.
     * @param status The status to check.
     * @return Whether the given task status describes a state where a path is planned.
     */
    bool isRunning(Status status);
    /**
     * @brief Returns whether the given task status describes a state where planning is done (may be failed).
     * @param status The status to check.
     * @return Whether the given task status describes a state where planning is done (may be failed)
     */
    bool finishedRunning(Status status) ;

    //status transition
    /**
     * @brief Performs a transition from the given to the appropiate following status in case of a task kill.
     * @param status The source status for the transition.
     * @return Returns the following status.
     */
    Status transitionAtKill(Status status);
    /**
     * @brief Performs a transition from the given to the appropiate following status in case of the task failing.
     * @param status The source status for the transition.
     * @return Returns the following status.
     */
    Status transitionAtOutoftime(Status status);
    /**
     * @brief Performs a transition from the given to the appropiate following status in case the task is done.
     * @param status The source status for the transition.
     * @return Returns the following status.
     */
    Status transitionAtDone(Status status);

}

