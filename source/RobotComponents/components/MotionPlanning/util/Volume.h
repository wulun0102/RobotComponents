/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <boost/math/special_functions/gamma.hpp>
#include <boost/math/constants/constants.hpp>

namespace armarx
{
    /**
     * @brief Returns the volume of a n dimensional unit hypersphere.
     * @param n The dimensionality of the sphere.
     * @return The volume of a n dimensional unit hypersphere.
     */
    template<class RealType>
    RealType volumeOfUnitHypersphere(std::size_t n)
    {
        const auto nReal = static_cast<RealType>(n);
        const auto piSqrt = boost::math::constants::root_pi<RealType>();
        const auto divisor = boost::math::tgamma<RealType>(nReal / 2.0 + 1.0);
        return std::pow(piSqrt, nReal) / divisor;
    }

    /**
     * @brief Returns the volume of a n dimensional hypersphere with radius radius.
     * @param n The dimensionality of the sphere.
     * @param radius The radius of the sphere.
     * @return The volume of a n dimensional hypersphere with radius radius.
     */
    template<class RealType>
    RealType volumeOfHypersphere(std::size_t n, RealType radius)
    {
        const auto nReal = static_cast<RealType>(n);
        const auto piSqrt = boost::math::constants::root_pi<RealType>();
        const auto divisor = boost::math::tgamma<RealType>(nReal / 2.0 + 1.0);
        return std::pow(piSqrt * radius, nReal) / divisor;
    }

    /**
     * @brief Returns the volume of an hyperellipsoid with the radii contained by the range [beginRadii, endRadii).
     * @param beginRadii The first radius.
     * @param endRadii One past the last radius.
     * @return The volume of an hyperellipsoid with the radii contained by the range [beginRadii, endRadii)
     */
    template<class RealType, class IteratorType>
    RealType volumeOfHyperellipsoid(IteratorType beginRadii, IteratorType endRadii)
    {
        const auto nReal = static_cast<RealType>(std::distance(beginRadii, endRadii));
        const auto divisor = boost::math::tgamma<RealType>(nReal / 2.0 + 1.0);
        auto dividend = std::pow(boost::math::constants::root_pi<RealType>(), nReal);

        for (; beginRadii != endRadii; ++beginRadii)
        {
            dividend *= *beginRadii;
        }

        return dividend / divisor;
    }

    /**
     * @brief Returns the volume of a hyperspheroid with given dimensionality and radii.
     *
     * A hyperspheroid is a hyperellipsoid with one radius equal the polar radius and all other radii equal the equatorial radius.
     * @param n The dimensionality of the hyperspheroid.
     * @param polarRadius The polar radius of the hyperspheroid.
     * @param equatorialRadius The equatorial radius of the hyperspheroid.
     * @return The volume of the hyperspheroid.
     */
    template<class RealType>
    RealType volumeOfHyperspheroid(std::size_t n, RealType polarRadius, RealType equatorialRadius)
    {
        const auto nReal = static_cast<RealType>(n);
        const auto divisor = boost::math::tgamma<RealType>(nReal / 2.0 + 1.0);
        const auto piSqrt = boost::math::constants::root_pi<RealType>();
        return std::pow(piSqrt, nReal) * std::pow(equatorialRadius, nReal - 1.0) * polarRadius / divisor;
    }
}
