/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <functional>

#include <boost/functional/hash.hpp>

//include "Preprocessor.h"


/**
 * @brief Expands to it's input.
 */
#define ARMARX_IDENTITY(...) __VA_ARGS__

/**
 * @brief Strips the parentheses of an input.
 */
#define ARMARX_STRIP_PARENTHESES(X) ARMARX_IDENTITY X

/**
 *@brief Overloads the std::hash template for Type.
 * If the hash value of type foo depends on the members a and b the call has to be:
 * ARMARX_OVERLOAD_STD_HASH((foo), (arg.a, arg.b))
 * The parentheses are required!
 */
#define ARMARX_OVERLOAD_STD_HASH(Type, Members)\
    namespace std\
    {\
        template<>\
        struct hash< ARMARX_STRIP_PARENTHESES(Type) >\
        {\
            using argument_type = ARMARX_STRIP_PARENTHESES(Type);\
            using result_type = std::size_t;\
            result_type operator()(argument_type const& arg) const\
            {\
                return armarx::hash_all( ARMARX_STRIP_PARENTHESES(Members) );\
            }\
        };\
    }

/**
 *@brief Overloads the std::hash template for a type Type with the member functions begin() and end().
 * If the type depends on some template parameters they can be passed as the first argument.
 * The parentheses around both arguments are required!
 *
 * examples:
 * for vector<int, AllocatorType> : ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((), (vector<int, AllocatorType>))
 * for all vectors: ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((class T, class A), (vector<T, A>))
 */
#define ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE(TemplateArgs, Type)\
    namespace std\
    {\
        template< ARMARX_STRIP_PARENTHESES(TemplateArgs) >\
        struct hash< ARMARX_STRIP_PARENTHESES(Type) >\
        {\
            using argument_type = ARMARX_STRIP_PARENTHESES(Type);\
            using result_type = std::size_t;\
            result_type operator()(argument_type const& arg) const\
            {\
                return boost::hash_range(arg.begin(), arg.end());\
            }\
        };\
    }

namespace armarx
{
    /**
     * @brief Combines the accumulated hash and the parameters hash.
     * @param hashed The accumulated hash.
     * @param t0 The value to hash.
     */
    template<class T0>
    void do_hash(std::size_t& hashed, const T0& t0)
    {
        //replace this call to change combination methode of hash values and the used hash function
        //(currently uses boost::hash_value)
        boost::hash_combine(hashed, t0);
    }

    /**
     * @brief Combines the accumulated hash and the parameters hash.
     * @param hashed The accumulated hash.
     * @param t0 The first value to hash.
     * @param ts Further values to hash.
     */
    template<class T0, class...Ts>
    void do_hash(std::size_t& hashed, const T0& t0, const Ts& ...ts)
    {
        do_hash(hashed, t0);
        do_hash(hashed, ts...);
    }

    /**
     * @brief Returns the hash of all parameters.
     * @param ts The parameters to hash
     * @return The hash of all parameters.
     */
    template<class...Ts>
    std::size_t hash_all(const Ts& ...ts)
    {
        std::size_t hashed = 0;
        do_hash(hashed, ts...);
        return hashed;
    }
}

//basic overloads from boost
#include <tuple>
namespace std
{
    /**
     * @brief Enables hashing of std::tuple.
     */
    template<class...Ts>
    struct hash<tuple<Ts...>>
    {
        /**
         * @brief the hash operator's argument type.
         */
        using argument_type = tuple<Ts...>;
        /**
         * @brief the hash operator's result type
         */
        using result_type = std::size_t;
        /**
         * @brief Hashes a tuple
         * @param the tuple to hash
         * @return The tuple's hash
         */
        result_type operator()(argument_type const& arg) const
        {
            return hash_all(arg);
        }
    };
}

#include <utility>
namespace std
{
    /**
     * @brief Enables hashing of std::pair.
     */
    template<class A, class B>
    struct hash<pair<A, B>>
    {
        /**
         * @brief the hash operator's argument type.
         */
        using argument_type = pair<A, B>;
        /**
         * @brief the hash operator's result type
         */
        using result_type = std::size_t;

        /**
         * @brief Hashes a pair
         * @param the pair to hash
         * @return The pair's hash
         */
        result_type operator()(argument_type const& arg) const
        {
            return armarx::hash_all(arg.first, arg.second);
        }
    };
}

#include <vector>
/**
 * @brief Enables hashing of std::vector.
 */
ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((class T, class A), (vector<T, A>))

#include <deque>
/**
 * @brief Enables hashing of std::deque.
 */
ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((class T, class A), (deque<T, A>))

#include <list>
/**
 * @brief Enables hashing of std::list.
 */
ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((class T, class A), (list<T, A>))

#include <set>
/**
 * @brief Enables hashing of std::set.
 */
ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((class T, class C, class A), (set<T, C, A>))
/**
 * @brief Enables hashing of std::multiset.
 */
ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((class T, class C, class A), (multiset<T, C, A>))

#include <map>
/**
 * @brief Enables hashing of std::map.
 */
ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((class K, class V, class C, class A), (map<K, V, C, A>))
/**
 * @brief Enables hashing of std::multimap.
 */
ARMARX_OVERLOAD_STD_HASH_FOR_ITERABLE((class K, class V, class C, class A), (multimap<K, V, C, A>))

