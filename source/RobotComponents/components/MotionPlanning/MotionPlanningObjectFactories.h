/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <vector>
#include <map>
#include <string>

#include <Ice/ObjectFactory.h>
#include <Ice/Ice.h>

#include <ArmarXCore/core/system/Synchronization.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include "Tasks/AdaptiveDynamicDomainInformedRRTStar/Task.h"
#include "Tasks/AdaptiveDynamicDomainInformedRRTStar/ManagerNode.h"
#include "Tasks/AdaptiveDynamicDomainInformedRRTStar/WorkerNode.h"

#include "Tasks/RRTConnect/Task.h"
#include "Tasks/RRTConnect/WorkerNode.h"

#include "Tasks/AStar/Task.h"

#include "Tasks/RandomShortcutPostprocessor/Task.h"

#include "Tasks/PathCollection/Task.h"

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <RobotComponents/components/MotionPlanning/CSpace/VoxelGridCSpace.h>
#include <RobotComponents/components/MotionPlanning/Tasks/BiRRT/Task.h>
#include <RobotComponents/components/MotionPlanning/Tasks/CSpaceVisualizerTask/CSpaceVisualizerTask.h>

#include "CSpace/SimoxCSpace.h"
#include "CSpace/ScaledCSpace.h"


#include "ResourceRequestStrategies/ComputingPowerRequestStrategy.h"
namespace armarx::ObjectFactories
{
    /**
     * @brief Object factories for objects used by planning algorithms and the algorithms task objects.
     */
    class MotionPlanningObjectFactories : public FactoryCollectionBase
    {
    public:
        /**
        * @return The map of object factories.
        */
        ObjectFactoryMap getFactories() override
        {
            ObjectFactoryMap map;
            //planning alg
            add<armarx::addirrtstar::TaskBase, armarx::addirrtstar::Task>(map);
            add<armarx::addirrtstar::ManagerNodeBase, armarx::addirrtstar::ManagerNode>(map);
            add<armarx::addirrtstar::WorkerNodeBase, armarx::addirrtstar::WorkerNode>(map);

            add<armarx::rrtconnect::TaskBase, armarx::rrtconnect::Task>(map);
            add<armarx::rrtconnect::WorkerNodeBase, armarx::rrtconnect::WorkerNode>(map);
            add<armarx::birrt::TaskBase, armarx::birrt::Task>(map);

            add<armarx::astar::TaskBase, armarx::astar::Task>(map);
            add<armarx::CSpaceVisualizerTaskBase, armarx::CSpaceVisualizerTask>(map);

            add<armarx::rngshortcut::TaskBase, armarx::rngshortcut::Task>(map);
            add<armarx::pathcol::TaskBase, armarx::pathcol::Task>(map);
            //cspace
            add<armarx::SimoxCSpaceBase, armarx::SimoxCSpace>(map);
            add<armarx::SimoxCSpaceWith2DPoseBase, armarx::SimoxCSpaceWith2DPose>(map);
            add<armarx::SimoxCSpaceWith3DPoseBase, armarx::SimoxCSpaceWith3DPose>(map);
            add<armarx::ScaledCSpaceBase, armarx::ScaledCSpace>(map);
            add<armarx::VoxelGridCSpace, armarx::VoxelGridCSpace>(map);

            //resource request strategies
            add<armarx::cprs::AndBase, armarx::cprs::And>(map);
            add<armarx::cprs::OrBase, armarx::cprs::Or>(map);
            add<armarx::cprs::NotBase, armarx::cprs::Not>(map);

            add<armarx::cprs::AlwaysBase, armarx::cprs::Always>(map);
            add<armarx::cprs::NeverBase, armarx::cprs::Never>(map);

            add<armarx::cprs::ElapsedTimeBase, armarx::cprs::ElapsedTime>(map);
            add<armarx::cprs::NoNodeCreatedBase, armarx::cprs::NoNodeCreated>(map);

            add<armarx::cprs::TotalNodeCountBase, armarx::cprs::TotalNodeCount>(map);

            add<armarx::cprs::TaskStatusBase, armarx::cprs::TaskStatus>(map);

            return map;
        }
    };

    const FactoryCollectionBaseCleanUp MotionPlanningObjectFactoriesVar =
        FactoryCollectionBase::addToPreregistration(new MotionPlanningObjectFactories());
}

