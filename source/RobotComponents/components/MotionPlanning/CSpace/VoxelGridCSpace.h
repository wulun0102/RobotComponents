/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once
#include "SimoxCSpace.h"

#include <MemoryX/interface/components/CommonStorageInterface.h>
#include <RobotComponents/interface/components/MotionPlanning/CSpace/VoxelGridCSpaceBase.h>
#include <VirtualRobot/VirtualRobot.h>

namespace armarx
{
    class VoxelGridCSpace :
        virtual public armarx::VoxelGridCSpaceBase,
        virtual public SimoxCSpace
    {
        template <class IceBaseClass, class DerivedClass> friend class armarx::GenericFactory;
        VoxelGridCSpace() {}
    public:
        VoxelGridCSpace(visionx::VoxelGridProviderInterfacePrx voxelGridProvider, memoryx::CommonStorageInterfacePrx cs, bool loadVisualizationModel = false, float stationaryObjectMargin = 0.0f);

        VirtualRobot::SceneObjectPtr createGridObstacle() const;
        // CSpaceBase interface
    public:
        armarx::CSpaceBasePtr clone(const Ice::Current&) override;
        void initCollisionTest(const Ice::Current&) override;
    protected:

        // SimoxCSpace interface
    public:
        CSpaceBasePtr clone(bool loadVisualizationModel) override;
    };
    using VoxelGridCSpacePtr = IceInternal::Handle<VoxelGridCSpace>;
}

