/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string>
#include <atomic>
#include <map>
#include <ArmarXCore/core/system/Synchronization.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/util/StringHelpers.h>

namespace armarx
{

    template <class ObjectTypePtr, class Id = std::string>
    class GlobalCache
    {
    public:
        GlobalCache()
        {
            auto lock = getMapLock();
            getInstanceCounter()++;
        }
        GlobalCache(const GlobalCache& source)
        {
            auto lock = getMapLock();
            getInstanceCounter()++;
        }

        ~GlobalCache()
        {
            auto lock = getMapLock();
            getInstanceCounter()--;
            if (getInstanceCounter() == 0)
            {
                ARMARX_IMPORTANT << "Clearing global cache for type " << armarx::GetTypeString<ObjectTypePtr>(true);
                getCache().clear();
            }
        }

        bool hasObject(const Id& id) const
        {
            auto lock = getMapLock();
            return getCache().count(id) != 0;
        }
        ObjectTypePtr getCacheObject(const Id& id)
        {
            auto lock = getMapLock();
            auto& map = getCache();
            auto it = map.find(id);
            if (it != map.end())
            {
                return it->second;
            }
            else
            {
                throw LocalException("key not found!");
            }
        }

        void insertObject(const Id& id, const ObjectTypePtr& obj)
        {
            auto lock = getMapLock();
            auto& map = getCache();
            auto it = map.find(id);
            if (it != map.end())
            {
                throw LocalException("key exists already");
            }
            map.insert(std::make_pair(id, obj));
        }
    protected:
        std::atomic<int>& getInstanceCounter()
        {
            static std::atomic<int> InstanceCounter = {0};
            return InstanceCounter;
        }

        ScopedLockPtr getMapLock() const
        {
            static Mutex mutex;
            return ScopedLockPtr(new ScopedLock(mutex));
        }

        std::map<Id, ObjectTypePtr>& getCache() const
        {
            static std::map<Id, ObjectTypePtr> map;
            return map;
        }

    };


}


