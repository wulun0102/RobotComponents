/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <functional>
#include <random>

#include <RobotComponents/interface/components/MotionPlanning/CSpace/CSpace.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace armarx
{
    class CSpace;
    /**
     * @brief An ice handle for a CSpace
     */
    using CSpacePtr = IceInternal::Handle<CSpace>;

    /**
     * @brief Implementation of the slice class \ref CSpaceBase.
     * This is the base class for all planning cspaces.
     *
     * The cspace holds all problem specific data of the planning problem.
     * These are:
     *  - The cspace's bounds.
     *  - information required for collision checking.
     *
     * The class provides a function to check whether a configuration is collision free.
     */
    class CSpace :
        virtual public CSpaceBase
    {
    public:



        /**
         * @param config The config to check.
         * @return Whether the given configuration is inside the cspace's bounds.
         */
        bool isValidConfiguration(const ::std::pair<const Ice::Float*, const Ice::Float*>& config, const Ice::Current& = Ice::emptyCurrent) const override;

        //noop functions (can be overwritten in derived classes)
        /**
         * @brief Initializes collision checking.
         * The default implementation is noop.
         */
        void initCollisionTest(const Ice::Current& = Ice::emptyCurrent) override {}

        /**
         * @param config The config to check.
         * @return Whether the given configuration is collision free.
         */
        bool isCollisionFree(const ::std::pair<const Ice::Float*, const Ice::Float*>& config, const Ice::Current& = Ice::emptyCurrent) override = 0;

        /**
         * @return A clone of this object.
         */
        CSpaceBasePtr clone(const Ice::Current& = Ice::emptyCurrent) override = 0;

        /**
         * @param first The configuration's first element.
         * @param last The configuration's last element.
         * @return Whether the given configuration [first,last) is collision free.
         */
        virtual bool isCollisionFree(const Ice::Float* first, const Ice::Float* last)
        {
            return isCollisionFree(std::make_pair(first, last));
        }

        /**
         * @param config The config to check. (as \ref armarx::VectorXf)
         * @return Whether the given configuration is inside the cspace's bounds.
         */
        virtual bool isValidConfiguration(const VectorXf& config) const
        {
            return isValidConfiguration(std::make_pair(config.data(), config.data() + config.size()));
        }

        /**
         * @param config The config to check. (as \ref armarx::VectorXf)
         * @return Whether the given configuration is collision free.
         */
        virtual bool isCollisionFree(const VectorXf& config)
        {
            return isCollisionFree(std::make_pair(config.data(), config.data() + config.size()));
        }

        /**
         * @return Whether the cspaces scales some axis internally during planning
         */
        virtual bool usesInternalScaling()
        {
            return false;
        }
    };

    class CSpaceAdaptor;
    /**
     * @brief An ice handle for a CSpace
     */
    using CSpaceAdaptorPtr = IceInternal::Handle<CSpaceAdaptor>;

    class CSpaceAdaptor:
        virtual public CSpace,
        virtual public CSpaceAdaptorBase
    {
    public:
        CSpaceAdaptor(CSpaceBasePtr originalCSpace): CSpaceAdaptorBase(originalCSpace)
        {
            if (! originalCSpace)
            {
                throw std::invalid_argument {"the original cspace can't be null"};
            }
        }

        CSpaceBasePtr getOriginalCSpace(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return originalCSpace;
        }
    protected:
        CSpaceAdaptor() = default;
    };
}
