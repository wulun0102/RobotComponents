/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::MMMPlayer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotComponents/interface/components/MMMPlayerInterface.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/Trajectory.h>

#include <mutex>

namespace armarx
{
    class MotionFileWrapper;
    class MotionData;

    /**
             * \class MMMPlayerPropertyDefinitions
             * \brief
             */
    class MMMPlayerPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        MMMPlayerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ArmarXProjects", "", "Comma-separated list with names of ArmarXProjects (e.g. 'Armar3,Armar4'). The MMM XML File can be specified relatively to a data path of one of these projects.");
            //defineOptionalProperty<std::string>("MMMFile", "", "Path to MMM XML File");
            defineOptionalProperty<bool>("ApplyButterworthFilter", false, "If true a butterworth filter is applied on the trajectory.");
            defineOptionalProperty<float>("ButterworthFilterCutOffFreq", 20, "Cut off frequency for the butterworth lowpass filter.");
        }
    };

    /**
             * \defgroup Component-MMMPlayer MMMPlayer
             * \ingroup RobotComponents-Components
             * \brief Replays an MMM trajectory from a file.
             *
             * MMMPlayer reads an MMM trajectory from an MMM XML file (component property) and replays the motion using the KinematicUnit and its currently loaded robot.
             * The trajectory can be replayed using position control or velocity control.
             * In the latter case, the control parameters (P, I, D) can be configured via component properties.
             */

    /**
             * @ingroup Component-MMMPlayer
             * @brief The MMMPlayer class
             */
    class MMMPlayer :
        virtual public armarx::Component,
        public armarx::MMMPlayerInterface
    {
    public:
        /**
                 * @see armarx::ManagedIceObject::getDefaultName()
                 */
        std::string getDefaultName() const override
        {
            return "MMMPlayer";
        }

    protected:
        /**
                 * @see armarx::ManagedIceObject::onInitComponent()
                 */
        void onInitComponent() override;

        /**
                 * @see armarx::ManagedIceObject::onConnectComponent()
                 */
        void onConnectComponent() override;

        /**
                 * @see armarx::ManagedIceObject::onDisconnectComponent()
                 */
        void onDisconnectComponent() override;

        /**
                 * @see armarx::ManagedIceObject::onExitComponent()
                 */
        void onExitComponent() override;

        /**
                 * @see PropertyUser::createPropertyDefinitions()
                 */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        void load(const std::string& filename, const std::string& projects);

        std::shared_ptr<MotionFileWrapper> motionWrapper;
        std::shared_ptr<MotionData> motionData;
        std::string motionPath;

        std::recursive_mutex mmmMutex;
    public:
        // MMMPlayerInterface
        bool loadMMMFile(const std::string& filename, const std::string& projects, const Ice::Current&) override;
        bool setMotionData(const std::string& motionName, const Ice::Current&) override;
        int getNumberOfFrames(const Ice::Current&) override;
        std::string getMotionPath(const Ice::Current&) override;
        std::string getModelPath(const Ice::Current&) override;
        Ice::StringSeq getJointNames(const Ice::Current&) override;
        Ice::StringSeq getMotionNames(const Ice::Current&) override;
        bool isMotionLoaded(const Ice::Current&) override;

        TrajectoryBasePtr getJointTraj(const Ice::Current&) override;
        TrajectoryBasePtr getBasePoseTraj(const Ice::Current&) override;

    };

    using MMMPlayerPtr = ::IceInternal::Handle< ::armarx::MMMPlayer>;

}

