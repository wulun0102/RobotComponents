/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::MMMPlayer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MMMPlayer.h"
#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>
#include <MMM/Model/ModelReaderXML.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <VirtualRobot/MathTools.h>
#include <RobotAPI/libraries/core/Trajectory.h>
#include <ArmarXCore/observers/filters/ButterworthFilter.h>
#include <ArmarXCore/core/util/StringHelpers.h>
#include <RobotComponents/libraries/MMM/MotionFileWrapper.h>

#include <Ice/ObjectAdapter.h>

using namespace armarx;
using namespace MMM;


void MMMPlayer::onInitComponent()
{
    //    offeringTopic("DebugObserver");
}


void MMMPlayer::onConnectComponent()
{
    ARMARX_INFO << "ON CONNECT";
    std::string armarxProjects = getProperty<std::string>("ArmarXProjects").getValue();

    if (!armarxProjects.empty())
    {
        std::vector<std::string> projects = armarx::Split(armarxProjects, ",;", true, true);

        for (std::string& p : projects)
        {
            //            ARMARX_INFO << "Adding to datapaths of " << p;
            armarx::CMakePackageFinder finder(p);

            if (!finder.packageFound())
            {
                ARMARX_WARNING << "ArmarX Package " << p << " has not been found!";
            }
            else
            {
                ARMARX_INFO << "Adding " << p << " to datapaths: " << finder.getDataDir();
                armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
            }
        }
    }

    std::string motionDefault = getProperty<std::string>("MMMFile").getValue();

    if (!motionDefault.empty())
    {
        load(motionDefault, "Armar4"); // TODO remove?!
    }

    /*std::string modelDefault = getProperty<std::string>("ModelFile").getValue();
    if (!modelDefault.empty())
    {
        modelPath = modelDefault; // TODO dieses Attribut ist sinnlos!
    }*/
}

void MMMPlayer::load(const std::string& MMMFile, const std::string& projects)
{
    std::unique_lock lock(mmmMutex);

    if (!projects.empty())
    {
        std::vector<std::string> proj = armarx::Split(projects, ",;", true, true);

        for (std::string& p : proj)
        {
            ARMARX_INFO << "Adding to datapaths of " << p;
            armarx::CMakePackageFinder finder(p);

            if (!finder.packageFound())
            {
                ARMARX_WARNING << "ArmarX Package " << p << " has not been found!";
            }
            else
            {
                ARMARX_INFO << "Adding to datapaths: " << finder.getDataDir();
                armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
            }
        }
    }

    ArmarXDataPath::getAbsolutePath(MMMFile, motionPath);

    motionWrapper = MotionFileWrapper::create(motionPath, getProperty<bool>("ApplyButterworthFilter").getValue() ? getProperty<float>("ButterworthFilterCutOffFreq") : 0);
    if (!motionWrapper)
    {
        terminate();
        motionData = nullptr;
        return;
    }

    motionData = motionWrapper->getFirstMotionData();
}

bool MMMPlayer::loadMMMFile(const std::string& MMMFile, const std::string& projects, const Ice::Current&)
{
    ARMARX_VERBOSE << "loaded trajectory " << MMMFile;
    load(MMMFile, projects);
    return motionData != nullptr;
}

bool MMMPlayer::setMotionData(const std::string& motionName, const Ice::Current&)
{
    motionData = motionWrapper->getMotionData(motionName);
    return motionData != nullptr;
}

int MMMPlayer::getNumberOfFrames(const Ice::Current&)
{
    return (int) motionData->numberOfFrames;
}

std::string MMMPlayer::getMotionPath(const Ice::Current&)
{
    return motionPath;
}

std::string MMMPlayer::getModelPath(const Ice::Current&)
{
    return motionData->modelPath;
}

Ice::StringSeq MMMPlayer::getJointNames(const Ice::Current&)
{
    return motionData->jointNames;
}

Ice::StringSeq MMMPlayer::getMotionNames(const Ice::Current&)
{
    if (!motionWrapper)
    {
        return Ice::StringSeq();
    }
    return motionWrapper->getMotionNames();
}

bool MMMPlayer::isMotionLoaded(const Ice::Current&)
{
    return motionData != nullptr;
}

void MMMPlayer::onDisconnectComponent()
{

}

void MMMPlayer::onExitComponent()
{

}

TrajectoryBasePtr MMMPlayer::getJointTraj(const Ice::Current&)
{
    return motionData->getJointTrajectory();
}

TrajectoryBasePtr MMMPlayer::getBasePoseTraj(const Ice::Current&)
{
    return motionData->getPoseTrajectory();
}

PropertyDefinitionsPtr MMMPlayer::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new MMMPlayerPropertyDefinitions(getConfigIdentifier()));
}
