/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::RobotIK
 * @author     Joshua Haustein ( joshua dot haustein at gmail dot com )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotComponents::ArmarXObjects::RobotIK

#define ARMARX_BOOST_TEST

#define EPSILON_POS 1.0f
#define EPSILON_ROT 0.1f
#define NUMBER_THREADS 10

#include <RobotComponents/Test.h>
#include <RobotComponents/components/RobotIK/RobotIK.h>
#include <RobotComponents/components/RobotIK/test/RobotIKTestEnvironment.h>

#include <VirtualRobot/RobotNodeSet.h>

#include <iostream>
#include <thread>
// #include <chrono>
// #include <ctime>
#include <ratio>
#include <random>

bool comparePosition(const Eigen::Matrix4f& m1, const Eigen::Matrix4f& m2)
{
    bool equal = true;

    for (size_t i = 0; i < 3; ++i)
    {
        equal &= (m1(i, 3) - m2(i, 3)) * (m1(i, 3) - m2(i, 3)) <= EPSILON_POS;
    }

    if (!equal)
    {
        BOOST_TEST_MESSAGE("Positions not equal for poses: " << m1 << ", " << m2);
    }

    return equal;
}

bool compareOrientation(const Eigen::Matrix4f& m1, const Eigen::Matrix4f& m2)
{
    bool equal = true;

    for (size_t i = 0; i < 3; ++i)
    {
        for (size_t j = 0; j < 3; ++j)
        {
            equal &= (m1(i, j) - m2(i, j)) * (m1(i, j) - m2(i, j)) <= EPSILON_ROT;
        }
    }

    if (!equal)
    {
        BOOST_TEST_MESSAGE("Poses not equal: " << m1 << ", " << m2);
    }

    return equal;
}

bool compareResults(const Eigen::Matrix4f& m1, const Eigen::Matrix4f& m2, armarx::CartesianSelection csel)
{
    bool success;

    if (csel == armarx::CartesianSelection::ePosition)
    {
        success = comparePosition(m1, m2);
    }
    else if (csel == armarx::CartesianSelection::eAll)
    {
        success = comparePosition(m1, m2) && compareOrientation(m1, m2);
    }
    else
    {
        // TODO extend in case other values of armarx::CartesianSelection should be tested
        success = false;
    }

    return success;
}

void synchRobot(VirtualRobot::RobotPtr localRobot, armarx::RobotStateComponentInterfacePrx robotState)
{
    VirtualRobot::RobotConfigPtr robotConfig = localRobot->getConfig();
    armarx::SharedRobotInterfacePrx sharedRobot = robotState->getSynchronizedRobot();
    for (auto& roboNode : robotConfig->getNodes())
    {
        armarx::SharedRobotNodeInterfacePrx sharedRoboNode = sharedRobot->getRobotNode(roboNode->getName());
        float val = sharedRoboNode->getJointValue();
        localRobot->getRobotNode(roboNode->getName())->setJointValue(val);
    }
}

void testIKGlobalPose(const std::string& setName, armarx::PosePtr pose, bool& success, armarx::CartesianSelection csel,
                      VirtualRobot::RobotPtr localRobot, armarx::RobotIKInterfacePrx robotIK, armarx::RobotStateComponentInterfacePrx sharedRobot)
{
    // Test component
    synchRobot(localRobot, sharedRobot);
    armarx::NameValueMap iksolution = robotIK->computeIKGlobalPose(setName, pose, csel);

    // Check validity of output
    if (iksolution.size() > 0)
    {
        BOOST_TEST_MESSAGE("Pose " << pose);
        BOOST_TEST_MESSAGE("Validating ik solution" << iksolution);
        localRobot->setJointValues(iksolution);
        BOOST_TEST_MESSAGE("configuration after setting:");
        for (auto& ikEntry : iksolution)
        {
            float val = localRobot->getRobotNode(ikEntry.first)->getJointValue();
            BOOST_TEST_MESSAGE(ikEntry.first << ": " << val);
        }
        VirtualRobot::RobotNodeSetPtr nodeset = localRobot->getRobotNodeSet(setName);
        Eigen::Matrix4f tcpPose = nodeset->getTCP()->getGlobalPose();
        // BOOST_TEST_MESSAGE("Comparing tcp pose");
        success = compareResults(tcpPose, pose->toEigen(), csel);
    }
    else
    {
        BOOST_TEST_MESSAGE("Empty solution");
        success = false;
    }
}

void testIKFramedPose(const std::string& setName, armarx::FramedPosePtr fpose, bool& success, armarx::CartesianSelection csel,
                      VirtualRobot::RobotPtr localRobot, armarx::RobotIKInterfacePrx robotIK, armarx::RobotStateComponentInterfacePrx roboState)
{
    armarx::FramedPosePtr globalPose = fpose->toGlobal(roboState->getSynchronizedRobot());
    Eigen::Matrix4f poseGlobal = globalPose->toEigen();

    // Test component
    synchRobot(localRobot, roboState);
    armarx::NameValueMap iksolution = robotIK->computeIKFramedPose(setName, fpose, csel);

    // Check validity of output
    if (iksolution.size() > 0)
    {
        VirtualRobot::RobotNodeSetPtr nodeset = localRobot->getRobotNodeSet(setName);
        localRobot->setJointValues(iksolution);
        Eigen::Matrix4f tcpPose = nodeset->getTCP()->getGlobalPose();

        success = compareResults(tcpPose, poseGlobal, csel);
    }
    else
    {
        success = false;
    }
}

void doGlobalPoseTests(const RobotIKTestEnvironment& testEnv, bool& finished)
{
    // Create test input
    std::string nodeSetName1("TorsoLeftArm");
    Eigen::Matrix4f poseMatrix1;
    poseMatrix1 << 1.0f, 0.0f, 0.0f, 400.0f,
                0.0f, 1.0f, 0.0f, 600.0f,
                0.0f, 0.0f, 1.0f, 900.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
    armarx::PosePtr pose1 = new armarx::Pose(poseMatrix1);
    // Create test input 2
    std::string nodeSetName2 = "TorsoRightArm";
    Eigen::Matrix4f poseMatrix2;
    poseMatrix2 << 1.0f, 0.0f, 0.0f, 400.0f,
                0.0f, -1.0f, 0.0f, 100.0f,
                0.0f, 0.0f, -1.0f, 600.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
    armarx::PosePtr pose2 = new armarx::Pose(poseMatrix2);

    Eigen::Matrix4f unreachablePoseMatrix;
    unreachablePoseMatrix << 1.0f, 0.0f, 0.0f, 1000.0f,
                          0.0f, 1.0f, 0.0f, 1000.0f,
                          0.0f, 0.0f, 1.0f, 1300.0f,
                          0.0f, 0.0f, 0.0f, 1.0f;
    armarx::PosePtr unreachablePose = new armarx::Pose(unreachablePoseMatrix);

    bool testSuccesses[3 * NUMBER_THREADS];
    // bool testSuccesses[2 * NUMBER_THREADS];
    std::vector<std::thread> threads;

    for (unsigned int tid = 0; tid < NUMBER_THREADS; ++tid)
    {
        VirtualRobot::RobotPtr localRobot1 = testEnv._robotModel->clone("LocalRobot1");
        threads.push_back(std::thread(testIKGlobalPose, std::ref(nodeSetName1), pose1,
                                      std::ref(testSuccesses[3 * tid]), armarx::CartesianSelection::ePosition, localRobot1, testEnv._robotIK, testEnv._robotStateComponent));
        VirtualRobot::RobotPtr localRobot2 = testEnv._robotModel->clone("LocalRobot2");
        threads.push_back(std::thread(testIKGlobalPose, std::ref(nodeSetName2), pose2,
                                      std::ref(testSuccesses[3 * tid + 1]), armarx::CartesianSelection::eAll, localRobot2, testEnv._robotIK, testEnv._robotStateComponent));
        VirtualRobot::RobotPtr localRobot3 = testEnv._robotModel->clone("LocalRobot3");
        threads.push_back(std::thread(testIKGlobalPose, std::ref(nodeSetName2), unreachablePose,
                                      std::ref(testSuccesses[3 * tid + 2]), armarx::CartesianSelection::eAll, localRobot3, testEnv._robotIK, testEnv._robotStateComponent));
    }

    for (size_t tid = 0; tid < threads.size(); ++tid)
    {
        threads[tid].join();
    }

    BOOST_CHECK(threads.size() == 3 * NUMBER_THREADS);

    for (size_t tid = 0; tid < NUMBER_THREADS; ++tid)
    {
        BOOST_CHECK(testSuccesses[3 * tid]);
        BOOST_CHECK(testSuccesses[3 * tid + 1]);
        BOOST_CHECK(!testSuccesses[3 * tid + 2]);
    }

    finished = true;
}

void doFramedPoseTests(const RobotIKTestEnvironment& testEnv, bool& finished)
{
    // Create test input
    std::string nodeSetName("TorsoLeftArm");
    std::string frame("TCP L");
    Eigen::Matrix4f poseMatrix;
    poseMatrix << 0.86603f, -0.5f, 0.0f, 50.0f,
               0.5f, 0.86603f, 0.0f, 60.0f,
               0.0f, 0.0f, 1.0f, 40.0f,
               0.0f, 0.0f, 0.0f, 1.0f;
    armarx::FramedPosePtr pose = new armarx::FramedPose(poseMatrix, frame, testEnv._robotStateComponent->getRobotName());

    bool testSuccesses[NUMBER_THREADS];
    std::vector<std::thread> threads;

    for (unsigned int tid = 0; tid < NUMBER_THREADS; ++tid)
    {
        VirtualRobot::RobotPtr localRobot = testEnv._robotModel->clone("LocalRobot");
        threads.push_back(std::thread(testIKFramedPose, std::ref(nodeSetName), pose,
                                      std::ref(testSuccesses[tid]), armarx::CartesianSelection::eAll, localRobot, testEnv._robotIK, testEnv._robotStateComponent));
    }

    for (size_t tid = 0; tid < threads.size(); ++tid)
    {
        threads[tid].join();
    }

    for (size_t tid = 0; tid < NUMBER_THREADS; ++tid)
    {
        BOOST_CHECK(testSuccesses[tid]);
    }

    finished = true;
}

void doReachabilityTests(const RobotIKTestEnvironment& testEnv, bool& finished)
{
    std::string nodeSetName("TorsoLeftArm");
    std::string frame("TCP L");
    std::string baseFrame("Armar3_Base");

    // Define two global poses
    // First, a reachable pose
    Eigen::Matrix4f matrix;
    matrix << 0.275582f,  0.950628f,  0.142693f,  -363.636f,
           0.892777f, -0.198076f, -0.404616f,   181.818f,
           -0.356376f,  0.238898f, -0.903285f,   727.273f,
           1.0f,         1.0f,         1.0f,         1.0f;
    armarx::PosePtr reachablePose = new armarx::Pose(matrix);
    // Second, a unreachable pose
    matrix << 1.0f, 0.0f, 0.0f, 1000.0f,
           0.0f, 1.0f, 0.0f, 1000.0f,
           0.0f, 0.0f, 1.0f, 1300.0f,
           0.0f, 0.0f, 0.0f, 1.0f;
    armarx::PosePtr unreachablePose = new armarx::Pose(matrix);
    // Now define two framed poses
    // Again, first reachable
    // matrix << 1.0f, 0.0f, 0.0f, 5.0f,
    //           0.0f, 1.0f, 0.0f, 6.0f,
    //           0.0f, 0.0f, 1.0f, 4.0f,
    //           0.0f, 0.0f, 0.0f, 1.0f;
    // armarx::FramedPosePtr reachableFramedPose = new armarx::FramedPose(matrix, frame, testEnv._robotStateComponent->getRobotName());
    // and an unreachable
    matrix << 0.86603f, -0.5f, 0.0f, 1000.0f,
           0.5f, 0.86603f, 0.0f, 600.0f,
           0.0f, 0.0f, 1.0f, 400.0f,
           0.0f, 0.0f, 0.0f, 1.0f;
    armarx::FramedPosePtr unreachableFramedPose = new armarx::FramedPose(matrix, frame, testEnv._robotStateComponent->getRobotName());

    // To create a reachability space we need workspace bounds.
    armarx::WorkspaceBounds minBounds;
    minBounds.x = -1000.0f;
    minBounds.y = -1000.0f;
    minBounds.z = -1000.0f;
    minBounds.ro = -3.15f;
    minBounds.pi = -3.15f;
    minBounds.ya = -3.15f;

    armarx::WorkspaceBounds maxBounds;
    maxBounds.x = 1000.0f;
    maxBounds.y = 1000.0f;
    maxBounds.z = 1000.0f;
    maxBounds.ro = 3.15f;
    maxBounds.pi = 3.15f;
    maxBounds.ya = 3.15f;

    // Now start testing
    // First, make a sanity check on hasReachabilitySpace
    BOOST_CHECK(!testEnv._robotIK->hasReachabilitySpace(nodeSetName));

    // We don't have a reachability space yet, so isPoseReachable should always return false.
    BOOST_CHECK(!testEnv._robotIK->isPoseReachable(nodeSetName, reachablePose));
    BOOST_CHECK(!testEnv._robotIK->isPoseReachable(nodeSetName, unreachablePose));
    // BOOST_CHECK(!testEnv._robotIK->isFramedPoseReachable(nodeSetName, reachableFramedPose));
    BOOST_CHECK(!testEnv._robotIK->isFramedPoseReachable(nodeSetName, unreachableFramedPose));

    bool createdReachabilitySpace = testEnv._robotIK->createReachabilitySpace(nodeSetName, baseFrame, 200.0f, 0.3f, minBounds, maxBounds, 1000000);
    // This should work
    BOOST_CHECK(createdReachabilitySpace);

    // Now we should have a reachability space:
    BOOST_CHECK(testEnv._robotIK->hasReachabilitySpace(nodeSetName));

    // Finally test the poses:
    //    BOOST_CHECK(testEnv._robotIK->isPoseReachable(nodeSetName, reachablePose));
    BOOST_CHECK(!testEnv._robotIK->isPoseReachable(nodeSetName, unreachablePose));
    // BOOST_CHECK(testEnv._robotIK->isFramedPoseReachable(nodeSetName, reachableFramedPose));
    BOOST_CHECK(!testEnv._robotIK->isFramedPoseReachable(nodeSetName, unreachableFramedPose));

    std::string savepath(testEnv._spaceSavePath + "testReachabilitySpace.bin");
    BOOST_TEST_MESSAGE(savepath);
    BOOST_CHECK(testEnv._robotIK->saveReachabilitySpace(nodeSetName, savepath));
    finished = true;

}

void doLoadReachabilityTest(const RobotIKTestEnvironment& testEnv, bool& finished)
{
    std::string nodeSetName("TorsoLeftArm");
    std::string frame("TCP L");
    std::string baseFrame("Armar3_Base");

    // Define two global poses
    // First, a reachable pose
    Eigen::Matrix4f matrix;
    matrix << 0.275582f,  0.950628f,  0.142693f,  -363.636f,
           0.892777f, -0.198076f, -0.404616f,   181.818f,
           -0.356376f,  0.238898f, -0.903285f,   727.273f,
           1.0f,         1.0f,         1.0f,         1.0f;
    armarx::PosePtr reachablePose = new armarx::Pose(matrix);
    // Second, a unreachable pose
    matrix << 1.0f, 0.0f, 0.0f, 1000.0f,
           0.0f, 1.0f, 0.0f, 1000.0f,
           0.0f, 0.0f, 1.0f, 1300.0f,
           0.0f, 0.0f, 0.0f, 1.0f;
    armarx::PosePtr unreachablePose = new armarx::Pose(matrix);

    matrix << 0.86603f, -0.5f, 0.0f, 1000.0f,
           0.5f, 0.86603f, 0.0f, 600.0f,
           0.0f, 0.0f, 1.0f, 400.0f,
           0.0f, 0.0f, 0.0f, 1.0f;
    armarx::FramedPosePtr unreachableFramedPose = new armarx::FramedPose(matrix, frame, testEnv._robotStateComponent->getRobotName());

    // To create a reachability space we need workspace bounds.
    armarx::WorkspaceBounds minBounds;
    minBounds.x = -1000.0f;
    minBounds.y = -1000.0f;
    minBounds.z = -1000.0f;
    minBounds.ro = -3.15f;
    minBounds.pi = -3.15f;
    minBounds.ya = -3.15f;

    armarx::WorkspaceBounds maxBounds;
    maxBounds.x = 1000.0f;
    maxBounds.y = 1000.0f;
    maxBounds.z = 1000.0f;
    maxBounds.ro = 3.15f;
    maxBounds.pi = 3.15f;
    maxBounds.ya = 3.15f;

    // Now start testing
    testEnv._robotIK->createReachabilitySpace(nodeSetName, baseFrame, 200.0f, 0.3f, minBounds, maxBounds, 1000000);

    // Now we should have a reachability space:
    BOOST_CHECK(testEnv._robotIK->hasReachabilitySpace(nodeSetName));

    // Finally test the poses:
    BOOST_CHECK(testEnv._robotIK->isPoseReachable(nodeSetName, reachablePose));
    BOOST_CHECK(!testEnv._robotIK->isPoseReachable(nodeSetName, unreachablePose));
    // BOOST_CHECK(testEnv._robotIK->isFramedPoseReachable(nodeSetName, reachableFramedPose));
    BOOST_CHECK(!testEnv._robotIK->isFramedPoseReachable(nodeSetName, unreachableFramedPose));

    finished = true;
}

void doDefineNodeSetTests(const RobotIKTestEnvironment& testEnv, bool& finished)
{
    std::string setName1("TestSet1");
    std::string tcpName("TCP L");
    std::string rootNode("Platform");
    armarx::NodeNameList jointList;
    jointList.push_back("Shoulder 1 L");
    jointList.push_back("Shoulder 2 L");
    jointList.push_back("Upperarm L");
    jointList.push_back("Elbow L");
    jointList.push_back("Underarm L");

    VirtualRobot::RobotPtr localRobot = testEnv._robotModel->clone("MyLocalRobot");

    bool defineSuccessful = testEnv._robotIK->defineRobotNodeSet(setName1, jointList, tcpName, rootNode);
    BOOST_CHECK(defineSuccessful);

    VirtualRobot::RobotNodeSet::createRobotNodeSet(localRobot, setName1, jointList, rootNode, tcpName, true);

    Eigen::Matrix4f poseMatrix1;
    poseMatrix1 << -0.258819f, -0.965926f, 0.0f, -315.0f,
                0.0f, 0.0f, 1.0f, 550.0f,
                -0.965926f, 0.258819f, 0.0f, 1077.5f,
                0.0f, 0.0f, 0.0f, 1.0f;

    armarx::PosePtr pose1 = new armarx::Pose(poseMatrix1);

    bool ikSuccess = false;
    testIKGlobalPose(setName1, pose1, ikSuccess, armarx::CartesianSelection::ePosition, localRobot, testEnv._robotIK, testEnv._robotStateComponent);
    BOOST_CHECK(ikSuccess);

    // Redefine should always return true.
    BOOST_CHECK(testEnv._robotIK->defineRobotNodeSet(setName1, jointList, tcpName, rootNode));

    // Redefine with different parameters, however, always false
    armarx::NodeNameList modifiedJointList(jointList);
    modifiedJointList.pop_back();
    BOOST_CHECK(!testEnv._robotIK->defineRobotNodeSet(setName1, modifiedJointList, tcpName, rootNode));
    std::string tcpName2("TCP R");
    BOOST_CHECK(!testEnv._robotIK->defineRobotNodeSet(setName1, jointList, tcpName2, rootNode));
    std::string rootNode2("Shoulder 1 L");
    BOOST_CHECK(!testEnv._robotIK->defineRobotNodeSet(setName1, jointList, tcpName, rootNode2));
    finished = true;
}

void doRobotIKComputeCoMIKTests(const RobotIKTestEnvironment& testEnv, bool& finished)
{
    std::string completeRobotSetname("Robot");
    armarx::CoMIKDescriptor comdesc;
    comdesc.priority = 0;
    comdesc.gx = 4.0;
    comdesc.gy = 45.0;
    comdesc.robotNodeSetBodies = completeRobotSetname;
    comdesc.tolerance = 2.0f;
    comdesc.coordSysName = "Armar3_Base";
    std::string jointsSetName("TorsoBothArms");
    VirtualRobot::RobotPtr localRobot = testEnv._robotModel->clone("MyLocalRobot");

    armarx::NameValueMap ikSolution = testEnv._robotIK->computeCoMIK(jointsSetName, comdesc);
    VirtualRobot::RobotNodeSetPtr robotNodeSet = localRobot->getRobotNodeSet(completeRobotSetname);

    localRobot->setJointValues(ikSolution);
    Eigen::Vector3f resultCoM = robotNodeSet->getCoM();

    // BOOST_TEST_MESSAGE("desired com " << comdesc.gx << " " << comdesc.gy);
    // BOOST_TEST_MESSAGE("is com " << resultCoM[0] << " " << resultCoM[1]);
    BOOST_CHECK((comdesc.gx - resultCoM[0]) * (comdesc.gx - resultCoM[0]) <= comdesc.tolerance * comdesc.tolerance);
    BOOST_CHECK((comdesc.gy - resultCoM[1]) * (comdesc.gy - resultCoM[1]) <= comdesc.tolerance * comdesc.tolerance);
    finished = true;
}

void doRobotIKComputeHierarchicalDeltaIKTests(const RobotIKTestEnvironment& testEnv, bool& finished)
{
    std::string jointsSetName("TorsoBothArms");
    armarx::IKTasks ikTasks;
    // Set up a task for the left arm first
    armarx::DifferentialIKDescriptor leftArmGoal;
    leftArmGoal.priority = 2;
    leftArmGoal.tcpName = "TCP L";
    leftArmGoal.csel = armarx::CartesianSelection::ePosition;
    leftArmGoal.ijm = armarx::InverseJacobiMethod::eSVD;
    Eigen::Matrix4f poseMatrix1;
    poseMatrix1 << 1.0f, 0.0f, 0.0f, -285.0f,
                0.0f, 1.0f, 0.0f, 540.0f,
                0.0f, 0.0f, 1.0f, 1125.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
    armarx::PosePtr pose1 = new armarx::Pose(poseMatrix1);
    leftArmGoal.tcpGoal = pose1;
    ikTasks.push_back(leftArmGoal);
    VirtualRobot::RobotPtr localRobot = testEnv._robotModel->clone("MyLocalRobot");

    // now let's set up a task for the right arm
    armarx::DifferentialIKDescriptor rightArmGoal;
    rightArmGoal.priority = 1;
    rightArmGoal.tcpName = "TCP R";
    rightArmGoal.csel = armarx::CartesianSelection::ePosition;
    rightArmGoal.ijm = armarx::InverseJacobiMethod::eSVD;
    Eigen::Matrix4f poseMatrix2;
    poseMatrix2 << 1.0f, 0.0f, 0.0f, 400.0f,
                0.0f, 1.0f, 0.0f, 100.0f,
                0.0f, 0.0f, 1.0f, 900.0f,
                0.0f, 0.0f, 0.0f, 1.0f;
    armarx::PosePtr pose2 = new armarx::Pose(poseMatrix2);
    rightArmGoal.tcpGoal = pose2;
    ikTasks.push_back(rightArmGoal);

    // finally let's add a center of mass task
    std::string completeRobotSetname("Robot");
    armarx::CoMIKDescriptor comdesc;
    comdesc.priority = 0;
    comdesc.gx = 4.0;
    comdesc.gy = 45.0;
    comdesc.robotNodeSetBodies = completeRobotSetname;
    comdesc.tolerance = 2.0f;
    comdesc.coordSysName = "Armar3_Base";

    float stepSize = 0.1;
    armarx::NameValueMap ikGradient;
    BOOST_TEST_MESSAGE(poseMatrix1);

    while (!compareResults(poseMatrix1, localRobot->getRobotNodeSet("TorsoLeftArm")->getTCP()->getGlobalPose(), armarx::CartesianSelection::ePosition)
           || !compareResults(poseMatrix2, localRobot->getRobotNodeSet("TorsoRightArm")->getTCP()->getGlobalPose(), armarx::CartesianSelection::ePosition))
    {
        ikGradient = testEnv._robotIK->computeHierarchicalDeltaIK(jointsSetName, ikTasks, comdesc, stepSize, true, true);
        BOOST_CHECK(ikGradient.size() > 0);

        VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(jointsSetName);
        const std::vector<VirtualRobot::RobotNodePtr> robotNodes = rns->getAllRobotNodes();

        armarx::NameValueMap newConfig;
        for (auto& node : robotNodes)
        {
            float val = node->getJointValue();
            float delta = ikGradient.find(node->getName())->second;
            newConfig.insert(armarx::NameValueMap::value_type(node->getName(), val + delta));
        }
        localRobot->setJointValues(newConfig);
        testEnv._robotStateComponent->reportJointAngles(newConfig, IceUtil::Time::now().toMicroSeconds(), true);
    }

    BOOST_CHECK(compareResults(poseMatrix1, localRobot->getRobotNodeSet("TorsoLeftArm")->getTCP()->getGlobalPose(), armarx::CartesianSelection::ePosition));
    BOOST_CHECK(compareResults(poseMatrix2, localRobot->getRobotNodeSet("TorsoRightArm")->getTCP()->getGlobalPose(), armarx::CartesianSelection::ePosition));

    Eigen::Vector3f resultCoM = localRobot->getRobotNodeSet(completeRobotSetname)->getCoM();
    BOOST_TEST_MESSAGE("desired com " << comdesc.gx << " " << comdesc.gy);
    BOOST_TEST_MESSAGE("is com " << resultCoM[0] << " " << resultCoM[1]);

    BOOST_CHECK((comdesc.gx - resultCoM[0]) * (comdesc.gx - resultCoM[0]) <= comdesc.tolerance * comdesc.tolerance);
    BOOST_CHECK((comdesc.gy - resultCoM[1]) * (comdesc.gy - resultCoM[1]) <= comdesc.tolerance * comdesc.tolerance);
    finished = true;
}

BOOST_AUTO_TEST_CASE(RobotIKGlobalPoseTest)
{
    // Create test environment first
    RobotIKTestEnvironment testEnv("RobotIKGlobalPoseTest");
    bool finished = false;
    doGlobalPoseTests(testEnv, finished);
}

BOOST_AUTO_TEST_CASE(RobotIKFramedPoseTest)
{
    // Create test environment first
    RobotIKTestEnvironment testEnv("RobotIKFramedPoseTest");
    bool finished = false;
    doFramedPoseTests(testEnv, finished);
}

BOOST_AUTO_TEST_CASE(RobotIKReachabilityTest)
{
    RobotIKTestEnvironment testEnv("RobotIKReachabilityTest");
    bool finished = false;
    doReachabilityTests(testEnv, finished);
}

BOOST_AUTO_TEST_CASE(RobotIKDefineRobotSetTest)
{
    RobotIKTestEnvironment testEnv("RobotIKDefineNodeTest");
    bool finished = false;
    doDefineNodeSetTests(testEnv, finished);
}

BOOST_AUTO_TEST_CASE(RobotIKComputeCoMIKTest)
{
    RobotIKTestEnvironment testEnv("RobotIKComputeCoMIKTest");
    bool finished = false;
    doRobotIKComputeCoMIKTests(testEnv, finished);
}

BOOST_AUTO_TEST_CASE(RobotIKComputeHierarchicalDeltaIK)
{
    RobotIKTestEnvironment testEnv("RobotIKComputeHierarchicalDeltaIK");
    bool finished = false;
    doRobotIKComputeHierarchicalDeltaIKTests(testEnv, finished);
}

BOOST_AUTO_TEST_CASE(RobotIKLoadReachabilitySpaceTest)
{
    RobotIKTestEnvironment testEnv("RobotIKLoadReachabilitySpaceTest", true);
    std::string nodeSetName("TorsoLeftArm");
    BOOST_CHECK(testEnv._robotIK->hasReachabilitySpace(nodeSetName));
}

// BOOST_AUTO_TEST_CASE(RobotIKLoadTest)
// {
//     RobotIKTestEnvironment testEnv("RobotIKLoadTest");
//     // set up time stuff
//     std::chrono::steady_clock::time_point startPoint = std::chrono::steady_clock::now();
//     std::chrono::duration<double> time_span = std::chrono::duration_cast< std::chrono::duration<double> >(std::chrono::steady_clock::now() - startPoint);
//     // set up random generators
//     std::default_random_engine generator; //1234567890
//     // std::uniform_int_distribution<int> uniDistribution(0, 5);
//     std::uniform_int_distribution<int> uniDistribution(0, 5);
//     // some bookkeeping data structures
//     std::vector<std::thread> threads;
//     bool hikStarted = false;
//     bool hikFinished = false;
//     bool dummyFinished = false;

//     while (time_span.count() < 120.0)
//     {
//         int testNumber = uniDistribution(generator);
//         switch (testNumber)
//         {
//             case 0:
//                 threads.push_back(std::thread(doGlobalPoseTests, std::ref(testEnv), std::ref(dummyFinished)));
//                 break;
//             case 1:
//                 threads.push_back(std::thread(doFramedPoseTests, std::ref(testEnv), std::ref(dummyFinished)));
//                 break;
//             case 2:
//                 threads.push_back(std::thread(doLoadReachabilityTest, std::ref(testEnv), std::ref(dummyFinished)));
//                 break;
//             case 3:
//                 threads.push_back(std::thread(doDefineNodeSetTests, std::ref(testEnv), std::ref(dummyFinished)));
//                 break;
//             case 4:
//                 threads.push_back(std::thread(doRobotIKComputeCoMIKTests, std::ref(testEnv), std::ref(dummyFinished)));
//                 break;
//             case 5:
//                 if (!hikStarted)
//                 {
//                     hikStarted = true;
//                     // threads.push_back(std::thread(doRobotIKComputeHierarchicalDeltaIKTests, std::ref(testEnv), std::ref(hikFinished)));
//                 }
//                 break;
//         }
//         if (hikFinished)
//         {
//             hikStarted = false;
//             hikFinished = false;
//         }
//         std::this_thread::sleep_for(std::chrono::seconds(uniDistribution(generator)));
//         time_span = std::chrono::duration_cast< std::chrono::duration<double> >(std::chrono::steady_clock::now() - startPoint);
//     }

//     for (size_t i = 0; i < threads.size(); ++i)
//     {
//         threads[i].join();
//     }
// }
