/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::SimpleEpisodicMemoryPlatformUnitConnector
 * @author     Fabian PK ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <eigen3/Eigen/Core>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <MemoryX/components/SimpleEpisodicMemory/SimpleEpisodicMemoryConnector.h>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

namespace armarx
{

    class SimpleEpisodicMemoryPlatformUnitConnector :
            virtual public armarx::Component,
            virtual public PlatformUnitListener,
            virtual public memoryx::SimpleEpisodicMemoryConnector
    {
    public:
        SimpleEpisodicMemoryPlatformUnitConnector();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        void reportPlatformPose(const PlatformPose &, const Ice::Current &);
        void reportNewTargetPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current &);
        void reportPlatformVelocity(Ice::Float, Ice::Float, Ice::Float, const Ice::Current &);
        void reportPlatformOdometryPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current &);

    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        void checkAndSendToMemory();

    private:
        int frequency;
        PeriodicTask<SimpleEpisodicMemoryPlatformUnitConnector>::pointer_type periodic_task;
        bool updated_pose;
        double timestampLastUpdateInMs_pose;
        std::mutex updatedMutex_pose;

        bool updated_target;
        double timestampLastUpdateInMs_target;
        std::mutex updatedMutex_target;


        float x;
        float y;
        float rot;
        std::mutex platformPose_mutex;

        float goal_x;
        float goal_y;
        float goal_rot;
        std::mutex platformTarget_mutex;

        float acc_x;
        float acc_y;
        float acc_rot;
        std::mutex platformAcceleration_mutex;

    };
}
