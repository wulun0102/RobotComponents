/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::ViconMarkerProvider
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <DataStreamClient.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <RobotComponents/interface/components/ViconMarkerProviderInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

namespace armarx
{
    /**
     * @class ViconMarkerProviderPropertyDefinitions
     * @brief
     */
    class ViconMarkerProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ViconMarkerProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("Hostname", "IP or hostname of the Vicon PC");
            defineOptionalProperty<int>("Port", 801, "Port used of the Vicon SDK on the Vicon PC");
            defineOptionalProperty<std::string>("ViconDataTopicName", "ViconDataUpdates", "Topic on which the marker data is published");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Topic for debug drawer");
        }
    };

    /**
     * @defgroup Component-ViconMarkerProvider ViconMarkerProvider
     * @ingroup RobotComponents-Components
     * A description of the component ViconMarkerProvider.
     *
     * @class ViconMarkerProvider
     * @ingroup Component-ViconMarkerProvider
     * @brief Brief description of class ViconMarkerProvider.
     *
     * Detailed description of class ViconMarkerProvider.
     */
    class ViconMarkerProvider :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "ViconMarkerProvider";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        void publish();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        ViconMarkerProviderListenerInterfacePrx listener;
        RunningTask<ViconMarkerProvider>::pointer_type publisherThread;
        ViconDataStreamSDK::CPP::Client dsClient;
        DebugDrawerInterfacePrx debugDrawer;
    };
}

