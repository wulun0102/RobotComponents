/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <MemoryX/components/LongtermMemory/LongtermMemory.h>
#include <MemoryX/components/PriorKnowledge/PriorKnowledge.h>
#include <MemoryX/components/CommonStorage/CommonStorage.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <MemoryX/core/MongoTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <RobotComponents/components/DMPComponent/DMPComponent.h>

class DMPComponentEnvironment
{
public:
    DMPComponentEnvironment(const std::string& testName)
    {

        Ice::PropertiesPtr properties = Ice::createProperties();
        armarx::Application::LoadDefaultConfig(properties);
        properties->setProperty("Ice.ThreadPool.Client.SizeMax", "2");
        properties->setProperty("Ice.ThreadPool.Server.SizeMax", "2");
        properties->setProperty("MemoryX.LongtermMemory.DatabaseName", "memdb");
        properties->setProperty("RobotComponents.DMPComponent.LongtermMemoryName", "LongtermMemory");
        properties->setProperty("MemoryX.PriorKnowledge.ClassCollections", "testdb.Prior_Objects");
        properties->setProperty("MemoryX.PriorKnowledge.RelationCollections", "");
        properties->setProperty("MemoryX.CommonStorage.MongoUser", "testuser");
        properties->setProperty("MemoryX.CommonStorage.MongoPassword", "testpass");


        iceTestHelper = new IceTestHelper();
        iceTestHelper->startEnvironment();
        manager = new TestArmarXManager(testName, iceTestHelper->getCommunicator(), properties);

        using namespace memoryx;
        manager->createComponentAndRun<CommonStorage, CommonStorageInterfacePrx>("MemoryX", "CommonStorage");
        manager->createComponentAndRun<PriorKnowledge, PriorKnowledgeInterfacePrx>("MemoryX", "PriorKnowledge");
        ltm = manager->createComponentAndRun<LongtermMemory, LongtermMemoryInterfacePrx>("MemoryX", "LongtermMemory");
        dmp = manager->createComponentAndRun<armarx::DMPComponent, armarx::DMPComponentBasePrx>("ArmarX", "DMPComponent");

    }

    MongoTestHelper mongoTestHelper;
    TestArmarXManagerPtr manager;
    IceTestHelperPtr iceTestHelper;
    memoryx::LongtermMemoryInterfacePrx ltm;
    armarx::DMPComponentBasePrx dmp;
};

using DMPComponentEnvironmentPtr = std::shared_ptr<DMPComponentEnvironment>;

