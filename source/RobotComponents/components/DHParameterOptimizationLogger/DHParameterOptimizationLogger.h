/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::DHParameterOptimizationLogger
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/csv/CsvWriter.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/filters/rtfilters/AverageFilter.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <RobotComponents/interface/components/ViconMarkerProviderInterface.h>
#include <RobotComponents/interface/components/DHParameterOptimizationLoggerInterface.h>

#include <mutex>

namespace armarx
{
    /**
     * @class DHParameterOptimizationLoggerPropertyDefinitions
     * @brief
     */
    class DHParameterOptimizationLoggerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DHParameterOptimizationLoggerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ViconDataTopicName", "ViconDataUpdates", "Topic on which the marker data is published by the ViconMarkerProvider");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent");
            defineOptionalProperty<std::string>("RobotUnitObserverName", "RobotUnitObserver", "Name of the RobotUnitObserver");
            defineOptionalProperty<std::string>("LoggingFilePath", "~/home/DHLogging", "Path to the logging file");
            defineOptionalProperty<std::string>("DebugObserverTopicName", "DebugObserver", "The topic where updates are send to");
            defineOptionalProperty<int>("FilterWindowSize", 100, "Window size of average filter applied to vicon marker positions.");
            defineRequiredProperty<std::string>("Neck_ViconObjectName", "Name of the object for the neck marker that is used in the vicon system. "
                                                "This is needed to parse the data received from the vicon system correctly.");
            defineRequiredProperty<std::string>("Hand_ViconObjectName", "Name of the object for the hand marker that is used in the vicon system. "
                                                "This is needed to parse the data received from the vicon system correctly.");
        }
    };

    /**
     * @defgroup Component-DHParameterOptimizationLogger DHParameterOptimizationLogger
     * @ingroup RobotComponents-Components
     * A description of the component DHParameterOptimizationLogger.
     *
     * @class DHParameterOptimizationLogger
     * @ingroup Component-DHParameterOptimizationLogger
     * @brief Brief description of class DHParameterOptimizationLogger.
     *
     * Detailed description of class DHParameterOptimizationLogger.
     */
    class DHParameterOptimizationLogger :
        virtual public armarx::Component,
        virtual public DHParameterOptimizationLoggerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "DHParameterOptimizationLogger";
        }

        // DHParameterOptimizationLoggerInterface interface
        /**
         * @brief logData logs data for the current position. Blocks until as many vicon data frames as specified by the property "FilterWindowSize" were received.
         */
        void logData(const Ice::Current& = Ice::emptyCurrent) override;
        void logDataWithRepeatAccuracy(const Ice::Current& = Ice::emptyCurrent) override;
        void startViconLogging(const Ice::Current& = Ice::emptyCurrent) override;
        void stopViconLogging(const Ice::Current& = Ice::emptyCurrent) override;
        void init(const std::string& kinematicChainName,
                  const std::map<std::string, std::string>& neckMarkerMapping,
                  const std::map<std::string, std::string>& handMarkerMapping,
                  const std::string& loggingFileName,
                  bool logRepeatAccuracy,
                  const Ice::Current& = Ice::emptyCurrent) override;
    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ViconMarkerProviderListenerInterface interface
        void reportLabeledViconMarkerFrame(const StringVector3fMap& markerPosMap, const Ice::Current& = Ice::emptyCurrent) override;
        void reportUnlabeledViconMarkerFrame(const Vector3fSeq& markerPos, const Ice::Current& = Ice::emptyCurrent) override;

    private:
        RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;
        ObserverInterfacePrx robotUnitObserver;
        DebugObserverInterfacePrx debugObserverPrx;
        DebugDrawerInterfacePrx debugDrawerPrx;

        std::string _filePath;
        void setupFile(const std::string& fileName);
        NameList _header;
        void setupHeader();
        bool _initialized;

        StringVector3fMap _viconMarkerBuffer;
        bool _viconBufferUpdated;
        StringVector3fMap _filteredViconMarkerPositions_robotRootFrame;
        bool _filteredViconMarkerPositionsUpdated;
        IceUtil::Time _timeStampLastViconDataUpdate;
        mutable std::mutex _bufferMutex;
        bool _viconLoggingActive;
        size_t _filterSize;

        void logData(std::vector<float>& data);
        void logViconMarkerPositions(std::vector<float>& data);
        void logJointValues(std::vector<float>& data) const;
        void logForwardKinematicToViconMarker(std::vector<float>& data) const;
        void logErrorBetweenModelAndVicon(std::vector<float>& data);
        void logRawTorqueTicks(std::vector<float>& data) const;
        void logTorqueValues(std::vector<float>& data) const;
        //        void logRawFTSensorTicks(std::vector<float>& data) const;
        void logFTSensorValues(std::vector<float>& data) const;

        StringVector3fMap waitBlockingForAllMarkersFiltered();

        std::string _kcName;
        VirtualRobot::RobotNodeSetPtr _kc;
        NameList _jointNames;
        NameList _neckMarker;
        NameList _handMarker;
        std::map<std::string, std::string> _neckMarkerMapping;
        std::map<std::string, std::string> _handMarkerMapping;
        VirtualRobot::RobotNodePtr getNeckMarkerRootNode() const;
        VirtualRobot::RobotNodePtr getHandMarkerRootNode() const;
        std::string _viconObjectName_neck;
        std::string _viconObjectName_hand;

        bool _logRepeatAccuracy;
        Eigen::Matrix4f _lastObservedHandRootPose;
        Eigen::Matrix4f _newObservedHandRootPose;
        bool _observedHandPosesUpdated;
        std::pair<float, float> calculateRepeatError();

        Eigen::Matrix4f registration3d(const Eigen::MatrixXf& matrixA, const Eigen::MatrixXf& matrixB) const;
        StringVector3fMap transformViconMarkerPositionsIntoRobotRootFrame(const StringVector3fMap& values) const;
    };
}
