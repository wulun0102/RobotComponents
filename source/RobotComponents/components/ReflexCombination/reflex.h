/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/thread/mutex.hpp>

namespace armarx
{
    class ReflexCombination;
    class Reflex
    {
    public:
        Reflex(int interval)
        {
            this->interval = interval;

            task = new PeriodicTask<Reflex>(this, &Reflex::calc, interval, false, "HeadStabilizationTask");
            task->setDelayWarningTolerance(5);
        }

        virtual ~Reflex()
        {
            stop();
        }


        void start()
        {
            //boost::mutex::scoped_lock lock(mutex);

            if (!task->isRunning())
            {
                task->start();
            }
        }

        void stop()
        {
            //boost::mutex::scoped_lock lock(mutex);

            if (task->isRunning())
            {
                task->stop();
            }

            onStop();

            jointAngles.clear();
        }

        void setEnabled(bool enabled)
        {
            if (enabled)
            {
                start();
            }
            else
            {
                stop();
            }
        }

        std::map<std::string, float> getJoints()
        {
            boost::mutex::scoped_lock lock(mutex);

            std::map<std::string, float> result(jointAngles);

            //if ((  currentTime - updateTime) > xxx) {

            //result.swap(jointAngles);
            // }

            return result;
        }


        float getWeight() const
        {
            return weight;
        }

        void setWeight(float weight)
        {
            this->weight = weight;
        }

        virtual std::string getName() const = 0;


    protected:

        Eigen::Vector3f quaternionToRPY(Eigen::Quaternionf q)
        {
            Eigen::Vector3f euler;

            euler(0) = -std::atan2(2.0 * (q.w() * q.x() - q.y() * q.z()), 1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y()));  //roll
            euler(1) = std::asin(2.0 * (q.w() * q.y() - q.z() * q.x()));                                                 //pitch
            euler(2) = std::atan2(2.0 * (q.w() * q.z() + q.x() * q.y()), 1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z()));   //yaw

            return euler;
        }

        virtual void calc() = 0;

        virtual void onStop() = 0;

        boost::mutex mutex;

        std::map<std::string, float>  jointAngles;

        int interval;

        bool isEnabled;

        IceUtil::Time updateTime;

        std::string name;


    private:

        PeriodicTask<Reflex>::pointer_type task;

        float weight;

    };


    using ReflexPtr = std::shared_ptr<Reflex>;
}
