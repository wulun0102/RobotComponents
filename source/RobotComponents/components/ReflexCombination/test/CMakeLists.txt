
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ReflexCombination)
 
armarx_add_test(ReflexCombinationTest ReflexCombinationTest.cpp "${LIBS}")