

#include "GazeStabInputOutput.hh"

// input

GazeStabInput::GazeStabInput(int n_LB_joints, int n_UB_joints)
{
    tsim = 0.;

    q_LB.reserve(n_LB_joints);
    q_UB.reserve(n_UB_joints);
    qd_LB.reserve(n_LB_joints);
    qd_UB.reserve(n_UB_joints);

    qd_LB_des = Eigen::VectorXd::Zero(n_LB_joints);

    for (int i = 0; i < n_LB_joints; i++)
    {
        q_LB[i] = 0.;
        qd_LB[i] = 0.;
    }

    pos_target << 1.150, 6.69, 1.1;
}

GazeStabInput::~GazeStabInput()
    = default;

// output

GazeStabOutput::GazeStabOutput(int n_UB_joints)
{
    qd_UB_des.reserve(n_UB_joints);

}

GazeStabOutput::~GazeStabOutput()
    = default;


