
#pragma once

#include "symbolic_routines/mbs_sensor2.h"

#include <vector>

namespace armarx
{

    class ForwardPredictor
    {
    public:

        ForwardPredictor();
        ~ForwardPredictor();

        std::vector<float> getOptFlowPred();
        std::vector<float> getIMUPred();

    private:

        void calc();
        void predict_IMU();
        void predict_optFlow();

        MbsSensor sens_virt, sens_eye, sens_head_imu;

        // input

        // output

        // head IMU prediction
        //std::vector<float> orientationQuaternion;
        std::vector<float> gyroscopeRotation_pred; // head velocity in absolute frame (x,y,z) [rad/s]

        // optical flow prediction
        std::vector<float> optFlow_pred; // [x, y] in [deg/s]

    };

}

