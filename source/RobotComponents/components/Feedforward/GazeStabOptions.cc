
#include "GazeStabOptions.hh"

GazeStabOptions::GazeStabOptions(int model)
{

    this->model = model;

    weighted_inverse = 1;
    pseudo_inverse = 0;

    lambda = 0.05;
}

GazeStabOptions::~GazeStabOptions()
    = default;



