
#include "forwardPredictor.h"


using namespace armarx;

ForwardPredictor::ForwardPredictor()
    = default;

ForwardPredictor::~ForwardPredictor()
    = default;

void ForwardPredictor::calc()
{

    predict_IMU();

    predict_optFlow();
}

void ForwardPredictor::predict_IMU()
{

}

void ForwardPredictor::predict_optFlow()
{

}




