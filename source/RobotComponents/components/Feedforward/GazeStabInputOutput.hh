#pragma once

#include <Eigen/Dense>
#include <vector>

class GazeStabInput
{
public:

    GazeStabInput(int n_LB_joints, int n_UB_joints);
    ~GazeStabInput();

    double tsim;                // time [s]
    std::vector<double> q_LB;   // Lower Body joints configuration [rad and m]
    std::vector<double> q_UB;   // Upper Body (neck and eyes) joints configuration [rad]

    std::vector<double> qd_LB;  // Lower Body joints velocity [rad/s and m/s]
    std::vector<double> qd_UB;  // Upper Body (neck and eyes) joints velocity [rad/s]

    Eigen::VectorXd qd_LB_des;  // Lower Body joints velocity references (command copy) [rad/s]

    Eigen::Vector3d pos_target; // position in world frame of the target [m]

};

class GazeStabOutput
{
public:

    GazeStabOutput(int n_UB_joints);
    ~GazeStabOutput();

    std::vector<double> qd_UB_des;  // Upper Body joints velocity references (command copy) [rad/s]

};

