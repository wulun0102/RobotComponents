/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::SimpleEpisodicMemoryKinematicUnitConnector
 * @author     Fabian PK ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <eigen3/Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <MemoryX/components/SimpleEpisodicMemory/SimpleEpisodicMemoryConnector.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

namespace armarx
{
    class SimpleEpisodicMemoryKinematicUnitConnector :
            virtual public armarx::Component,
            virtual public KinematicUnitListener,
            virtual public memoryx::SimpleEpisodicMemoryConnector
    {
    public:
        std::string getDefaultName() const override;

        SimpleEpisodicMemoryKinematicUnitConnector();

        void reportControlModeChanged(const NameControlModeMap &, Ice::Long, bool, const Ice::Current &);
        void reportJointAngles(const NameValueMap &, Ice::Long, bool, const Ice::Current &);
        void reportJointVelocities(const NameValueMap &, Ice::Long, bool, const Ice::Current &);
        void reportJointTorques(const NameValueMap &, Ice::Long, bool, const Ice::Current &);
        void reportJointAccelerations(const NameValueMap &, Ice::Long, bool, const Ice::Current &);
        void reportJointCurrents(const NameValueMap &, Ice::Long, bool, const Ice::Current &);
        void reportJointMotorTemperatures(const NameValueMap &, Ice::Long, bool, const Ice::Current &);
        void reportJointStatuses(const NameStatusMap &, Ice::Long, bool, const Ice::Current &);

    private:
        void checkAndSendToMemory();

    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        int frequency;
        PeriodicTask<SimpleEpisodicMemoryKinematicUnitConnector>::pointer_type periodic_task;
        bool updated;
        double timestampLastUpdateInMs;
        std::mutex updatedMutex;
        std::map<std::string, float> jointAngleMap;
        std::mutex jointAngle_mutex;
        std::map<std::string, float> jointVelocityMap;
        std::mutex jointVelocity_mutex;
        std::map<std::string, float> jointTorqueMap;
        std::mutex jointTorque_mutex;
        std::map<std::string, float> jointAccelerationMap;
        std::mutex jointAcceleration_mutex;
        std::map<std::string, float> jointCurrentMap;
        std::mutex jointCurrent_mutex;
        std::map<std::string, float> jointTemperatureMap;
        std::mutex jointTemperature_mutex;
        std::map<std::string, bool> jointEnabledMap;
        std::mutex jointEnabled_mutex;

    };
}
