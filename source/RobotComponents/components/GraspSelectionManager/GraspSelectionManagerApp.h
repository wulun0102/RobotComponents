/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents::GraspSelectionManager
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include "GraspSelectionManager.h"
#include <ArmarXCore/core/application/Application.h>
#include <RobotComponents/components/GraspSelectionManager/selectionCriteria/DummyCriterion.h>
#include <RobotComponents/components/GraspSelectionManager/selectionCriteria/NaturalGraspFilter.h>

namespace armarx
{
    class GraspSelectionManagerApp :
        virtual public armarx::Application
    {
        void setup(const armarx::ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override
        {
            registry->addObject(armarx::Component::create<GraspSelectionManager>(properties, "GraspSelectionManager"));
            registry->addObject(armarx::Component::create<DummyCriterion>(properties, "DummyCriterion"));
            registry->addObject(armarx::Component::create<NaturalGraspFilter>(properties, "NaturalGraspFilter"));
        }
    };
}
