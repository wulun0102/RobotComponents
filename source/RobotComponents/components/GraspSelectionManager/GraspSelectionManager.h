/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents::GraspSelectionManager
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotComponents/interface/components/GraspingManager/GraspSelectionManagerInterface.h>
#include <RobotComponents/interface/components/GraspingManager/RobotPlacementInterface.h>

namespace armarx
{

    class GraspSelectionManagerPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        GraspSelectionManagerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            //            defineOptionalProperty<float>("AskHumanThreshold", 0.7f, "Confidence threshold below which the human will be asked for confirmation of a replacement");
        }
    };


    /*!
     * \brief The GraspSelectionManager class
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT GraspSelectionManager :
        virtual public GraspSelectionManagerInterface,
        virtual public Component
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "GraspSelectionManager";
        }

        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(
                       new GraspSelectionManagerPropertyDefinitions(getConfigIdentifier()));
        }

        void onInitComponent() override;
        void onConnectComponent() override;

        /**
         * @brief Filters the grasps for each registered criterion.
         *
         * The criteria have to be defined in the calling method, before filterGrasps is called and register themselves
         * with the GraspSelectionManager. If no criteria are
         * defined, the criteria vector stays uninitilized and no grasps are filtered. In this case the original grasp
         * list is returned. Otherwise, the list of grasps is filtered for/by each registered criterion.
         *
         * @param grasps
         * @return list of grasps
         */
        GeneratedGraspList filterGrasps(const GeneratedGraspList& grasps, const Ice::Current& = Ice::emptyCurrent) override;
        GraspingPlacementList filterPlacements(const GraspingPlacementList& placements, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Checks if a criterion is already registered; if not, register it with the GraspSelectionManager
         * @param criterion
         */
        void registerAsGraspSelectionCriterion(const GraspSelectionCriterionInterfacePrx& criterion, const Ice::Current& = Ice::emptyCurrent) override;
        GraspSelectionCriterionInterfaceList getRegisteredGraspSelectionCriteria(const Ice::Current&) override;
        //FeedbackPublisherInterface

        std::vector<GraspSelectionCriterionInterfacePrx> criteria;
    };
}

