/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotComponents/components/GraspSelectionManager/GraspSelectionCriterionBase.h>
#include <MemoryX/components/CommonPlacesLearner/CommonPlacesLearner.h>
#include <ArmarXCore/core/Component.h>

namespace armarx
{
    class DummyCriterionPropertyDefinitions:
        public GraspSelectionCriterionPropertyDefinitions
    {
    public:
        DummyCriterionPropertyDefinitions(std::string prefix):
            GraspSelectionCriterionPropertyDefinitions(prefix)
        {
            //            defineOptionalProperty<std::string>("CommonPlacesLearnerName", "CommonPlacesLearnerGraspSelectionCriterion", "The CommonPlacesLearner to use");
        }
    };

    class DummyCriterion : public GraspSelectionCriterionBase
    {
    public:
        DummyCriterion();

        // ManagedIceObject interface
    protected:
        void onInitGraspSelectionCriterion() override;
        void onConnectGraspSelectionCriterion() override;
        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new DummyCriterionPropertyDefinitions(
                           getConfigIdentifier()));
        }

    public:
        GeneratedGraspList filterGrasps(const GeneratedGraspList& grasps, const Ice::Current&) override;
        GraspingPlacementList filterPlacements(const GraspingPlacementList& placements, const Ice::Current&) override;
        Ice::Int getHash(const Ice::Current&) const override
        {
            return 0;
        }
    };

} // namespace spoac

