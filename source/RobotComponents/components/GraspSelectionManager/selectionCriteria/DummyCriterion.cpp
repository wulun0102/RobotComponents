/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DummyCriterion.h"

#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;

DummyCriterion::DummyCriterion()
{
}

void DummyCriterion::onInitGraspSelectionCriterion()
{
    //    usingProxy("LongtermMemory");
}

void DummyCriterion::onConnectGraspSelectionCriterion()
{
    //    This would automatically add the SelectionCriterion
    //    graspSelectionManager = getProxy<GraspSelectionManagerInterfacePrx>(getProperty<std::string>("GraspSelectionManagerName").getValue());
    //    graspSelectionManager->registerAsGraspSelectionCriterion(GraspSelectionCriterionInterfacePrx::uncheckedCast(getProxy()));
}

std::string DummyCriterion::getDefaultName() const
{
    return "DummyCriterion";
}

GeneratedGraspList DummyCriterion::filterGrasps(const GeneratedGraspList& grasps, const Ice::Current&)
{
    return grasps;
}

GraspingPlacementList DummyCriterion::filterPlacements(const GraspingPlacementList& placements, const Ice::Current&)
{
    return placements;
}
