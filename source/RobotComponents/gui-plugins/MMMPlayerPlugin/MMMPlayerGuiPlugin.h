/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::ObjectExaminerGuiPlugin
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @copyright  2012
* @license    http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License

*/

#pragma once

/* ArmarX headers */
#include <RobotComponents/gui-plugins/MMMPlayerPlugin/ui_MMMPlayerGuiPlugin.h>

#include "MMMPlayerConfigDialog.h"
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>

/* Qt headers */
#include <QMainWindow>
#include <QTimer>
#include <RobotComponents/interface/components/MMMPlayerInterface.h>


#include <RobotAPI/interface/components/TrajectoryPlayerInterface.h>
#include <RobotAPI/libraries/core/Trajectory.h>

#include <string>
#include <QLayout>
#include <QFileDialog>
#include <QListWidget>



namespace armarx
{
    class MMMPlayerConfigDialog;

    /*!
      \class MMMPlayerGuiPlugin
      \brief This plugin provides a widget with which the MMMPlayer can be controlled.

      \see MMMPlayerWidget
      */
    class MMMPlayerGuiPlugin :
        public ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        MMMPlayerGuiPlugin();
        QString getPluginName() override
        {
            return "MMMPlayerGuiPlugin";
        }
    };

    /*!
      \class MMMPlayerWidget
      \brief With this widget the MMMPlayer can be controlled.

      \ingroup RobotAPI-ArmarXGuiPlugins ArmarXGuiPlugins
      \see MMMPlayerGuiPlugin
      */
    class MMMPlayerWidget :
        public ArmarXComponentWidgetControllerTemplate<MMMPlayerWidget>
    {
        Q_OBJECT
    public:
        MMMPlayerWidget();
        ~MMMPlayerWidget()
        {}

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;
        void onDisconnectComponent() override;

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "RobotControl.MMMPlayerGUI";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/MMM.png");
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override
        {
            if (!dialog)
            {
                dialog = new MMMPlayerConfigDialog(parent);
            }

            return qobject_cast<MMMPlayerConfigDialog*>(dialog);

        }

        bool onClose() override;



        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        //        void configured();

        void startComponent();
        void loadTrajPlayer();
        void setSpeed(double value);
    signals:

    private slots:

        void on_initializeButton_clicked();

        void on_startButton_clicked();

        void on_previewButton_clicked();

        void on_pauseButton_clicked();

        void on_stopButton_clicked();

        void configFileSelected();

        void updateSlider();

        void setupJointList();

        void setupMotionList();

        void on_loopPlayback_toggled(bool state);

        void jointListChanged(QListWidgetItem* joint);

        void motionChanged(int);

        void on_spinBoxFPS_valueChanged(double value);

        void on_controlMode_changed(int controlMode);

        void on_enableRobotPoseUnit_toggled(bool state);

    protected:
        void connectSlots();

        std::string configFile;
        Ui::MMMPlayerGuiPlugin ui;
        MMMPlayerInterfacePrx MMMLoader;
        TrajectoryPlayerInterfacePrx trajPlayer;
    private:
        QPointer<QWidget> __widget;
        QPointer<MMMPlayerConfigDialog> dialog;
        QFileDialog* fileDialog2;
        QListWidget* jointList;

        QTimer* updateTimer;
        bool isComponentCreated;
    };
    //using MMMPlayerGuiPluginPtr = boost::shared_ptr<MMMPlayerWidget>;
}

