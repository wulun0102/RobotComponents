/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "MMMPlayerConfigDialog.h"
#include <RobotComponents/gui-plugins/MMMPlayerPlugin/ui_MMMPlayerConfigDialog.h>
#include <IceUtil/UUID.h>

#include <QPushButton>
#include <QMessageBox>

#include <RobotAPI/interface/core/RobotState.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <filesystem>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <RobotComponents/interface/components/MMMPlayerInterface.h>
#include <RobotAPI/interface/components/TrajectoryPlayerInterface.h>


armarx::MMMPlayerConfigDialog::MMMPlayerConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::MMMPlayerConfigDialog)
{
    ui->setupUi(this);
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfiguration()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);

    kinematicUnitComponentProxyFinder = new IceProxyFinder<KinematicUnitInterfacePrx>(this);
    kinematicUnitComponentProxyFinder->setSearchMask("KinematicUnit*");
    ui->verticalLayout->addWidget(kinematicUnitComponentProxyFinder);

    mmmPlayerComponentProxyFinder = new IceProxyFinder<MMMPlayerInterfacePrx>(this);
    mmmPlayerComponentProxyFinder->setSearchMask("MMMPlayer*");
    ui->verticalLayout->addWidget(mmmPlayerComponentProxyFinder);

    trajPlayerComponentProxyFinder = new IceProxyFinder<TrajectoryPlayerInterfacePrx>(this);
    trajPlayerComponentProxyFinder->setSearchMask("TrajectoryPlayer*");
    ui->verticalLayout->addWidget(trajPlayerComponentProxyFinder);
    needtoCreate = false;
}

void armarx::MMMPlayerConfigDialog::onInitComponent()
{
    kinematicUnitComponentProxyFinder->setIceManager(this->getIceManager());
    mmmPlayerComponentProxyFinder->setIceManager(this->getIceManager());
    trajPlayerComponentProxyFinder->setIceManager(this->getIceManager());
}

void armarx::MMMPlayerConfigDialog::onConnectComponent()
{

}

void armarx::MMMPlayerConfigDialog::onExitComponent()
{

    QObject::disconnect();
}

armarx::MMMPlayerConfigDialog::~MMMPlayerConfigDialog()
{
    delete ui;
}


void armarx::MMMPlayerConfigDialog::verifyConfiguration()
{
    if (kinematicUnitComponentProxyFinder->getSelectedProxyName().trimmed().length() == 0)
    {
        QMessageBox::critical(this, "Invalid Configuration", "The proxy name must not be empty");
    }
    else
    {
        if (mmmPlayerComponentProxyFinder->getSelectedProxyName().trimmed().length() == 0)
        {
            needtoCreate = ui->needCreated->isChecked();
        }

        kinematicTopicName = ui->topicNameEditor->text().toStdString();

        this->accept();
    }
}

void armarx::MMMPlayerConfigDialog::reject()
{
    if (getIceManager())
    {
        getIceManager()->removeObject("MMMPlayerConfigDialog");
    }

    QDialog::reject();

}

