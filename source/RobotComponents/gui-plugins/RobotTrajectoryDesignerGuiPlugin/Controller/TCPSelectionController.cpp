/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::TCPSelectionController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "TCPSelectionController.h"
#include "../Environment.h"

namespace armarx
{
    void TCPSelectionController::onInitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TCPSelectionController on init";

        currentTCPCombobox->clear();
        currentTCPCombobox->setEnabled(false);

        // Set strong focus and add wheel event filter
        currentTCPCombobox->setFocusPolicy(Qt::StrongFocus);
        currentTCPCombobox->installEventFilter(new WheelEventFilter(this));
    }

    void TCPSelectionController::onConnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TCPSelectionController on connect";

        // Select trajectory: update selected trajectory
        QObject::connect(currentTCPCombobox, SIGNAL(activated(int)),
                         this, SLOT(updateSelectedTCP(int)));
    }

    void TCPSelectionController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TCPSelectionController on disconnect";
    }

    void TCPSelectionController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TCPSelectionController on exit";
    }

    TCPSelectionController::TCPSelectionController(QComboBox* currentTCPCombobox) :
        currentTCPCombobox(currentTCPCombobox)
    {
        onInitComponent();
        onConnectComponent();
    }

    QComboBox* TCPSelectionController::getCurrentTCPCombobox()
    {
        return this->currentTCPCombobox;
    }

    void TCPSelectionController::setCurrentTCPCombobox(QComboBox* currentTCPCombobox)
    {
        if (currentTCPCombobox != NULL)
        {
            this->currentTCPCombobox = currentTCPCombobox;
        }
    }

    void TCPSelectionController::updateSelectedTCP(int index)
    {
        if (index >= 0)
        {
            emit changedSelectedTCP(this->currentTCPCombobox->currentText());
        }
    }

    void TCPSelectionController::updateSelectedTCP(QString trajectory)
    {
        int index = currentTCPCombobox->findText(trajectory);
        if (index != -1)
        {
            currentTCPCombobox->setCurrentIndex(index);
            emit changedSelectedTCP(trajectory);
        }
    }

    void TCPSelectionController::addTrajectory(QString trajectory)
    {
        if (trajectory != NULL)
        {
            if (this->currentTCPCombobox->findText(trajectory) == -1)
            {
                this->currentTCPCombobox->addItem(trajectory);
                if (this->currentTCPCombobox->count() == 1)
                {
                    this->currentTCPCombobox->setEnabled(true);
                }
            }
        }
    }

    void TCPSelectionController::removeTrajectory(QString trajectory)
    {
        int index = currentTCPCombobox->findText(trajectory);

        if (index != -1)
        {
            currentTCPCombobox->removeItem(index);
        }
        if (currentTCPCombobox->count() == 0)
        {
            currentTCPCombobox->setEnabled(false);
        }
    }

    void TCPSelectionController::enableSelectedTCP(bool enable)
    {
        this->currentTCPCombobox->setEnabled(enable);
    }

    void TCPSelectionController::removeAllTrajectories()
    {
        currentTCPCombobox->clear();
        currentTCPCombobox->setEnabled(false);
    }
}
