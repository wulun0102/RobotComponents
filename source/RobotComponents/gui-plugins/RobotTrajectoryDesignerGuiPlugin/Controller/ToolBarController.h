/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::ToolBarController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef TOOLBARCONTROLLER_H
#define TOOLBARCONTROLLER_H
#include "AbstractController.h"
#include "../View/ToolBar.h"

namespace armarx
{
    /**
     * @class ToolBarController
     * @brief Subcontroller which handles all user interaction with the tool
     *          bar in the GUI, communicates with other controllers via signals
     *          and slots
     */
    class ToolBarController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onExitComponent() override;

        /**
         * @brief Creates a new ToolBarController and assigns a ToolBar to handle
         * @param guiToolbar Pointer to the ToolBar whose user interaction
         *                         is handled by the ToolBarController
         */
        ToolBarController(ToolBarPtr guiToolbar);

        /**
         * @brief Getter for the ToolBar pointer to guiToolbar
         * @return A Pointer to the guiToolbar
         */
        ToolBarPtr getGuiToolbar();

        /**
         * @brief Setter for the ToolBar pointer to guiToolbar
         * @param guiToolbar Pointer to the ToolBar whose user interaction
         *                      is handled by the ToolBarController
         */
        void setGuiToolbar(ToolBarPtr guiToolbar);

        /**
         * @brief Getter for the index of the currently selected waypoint
         * @return Index of the currently selected waypoint
         */
        int getCurrentWaypoint();

    public slots:
        /**
         * @brief Setter for the index of the currently selected waypoint
         * @param currentWaypoint Index of the currently selected waypoint
         */
        void setCurrentWaypoint(int currentWaypoint);

        /**
         * @brief Retranslates the guiToolbar
         */
        void retranslateGui();

        /**
         * @brief Enables or disables the delete and change button
         * @param enable Determines whether to enable or disable the delete button
         */
        void enableDeleteChangeButton(bool enable);

        /**
         * @brief Enables or disables the add button
         * @param enable Determines whether to enable or disable the add button
         */
        void enableAddButton(bool enable);

        /**
         * @brief Enables or disables the prview button
         * @param enable Determines whether to enable or disable the previes button
         */
        void enablePreviewButton(bool enable);

        /**
         * @brief Enables or disables the prview all button
         * @param enable Determines whether to enable or disable the previes button
         */
        void enablePreviewAllButton(bool enable);

        /**
         * @brief Enables or disables the prview all button
         * @param enable Determines whether to enable or disable the previes button
         */
        void enableStopButton(bool enable);


    private slots:
        /**
         * @brief Adds a new waypoint at the current position of the TCP control
         *          unit after the last waypoint
         */
        void setWaypoint();

        /**
         * @brief Deletes the currently selected waypoint
         */
        void deleteWaypoint();

        /**
         * @brief Changes the currently selected waypoint
         */
        void changeWaypoint();

        /**
         * @brief Plays a preview of all trajectories
         */
        void playAllPreview();

        /**
         * @brief Plays a preview of the current trajectory
         */
        void playPreview();

        void stopPreview();

    signals:
        /**
         * @brief Notifies other controllers about the addition of a waypoint
         *          at the current position of the TCP control unit after the
         *          last waypoint
         * @param constraints Constraints of the Waypoint: InsertAfter
         */
        void addedWaypoint(int index, bool insertAfter = true);

        /**
         * @brief Notifies other controllers about the deletion of the currently
         *          selected waypoint
         * @param index Index of the currently selected waypoint
         */
        void deletedWaypoint(int index);

        /**
         * @brief Notifies other controllers about the change of the currently
         *          selected waypoint
         * @param index Index of the currently selected waypoint
         */
        void changeWaypoint(int index);

        /**
         * @brief Plays the preview of all trajectories
         */
        void notifyAllPreview();


        /**
         * @brief Plays the preview of the current trajectory
         */
        void notifyPreview();

        /**
         * @brief stop the preview of the trajectories
         */
        void notifyStopPreview();

    private:
        ToolBarPtr guiToolbar;
        int currentWaypoint;
    };

    using ToolBarControllerPtr = std::shared_ptr<ToolBarController>;
}

#endif // TOOLBARCONTROLLER_H
