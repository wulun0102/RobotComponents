#include "ImportDialogController.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

namespace armarx
{
    void ImportDialogController::onInitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ExportDialogController on init";
    }

    void ImportDialogController::onConnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ExportDialogController on connect";
    }

    void ImportDialogController::onDisconnectComponent()
    {

        ARMARX_INFO << "RobotTrajectoryDesigner: ExportDialogController on disconnect";
    }

    void ImportDialogController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ExportDialogController on exit";
    }

    ImportDialogController::ImportDialogController()
    {
        onInitComponent();
        onConnectComponent();
    }

    void ImportDialogController::open()
    {
        if (environment == NULL)
        {
            return;
        }
        QFileDialog dialog(0);
        dialog.setFileMode(QFileDialog::ExistingFiles);
        dialog.setAcceptMode(QFileDialog::AcceptOpen);
        dialog.setNameFilter(tr("XML (*.xml)"));
        QStringList fileNames;
        if (dialog.exec())
        {
            fileNames = dialog.selectedFiles();
        }
        if (fileNames.length() > 1)
        {
            QMessageBox* toManyFilesSelected = new QMessageBox;
            toManyFilesSelected->setWindowTitle(QString::fromStdString("Information"));
            toManyFilesSelected->setText(QString::fromStdString("To many files selected.\nAs default the first file was taken."));
            toManyFilesSelected->exec();
        }
        if (!fileNames.empty())
        {
            try
            {
                MMMImporterPtr mmmImporter = std::make_shared<MMMImporter>(environment);
                std::vector<DesignerTrajectoryPtr> trajectories = mmmImporter->importTrajectory(fileNames[0].toStdString());
                if (!trajectories.empty())
                {
                    for (DesignerTrajectoryPtr trajectory : trajectories)
                    {
                        emit import(trajectory);
                    }
                }
                else
                {
                    helpExceptionMessageBox("Import failed.");
                }
            }
            catch (InvalidArgumentException e)
            {
                helpExceptionMessageBox(e.reason);
            }
            catch (std::exception e)
            {
                helpExceptionMessageBox("Could not import MMM file");
            }
        }
    }

    void ImportDialogController::environmentChanged(EnvironmentPtr environment)
    {
        this->environment = environment;
    }

    void ImportDialogController::helpExceptionMessageBox(std::string errorMessage)
    {
        QMessageBox* errorMessageBox = new QMessageBox;
        errorMessageBox->setWindowTitle(QString::fromStdString("Error Message"));
        errorMessageBox->setText(QString::fromStdString(errorMessage));
        errorMessageBox->exec();
    }
}
