#ifndef TRAJECTORYCONTROLLER_H
#define TRAJECTORYCONTROLLER_H
#include "AbstractController.h"
#include "../Manager/DesignerTrajectoryHolder.h"
#include "../Manager/DesignerTrajectoryManager.h"

namespace armarx
{
    /**
     * @class TrajectoryController
     * @brief Subcontroller which handles all program interaction with the modle,
     *        communicates with other controllers via signals and slots
     */
    class TrajectoryController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent() override;

        /**
         * @brief @see AbstractController
         */
        void onExitComponent() override;

        /**
         * @brief Creates a new TrajectoryController and assigns a QWidget to handle
         */
        TrajectoryController();

    public slots:
        /**
         * @brief Updates the selected TCP
         * @param index Index of the selected TCP
         */
        void updateTCP(QString tcp);

        /**
         * @brief Updates all values of a given transition
         * @param transition Index of the transition
         * @param it Index of the interpolation of the transition
         */
        void updateTransition(int transition, int it);

        /**
         * @brief Updates all values of a given transition
         * @param transition Index of the transition
         * @param duration Duration of the transition
         */
        void updateTransition(int transition, double duration);

        /**
         * @brief Updates all values of a given waypoint
         * @param waypoint Index of the waypoint
         * @param values Array containing the x, y, z, coordinates of a waypoint as well as the
         *                  r, p, y euler angles
         */
        void updateWaypoint(int waypoint, std::vector<double> values);


        /**
         * @brief Updates all values of a given waypoint
         * @param waypoint Index of the waypoint
         */
        void updateWaypoint(int waypoint);

        /**
         * @brief Updates all values of a given waypoint
         * @param waypoint Index of the waypoint
         * @param newPoseBase Is the new PoseBase of the waypoint
         */
        void updateWaypoint(int waypoint, PoseBasePtr newPoseBase);

        /**
         * @brief Updates CartesianSelection value of a given waypoint
         * @param waypoint Index of the waypoint
         * @param cartesianSelection Integer representing the cartesian selection
         *                      or before
         */
        void updateWaypoint(int waypoint, int cartesianSelection);

        /**
         * @brief Updates isBreakpoint value of a given waypoint
         * @param waypoint Index of the waypoint
         * @param isBreakpoint Boolean determining whether the waypoint is a breakpoint
         */
        void updateWaypoint(int waypoint, bool isBreakpoint);

        /**
         * @brief Adds a new waypoint relative to the given waypoint
         * @param constraints Constraints of the new waypoint: isBreakpoint, ikConstraints,
         *                      insertBefore/-After
         * @param waypoint Index of the waypoint
         * @param cartesianSelection Integer representing the cartesian selection
         * @param insertAfter Boolean determining whether to insert after the waypoint
         *                      or before
         * @param isBreakpoint Boolean determining whether the waypoint is a breakpoint
         */
        void addWaypoint(int waypoint, bool insertAfter);

        /**
         * @brief Deletes the given waypoint
         * @param waypoint Index of the waypoint
         */
        void deleteWaypoint(int waypoint);

        /**
         * @brief Imports a given trajectory into the model
         * @param designerTrajectory Trajectory which is being imported
         */
        void import(DesignerTrajectoryPtr designerTrajectory);
        /*
         * Changed signature because 'export' is a keyword and thus cannot be user
         * as method name
         */
        /**
         * @brief Stages the given trajectory for export
         * @param fps for export to MMM
         */
        void exportTrajectory(int fps);

        /**
         * @brief Stages the given trajectory for export
         */
        void exportTrajectory();

        /**
         * @brief Undo trajectroy
         */
        void undo();

        /**
         * @brief Redo trajectory
         */
        void redo();

        /**
         * @brief Play all trajectories
         */
        void playTrajectories();

        /**
         * @brief Set the enviroment
         * @param The new enviroment
         */
        void environmentChanged(EnvironmentPtr environment);

        /**
         * @brief Sets the active collision model name
         * @param activeColModelName Active collision model name
         */
        void setActiveColModelName(QString activeColModelName);

        /**
         * @brief Sets the body collision models names
         * @param activeColModelName Body collision models names
         */
        void setBodyColModelsNames(QStringList bodyColModelsNames);

    signals:
        /**
         * @brief Notfies other controllers about changes of an existing waypoint
         * @param values Array containting x, y, z, coordinates as well as a, b, g
         *               euler angles of the waypoint
         * @param constraints Constraints of the waypoint: isBreakpoint, ikConstraints
         * @param waypoint Index of the waypoint
         */
        void changeWaypointGui(int index, std::vector<double> values, int cartesianSelection,
                               bool isBreakpoint);

        /**
         * @brief Notifies other controllers about changes of an existing transition
         * @param duration Duration of the transition
         * @param start Start time of the transition
         * @param it Index of interpolation of the transition
         * @param transition Index of the transition
         */
        void changeTransitionGui(int transition, double duration, double start, int it);

        /**
         * @brief Notifies other controllers about the addition of a waypoint
         * @param values Array containting x, y, z, coordinates as well as r, p, y
         *               euler angles of the waypoint
         * @param constraints Constraints of the waypoint: isBreakpoint, ikConstraints
         * @param waypoint Index of the waypoint
         */
        void addWaypointGui(int waypoint, std::vector<double> values, int cartesianSelection, bool isBreakpoint);

        /**
         * @brief Notifies other controllers about the deletion of a given waypoint
         * @param index Index of the waypoitn
         */
        void removeWaypointGui(int index);

        /**
         * @brief Notifies other controllers about the addition of a transition
         * @param duration Duration of the transition
         * @param start Start time of the transition
         * @param it Index of the interpolation of the transition
         * @param transition Index of the transition
         */
        void addTransitionGui(int transition, double duration, double start, int it);

        /**
         * @brief Notifies other controllers about the deletion of a given transtion
         * @param index Index of the transition
         */
        void removeTransitionGui(int index);

        /**
         * @brief Notifies WaypointTab and Transition to delete all ListWidgets
         */
        void removeTransitionWaypointGui();

        /**
         * @brief Notifies the VisualizationController about a changed RobotNodeSet
         * @param rns new RobotNodeSet
         */
        void rnsChanged(VirtualRobot::RobotNodeSetPtr rns);

        /**
         * @brief Notifies the TCPSelcetionController about a new Trajectory
         * @param trajectory new trajectory
         */
        void newTrajectory(QString trajectory);

        /**
         * @brief Removes the given trajectory from the TCP selection combo box
         * @param trajectory String identifier of the trajectory
         */
        void removeTrajectory(QString trajectory);

        /**
         * @brief Removes all trajectories from the TCP selection combo box
         */
        void removeAllTrajectories();

        /**
         * @brief Updates the currently displayed trajectory of the corresponding TCP
         * @param trajectory String identifier of the trajectory
         */
        void updateSelectedTCP(QString trajectory);

        /**
         * @brief Provides the trajectory for VisualizationController
         * @param designerTrajectory Trajectory to provide
         */
        void showTrajectory(DesignerTrajectoryPtr designerTrajectory);

        /**
         * @brief Provides all trajectories for VisualizationController
         * @param designerTrajectories Trajectories to play
         */
        void playTrajectories(std::vector<DesignerTrajectoryPtr> designerTrajectories);

        /**
         * @brief Provides the trajectory for ExportController
         * @param trajectory Trajectory to provide
         */
        void exportTrajectory(std::vector<DesignerTrajectoryPtr> trajectories);

        /**
         * @brief cartesianSelectionChanged recalculates the configuration of the robot joints to fit the new selection
         * @param cs the new CartesianSelection
         */

        void cartesianSelectionChanged(VirtualRobot::IKSolver::CartesianSelection cs);
        /**
         * @brief Enables or disables the redo button/shortcut
         * @param enable Determines whether to enable or disable the redo button/shortcut
         */
        void enableRedo(bool enable);

        /**
         * @brief Enables or disables the undo button/shortcut
         * @param enable Determines whether to enable or disable the undo button/shortcut
         */
        void enableUndo(bool enable);

        /**
         * @brief Enables or disables the new IK solution button
         * @param enable Determines whether to enable or disable the ik solution button
         */
        void enableIKSolutionButton(bool enable);

        /**
         * @brief Enables or disables the export buttons
         * @param enable Determines whether to enable or disable the export buttons
         */
        void enableExportButtons(bool enable);

        /**
         * @brief Enables or disables the delete and change button/shortcut
         * @param enable Determines whether to enable or disable the delete and change button/shortcut
         */
        void enableDeleteChange(bool enable);

        /**
         * @brief Enables or disables the add button/shortcut
         * @param enable Determines whether to enable or disable the add button/shortcut
         */
        void enableAdd(bool enable);

        /**
         * @brief Enables or disables the prview button/shortcut
         * @param enable Determines whether to enable or disable the previes button/shortcut
         */
        void enablePreview(bool enable);

        /**
         * @brief Enables or disables the prview all button/shortcut
         * @param enable Determines whether to enable or disable the previes button/shortcut
         */
        void enablePreviewAll(bool enable);

    private:
        /**
         * @brief Converts a PoseBase to x, y, z coordinates and r, p, y euler angles
         * @param pose PoseBase to convert
         * @return Vector containing the resulting coordinates and angles
         */
        std::vector<double>parseToCoordinate(PoseBasePtr pose);

        PoseBasePtr parseToPoseBasePtr(std::vector<double> values);

        void helpAddWaypointGui(UserWaypointPtr waypoint, int waypointIndex);
        void helpAddTransitionGui(TransitionPtr transition, int transitionIndex);
        void addTransitionWaypointGui(DesignerTrajectoryPtr trajectory);

        void helpChangeTransitionGui(TransitionPtr transition, int transitionIndex);
        void helpChangeWaypointGui(UserWaypointPtr waypoint, int waypointIndex);
        void changeTransitionWaypointGui(DesignerTrajectoryPtr trajectory);

        void helpImportDesignerTrajectory(DesignerTrajectoryPtr designerTrajectory);

        void helpCreateDesignerTrajectoryManager();

        bool helpEnableExportPreviewAll();
        void helpEnableUndoRedo();
        void helpEnableButtons();

        void helpExceptionMessageBox(std::string errorMessage);

        void helpCollision();

        DesignerTrajectoryManagerPtr manager;
        DesignerTrajectoryHolderPtr  holder;
        VirtualRobot::RobotNodeSetPtr rns;
        EnvironmentPtr environment;
        std::string activeColModelName;
        std::vector<std::string> bodyColModelsNames;
    };

    using TrajectoryControllerPtr = std::shared_ptr<TrajectoryController>;
}

#endif // TRAJECTORYCONTROLLER_H
