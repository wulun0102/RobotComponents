/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::TCPInformationController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "TCPInformationController.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
namespace armarx
{

    void TCPInformationController::onInitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TCPInformationController on init";
        clearPoseLabels();
    }

    void TCPInformationController::onConnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TCPInformationController on connect";
    }

    void TCPInformationController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TCPInformationController on disconnect";
    }

    void TCPInformationController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: TCPInformationController on exit";
    }

    TCPInformationController::TCPInformationController(TCPInformationPtr guiTCPInformation) :
        guiTCPInformation(guiTCPInformation)
    {
        onInitComponent();
        onConnectComponent();
    }

    TCPInformationPtr TCPInformationController::getGuiTCPInformation()
    {
        return this->guiTCPInformation;
    }

    void TCPInformationController::setGuiTCPInformation(TCPInformationPtr guiTCPInformation)
    {
        if (guiTCPInformation != NULL)
        {
            this->guiTCPInformation = guiTCPInformation;
        }
    }


    void TCPInformationController::setCurrentPose(Eigen::Matrix4f pose)
    {
        setLineEditText(guiTCPInformation->getTCPInformationTab()->currentPoseLayout,
                        pose);
    }

    void TCPInformationController::setDesiredPose(Eigen::Matrix4f pose)
    {
        setLineEditText(guiTCPInformation->getTCPInformationTab()->desiredPoseLayout,
                        pose);

    }

    void TCPInformationController::setReachable(bool reachable)
    {
        std::string reachableString = "";
        if (reachable)
        {
            reachableString = "Pose reachable!";
        }
        else
        {
            reachableString = "Pose  NOT reachable!";
        }
        guiTCPInformation->getTCPInformationTab()->reachableLabel->setText(QString::fromStdString(reachableString));
    }

    void TCPInformationController::clearPoseLabels()
    {
        Eigen::Matrix4f zero;
        setLineEditText(guiTCPInformation->getTCPInformationTab()->currentPoseLayout,
                        zero);
        setLineEditText(guiTCPInformation->getTCPInformationTab()->desiredPoseLayout,
                        zero);
    }

    void TCPInformationController::retranslateGui()
    {
        throw ("not yet implemented");
    }

    void TCPInformationController::setLineEditText(QGridLayout* gridLayout, Eigen::Matrix4f pose)
    {
        int columns = gridLayout->columnCount();
        int rows = gridLayout->rowCount();

        /*
         * check if pose is empty,
         * check if pose has correct size
         */
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                // get layout item at position
                QLayoutItem* item = gridLayout->itemAtPosition(j, i);
                if (item != NULL)
                {
                    // cast item dynamically to line edit
                    QWidget* widget = item->widget();
                    QLabel* lineEdit = dynamic_cast<QLabel*>(widget);

                    // Parse (i, j) to list index and set text
                    if (lineEdit)
                    {
                        std::stringstream stream;
                        stream << std::fixed << std::setprecision(2) << pose(j, i);
                        std::string s = stream.str();
                        lineEdit->setText(QString::fromStdString(s));
                    }
                }
            }
        }
    }

}
