#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::Exportdialogcontroller
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../ExportDialogController.h"
#include "../../View/ExportDialog.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with export dialog and controller");

    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    ExportDialog exportDialog;
    //ExportDialogControllerPtr exportDialogController(new ExportDialogController(environment));
};

BOOST_FIXTURE_TEST_CASE(ExportDialogControllerTest, F)
{

}
