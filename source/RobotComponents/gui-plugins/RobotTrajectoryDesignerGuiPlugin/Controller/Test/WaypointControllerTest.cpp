#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::Waypointcontroller
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../WaypointController.h"
#include "../../View/WaypointTab.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with waypoint tab and controller");

    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    int i;
    WaypointTabPtr waypointTab;
    WaypointControllerPtr waypointController;
};

BOOST_FIXTURE_TEST_CASE(WaypointControllerTest, F)
{

}
