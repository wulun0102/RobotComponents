#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::Importdialogcontroller
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../ImportDialogController.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with import dialog and controller");

    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    ImportDialogControllerPtr importDialogController;
};

BOOST_FIXTURE_TEST_CASE(ImportDialogControllerTest, F)
{

}
