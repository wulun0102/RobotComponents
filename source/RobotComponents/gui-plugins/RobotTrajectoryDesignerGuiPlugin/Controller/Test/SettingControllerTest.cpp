#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::SettingController
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../SettingController.h"
#include "ui_SettingTab.h"
#include <QApplication>
#include <QWidget>
#include <QComboBox>
#include <QMainWindow>
#include "../View/SettingTab.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with setting tab and controller");
        i = 0;
        //settingTab = make_shared<SettingTab>(0);
        //settingController = make_shared<SettingController>(settingTab);
        //tcp = settingTab->getSettingTab()->tcpComboBox;
    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    int i;
    //SettingTabPtr settingTab;
    //SettingControllerPtr settingController;
    //QComboBox* tcp;
};

BOOST_FIXTURE_TEST_CASE(initTCPComboBoxTest, F)
{
    RobotPtr robot = RobotIO::loadRobot(
                         "../../../../../data/RobotTrajectoryDesigner/Resources/ArmarIII.xml");
    Environment* environment = new Environment();
    environment->setRobot(robot);
    //    Environment::setRobot(robot);
    //    if (environment->getRobot() == NULL)
    //    {
    //        BOOST_TEST_MESSAGE("Robot is NULL");
    //    }
    //cout << environment->getRobot()->getRobotNodeSets().size() << endl;
    //BOOST_CHECK(tcp->count() > 0);
    //QStringList* rns = new QStringList;
    //for (int i = 0; i < tcp->count(); i++)
    //{
    //    rns->append(tcp->itemText(i));
    //}

    //    BOOST_CHECK(rns->contains(QString::fromStdString("Torso")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("TorsoHeadColModel")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("PlatformTorsoColModel")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("PlatformTorsoHeadColModel")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("TorsoLeftArm")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("TorsoRightArm")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("TorsoBothArms")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("Robot")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("PlatformYawTorsoLeftArm")));
    //    BOOST_CHECK(rns->contains(QString::fromStdString("PlatformTorsoRightArm")));
}

