#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::Transitioncontroller
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../TransitionController.h"
#include "../../View/TransitionTab.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with transition tab and controller");

    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    int i;
    TransitionTabPtr transitionTab;
    TransitionControllerPtr transitionController;
};

BOOST_FIXTURE_TEST_CASE(TransitionControllerTest, F)
{

}
