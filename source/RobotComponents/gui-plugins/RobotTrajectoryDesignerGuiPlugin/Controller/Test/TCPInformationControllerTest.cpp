#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::Controller::TCPInformationcontroller
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"

#include "../TCPInformationController.h"
#include "../../View/TCPInformationTab.h"
#include <memory>
#include <VirtualRobot/XML/RobotIO.h>
#include <iostream>

using namespace armarx;
using namespace VirtualRobot;
using namespace std;

struct F
{
    F()
    {
        BOOST_TEST_MESSAGE("Setup fixture with TCP information tab and controller");

    }
    ~F()
    {
        BOOST_TEST_MESSAGE("Teardown fixture");
    }

    TCPInformationPtr tcpInformation;
    TCPInformationControllerPtr tcpInformationController;
};

BOOST_FIXTURE_TEST_CASE(TCPInformationControllerTest, F)
{

}
