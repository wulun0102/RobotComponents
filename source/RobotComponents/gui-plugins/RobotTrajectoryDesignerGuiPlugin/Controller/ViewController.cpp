/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::ViewController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "ViewController.h"

namespace armarx
{
    void ViewController::onInitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ViewController on disconnect";

        // Fill perspectives combo box with items
        this->initPerspectivesCombobox();
        numberViews = 1;
        enableAddRemoveViewButton();
    }

    void ViewController::onConnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ViewController on connect";

        // Set perspective (index)
        QObject::connect(guiPerspectives->getPerspectives()->perspectiveComboBox,
                         SIGNAL(activated(int)),
                         this, SLOT(setViewPerspective(int)));

        QObject::connect(guiPerspectives->getPerspectives()->addViewButton,
                         SIGNAL(clicked()), this, SLOT(addViewSlot()));
        QObject::connect(guiPerspectives->getPerspectives()->deleteViewButton,
                         SIGNAL(clicked()), this, SLOT(removeViewSlot()));
    }

    void ViewController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ViewController on disconnect";
    }

    void ViewController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: ViewController on exit";
    }

    ViewController::ViewController(PerspectivesPtr guiPerspectives) :
        guiPerspectives(guiPerspectives)
    {
        onInitComponent();
        onConnectComponent();
    }

    PerspectivesPtr ViewController::getGuiPerspectives()
    {
        return this->guiPerspectives;
    }

    void ViewController::setGuiPerspectives(PerspectivesPtr guiPerspectives)
    {
        this->guiPerspectives = guiPerspectives;
    }

    void ViewController::setViewPerspective(int index)
    {
        emit changedPerspective(index);
    }

    void ViewController::retranslateGui()
    {
        throw ("not yet implemented");
    }

    void ViewController::addViewSlot()
    {
        numberViews++;
        emit addView();
        enableAddRemoveViewButton();
    }

    void ViewController::removeViewSlot()
    {
        numberViews--;
        emit removeView();
        enableAddRemoveViewButton();
    }

    void ViewController::initPerspectivesCombobox()
    {
        QComboBox* perspectives = guiPerspectives->getPerspectives()->perspectiveComboBox;

        // Set focus to strong focus, add wheel event filter
        perspectives->setFocusPolicy(Qt::StrongFocus);
        perspectives->installEventFilter(new WheelEventFilter(this));

        // Clear combo box, fill with items
        perspectives->clear();
        perspectives->addItem(QString::fromStdString("High Angle"));
        perspectives->addItem(QString::fromStdString("Top"));
        perspectives->addItem(QString::fromStdString("Front"));
        perspectives->addItem(QString::fromStdString("Back"));
        perspectives->addItem(QString::fromStdString("Left"));
        perspectives->addItem(QString::fromStdString("Right"));
        perspectives->setEnabled(true);
        perspectives->setCurrentIndex(0);
    }

    void ViewController::enableAddRemoveViewButton()
    {
        if (numberViews == 1)
        {
            guiPerspectives->getPerspectives()->addViewButton->setEnabled(true);
            guiPerspectives->getPerspectives()->deleteViewButton->setEnabled(false);
        }
        else if (numberViews == 4)
        {
            guiPerspectives->getPerspectives()->addViewButton->setEnabled(false);
            guiPerspectives->getPerspectives()->deleteViewButton->setEnabled(true);
        }
        else
        {
            guiPerspectives->getPerspectives()->addViewButton->setEnabled(true);
            guiPerspectives->getPerspectives()->deleteViewButton->setEnabled(true);
        }
    }
}
