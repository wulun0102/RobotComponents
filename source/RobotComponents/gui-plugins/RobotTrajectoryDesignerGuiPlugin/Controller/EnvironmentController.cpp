#include "EnvironmentController.h"
#include <string>
#include <QFileDialog>
#include <VirtualRobot/XML/RobotIO.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/application/Application.h>
#include "../KinematicSolver.h"
#include <QMessageBox>

namespace armarx
{
    void armarx::EnvironmentController::onInitComponent()
    {
        environment = std::make_shared<Environment>();
        ARMARX_INFO << "RobotTrajectoryDesigner: EnvironmentController on init";
    }

    void EnvironmentController::onConnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: EnvironmentController on connect";
    }

    void EnvironmentController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: EnvironmentController on disconnect";
    }

    void EnvironmentController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: EnvironmentController on exit";
    }

    void EnvironmentController::loadArmar3()
    {
        std::string robotFile = "/RobotAPI/robots/Armar3/ArmarIII.xml";
        CMakePackageFinder finder("RobotAPI");

        if (finder.packageFound())
        {
            environment->setRobot(VirtualRobot::RobotIO::loadRobot(finder.getDataDir() + robotFile));
        }
        else
        {
            ARMARX_WARNING << "ArmarX Package RobotAPI has not been found!";
        }
        emit environmentChanged(environment);
    }

    void EnvironmentController::loadRobotFromProxy(std::string robotFileName)
    {
        environment->setRobot(VirtualRobot::RobotIO::loadRobot(robotFileName));
        emit environmentChanged(environment);
    }

    EnvironmentController::EnvironmentController()
    {
        onInitComponent();
        onConnectComponent();
    }

    void EnvironmentController::openRobotFileDialog()
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(0, "Select robot", "If you change the robot all trajectories are lost.\nDo you want to change?", QMessageBox::Yes | QMessageBox::No);
        if (reply == QMessageBox::No)
        {
            return;
        }
        QFileDialog dialog(0);
        dialog.setFileMode(QFileDialog::ExistingFiles);
        dialog.setAcceptMode(QFileDialog::AcceptOpen);
        dialog.setNameFilter(tr("XML (*.xml)"));
        QStringList fileNames;

        if (dialog.exec())
        {
            fileNames = dialog.selectedFiles();
        }
        if (fileNames.length() > 1)
        {
            QMessageBox* toManyFilesSelected = new QMessageBox;
            toManyFilesSelected->setWindowTitle(QString::fromStdString("Information"));
            toManyFilesSelected->setText(QString::fromStdString("To many files selected.\nAs default the first file was taken."));
            toManyFilesSelected->exec();
        }
        if (!fileNames.empty())
        {
            try
            {
                VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(fileNames[0].toStdString());
                environment->setRobot(robot);
                emit environmentChanged(environment);
                KinematicSolver::reset();
                KinematicSolver::getInstance(NULL, robot);
            }
            catch (std::exception& e)
            {
                QMessageBox* selectRobotFailed = new QMessageBox;
                selectRobotFailed->setWindowTitle(QString::fromStdString("Error Message"));
                selectRobotFailed->setText(QString::fromStdString("The selected file is not a robot xml file."));
                selectRobotFailed->exec();
            }
        }
    }
}
