/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::MementoController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "MementoController.h"

namespace armarx
{
    void MementoController::onInitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: MementoController on init";

        enableUndoButton(false);
        enableRedoButton(false);
    }

    void MementoController::onConnectComponent()
    {
        // Undo lastly executed operation
        QObject::connect(undoButton, SIGNAL(clicked()), this, SLOT(undoOperation()));

        // Redo lastly undone operation
        QObject::connect(redoButton, SIGNAL(clicked()), this, SLOT(redoOperation()));
    }

    void MementoController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: MementoController on disconnect";
    }

    void MementoController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: MementoController on exit";
    }

    MementoController::MementoController(QPushButton* undoButton, QPushButton* redoButton) :
        undoButton(undoButton),
        redoButton(redoButton)
    {
        onInitComponent();
        onConnectComponent();
    }

    QPushButton* MementoController::getUndoButton()
    {
        return this->undoButton;
    }

    QPushButton* MementoController::getRedoButton()
    {
        return this->redoButton;
    }

    void MementoController::undoOperation()
    {
        emit undo();
    }

    void MementoController::redoOperation()
    {
        emit redo();
    }

    void MementoController::enableRedoButton(bool enable)
    {
        this->redoButton->setEnabled(enable);
        this->redoBool = enable;
    }

    void MementoController::enableRedoButtonVisualization(bool enable)
    {
        if (redoBool)
        {
            this->redoButton->setEnabled(enable);
        }
    }

    void MementoController::enableUndoButton(bool enable)
    {
        this->undoButton->setEnabled(enable);
    }
}
