/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::SettingController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "SettingController.h"
#include <VirtualRobot/XML/RobotIO.h>
#include "../KinematicSolver.h"
#include <QFileDialog>

namespace armarx
{
    void SettingController::onInitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: SettingController on init";

        // Disable collision model combo box and list, import and export button
        enableWidgets(true);
        enableExportButtons(false);
        enableIKSolutionButton(false);
    }

    void SettingController::onConnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: SettingController on connect";

        // Robot selection button: clicked
        QObject::connect(guiSettingTab->getSettingTab()->robotSelectionButton,
                         SIGNAL(clicked()), this, SLOT(openRobotSelectionDialog()));

        // Shortcut button: clicked
        QObject::connect(guiSettingTab->getSettingTab()->shortcutButton,
                         SIGNAL(clicked()), this, SLOT(openShortcutDialog()));

        // TCP combo box: index changed
        QObject::connect(guiSettingTab->getSettingTab()->tcpComboBox,
                         SIGNAL(activated(int)),
                         this, SLOT(selectTCP(int)));

        // New IK solution button: clicked
        QObject::connect(guiSettingTab->getSettingTab()->newIKSolutionButton,
                         SIGNAL(clicked()), this, SLOT(newIKSolution()));

        // Active collision model changed
        QObject::connect(guiSettingTab->getSettingTab()->collisionModelComboBox,
                         SIGNAL(activated(int)), this, SLOT(selectActiveCM(int)));

        // Other used collision models changed
        QObject::connect(guiSettingTab->getSettingTab()->collisionModelList,
                         SIGNAL(itemChanged(QListWidgetItem*)),
                         this, SLOT(setCollisionModelList(QListWidgetItem*)));

        // Export
        QObject::connect(guiSettingTab->getSettingTab()->convertToMMMButton,
                         SIGNAL(clicked()), this, SLOT(convertToMMMSlot()));

        // Import
        QObject::connect(guiSettingTab->getSettingTab()->importButton,
                         SIGNAL(clicked()), this, SLOT(openImportDialog()));
    }

    void SettingController::onDisconnectComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: SettingController on disconnect";
    }

    void SettingController::onExitComponent()
    {
        ARMARX_INFO << "RobotTrajectoryDesigner: SettingController on exit";
    }

    SettingController::SettingController(SettingTabPtr settingTab) :
        guiSettingTab(settingTab)
    {
        onConnectComponent();
        onInitComponent();
    }

    SettingTabPtr SettingController::getGuiSettingTab()
    {
        return this->guiSettingTab;
    }

    void SettingController::setGuiSettingTab(SettingTabPtr guiSettingTab)
    {
        if (guiSettingTab != NULL)
        {
            this->guiSettingTab = guiSettingTab;
        }
    }

    void SettingController::openRobotSelectionDialog()
    {
        emit openRobotSelection();
    }

    void SettingController::openShortcutDialog()
    {
        emit openShortcut();
    }

    void SettingController::selectTCP(int index)
    {
        /*
         * If index is valid kinematic chain, enable widgets and send signal,
         * else disable widgets
         */
        if (index > 0)
        {
            enableWidgets(true);
            emit changedTCP(this->guiSettingTab->getSettingTab()->
                            tcpComboBox->currentText());
        }
        else
        {
            enableWidgets(false);
        }
    }

    void SettingController::selectTCP(QString tcp)
    {
        QComboBox* combobox = guiSettingTab->getSettingTab()->tcpComboBox;
        int index = combobox->findText(tcp);

        // Check if tcp is valid kinematic chain
        if (index > 0)
        {
            combobox->setCurrentIndex(index);
        }
    }

    void SettingController::selectActiveCM(int index)
    {
        QListWidget* cmList = this->guiSettingTab->getSettingTab()->collisionModelList;
        if (index > 0)
        {
            // Enable all collision model items but active collision model item
            for (int i = 0; i < cmList->count(); i++)
            {
                QListWidgetItem* item = cmList->item(i);
                if (i == (index - 1))
                {
                    item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
                }
                else
                {
                    item->setFlags(item->flags() | Qt::ItemIsEnabled);
                }
            }
            cmList->setEnabled(true);
        }
        else
        {
            cmList->setEnabled(false);
        }
        emit setActiveColModelName(guiSettingTab->getSettingTab()->collisionModelComboBox->itemText(index));
    }

    void SettingController::setCollisionModelList(QListWidgetItem* item)
    {
        if (item != NULL)
        {
            QComboBox* tcp = guiSettingTab->getSettingTab()->tcpComboBox;
            QComboBox* cm = guiSettingTab->getSettingTab()->collisionModelComboBox;

            // Check if tcp is selected and active collision model is selected
            if ((tcp->currentIndex() > 0)
                && (cm->currentIndex() > 0))
            {
                QListWidget* models = guiSettingTab->getSettingTab()->collisionModelList;
                QStringList bodyCollisionModels;

                // Add each checked collision model as robot node set pointer to vector
                for (int i = 0; i < models->count(); i++)
                {
                    if (models->item(i)->checkState() == Qt::Checked)
                    {
                        bodyCollisionModels.push_back(models->item(i)->text());
                    }
                }
                emit setBodyColModelsNames(bodyCollisionModels);
            }
        }
    }

    void SettingController::exportTrajectorySlot()
    {
        emit exportTrajectory();
    }

    void SettingController::convertToMMMSlot()
    {
        emit convertToMMM();
    }

    void SettingController::openImportDialog()
    {
        emit openImport();
    }

    void SettingController::setLanguage(int index)
    {
        /*
        QVariant language = guiSettingTab->getSettingTab()->languageComboBox->
                            itemData(index, Qt::UserRole);
        if (language.isValid())
        {
            emit changedLanguage(language.toString());
        }
        */
    }

    void SettingController::enableIKSolutionButton(bool enable)
    {
        this->guiSettingTab->getSettingTab()->newIKSolutionButton->setEnabled(enable);
    }

    void SettingController::enableExportButtons(bool enable)
    {
        this->guiSettingTab->getSettingTab()->convertToMMMButton->setEnabled(enable);
        //this->guiSettingTab->getSettingTab()->exportTrajectoryButton->setEnabled(enable);
    }

    void SettingController::enableImportTCPCollision(bool enable)
    {
        this->guiSettingTab->getSettingTab()->collisionModelComboBox->setEnabled(enable);
        this->guiSettingTab->getSettingTab()->collisionModelList->setEnabled(enable);
        this->guiSettingTab->getSettingTab()->tcpComboBox->setEnabled(enable);
        this->guiSettingTab->getSettingTab()->importButton->setEnabled(enable);
    }

    void SettingController::newIKSolution()
    {
        VirtualRobot::RobotPtr robot = environment->getRobot();
        KinematicSolverPtr kc = KinematicSolver::getInstance(NULL, robot);
        VirtualRobot::RobotNodeSetPtr activeKinematicChain = robot->getRobotNodeSet(guiSettingTab->getSettingTab()->tcpComboBox->currentText().toStdString());
        std::vector<double> newJA = kc->solveIK(activeKinematicChain, PoseBasePtr(new Pose(activeKinematicChain->getTCP()->getGlobalPose())), VirtualRobot::IKSolver::CartesianSelection::All, 50);
        if (newJA.size() != 0)
        {
            activeKinematicChain->setJointValues(std::vector<float>(newJA.begin(), newJA.end()));
        }
    }

    void SettingController::retranslateGui()
    {
        throw ("not yet implemented");
    }

    void SettingController::enableSelectRobotButton(bool enable)
    {
        guiSettingTab->getSettingTab()->robotSelectionButton->setEnabled(enable);
    }

    void SettingController::environmentChanged(EnvironmentPtr environment)
    {
        guiSettingTab->getSettingTab()->tcpComboBox->clear();
        guiSettingTab->getSettingTab()->collisionModelComboBox->clear();
        guiSettingTab->getSettingTab()->collisionModelList->clear();
        this->environment = environment;
        // Get robot and initialize child widgets
        VirtualRobot::RobotPtr robot = environment->getRobot();
        initTCPComboBox(robot);
        initCMComboBox(robot);
        initCMList(robot);
    }


    /************************************************************************************/
    /* Private functions                                                                */
    /************************************************************************************/
    void SettingController::enableWidgets(bool enable)
    {
        this->guiSettingTab->getSettingTab()->collisionModelComboBox->setEnabled(enable);
        this->guiSettingTab->getSettingTab()->collisionModelList->setEnabled(enable);
        //Man kann auch importieren ohne einen TCP ausgewählt zu haben.
        //this->guiSettingTab->getSettingTab()->importButton->setEnabled(enable);
    }

    void SettingController::initTCPComboBox(VirtualRobot::RobotPtr robot)
    {
        QComboBox* tcpComboBox = this->guiSettingTab->getSettingTab()->tcpComboBox;

        // Set strong focus and add wheel event filter
        tcpComboBox->setFocusPolicy(Qt::StrongFocus);
        tcpComboBox->installEventFilter(new WheelEventFilter(this));

        if (robot)
        {
            tcpComboBox->clear();

            // Combo box item at first index is no rns
            tcpComboBox->insertItem(0, QString::fromStdString("- select -"));

            auto robotNodeSets = robot->getRobotNodeSets();

            // Add combo box item for each robot node set
            for (VirtualRobot::RobotNodeSetPtr s : robotNodeSets)
            {
                if ((s->isKinematicChain()) && (s->getTCP() != NULL))
                {
                    ARMARX_DEBUG << "Add item " << s->getName() << " to tcp combo box";
                    tcpComboBox->addItem(QString::fromStdString(s->getName()));
                }
            }
            tcpComboBox->setEnabled(true);
            tcpComboBox->setCurrentIndex(0);
        }
        else
        {
            // Error message? Or just label in red etc?
            tcpComboBox->setEnabled(false);
        }
    }

    void SettingController::initCMComboBox(VirtualRobot::RobotPtr robot)
    {
        QComboBox* cmComboBox = this->guiSettingTab->getSettingTab()->
                                collisionModelComboBox;

        // Set strong focus and add wheel event filter
        cmComboBox->setFocusPolicy(Qt::StrongFocus);
        cmComboBox->installEventFilter(new WheelEventFilter(this));

        if (robot)
        {
            cmComboBox->clear();

            // Combo box item at first index is no collision model
            cmComboBox->insertItem(0, QString::fromStdString("- select -"));

            auto robotNodeSets = robot->getRobotNodeSets();

            // Add combo box item for each robot node set (collision model)
            for (VirtualRobot::RobotNodeSetPtr s : robotNodeSets)
            {
                if (s->isKinematicChain())
                {
                    ARMARX_DEBUG << "Add item " << s->getName() << " to collision model combo box";
                    cmComboBox->addItem(QString::fromStdString(s->getName()));
                }
            }
            cmComboBox->setCurrentIndex(0);
        }
        else
        {
            // Error message? Or just label in red etc?
            cmComboBox->setEnabled(false);
        }
    }

    void SettingController::initCMList(VirtualRobot::RobotPtr robot)
    {
        QListWidget* cmList = this->guiSettingTab->getSettingTab()->collisionModelList;
        if (robot)
        {
            cmList->clear();

            auto robotNodeSets = robot->getRobotNodeSets();

            for (VirtualRobot::RobotNodeSetPtr s : robotNodeSets)
            {
                if (s->isKinematicChain())
                {
                    QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(s->getName()));
                    item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
                    item->setCheckState(Qt::Unchecked);
                    ARMARX_DEBUG << "Add item " << s->getName() << " to collision model list";
                    cmList->addItem(item);
                }
            }
        }
        else
        {
            // Error message? Or just label in red etc?
            cmList->setEnabled(false);
        }
    }

}
