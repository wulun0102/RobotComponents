/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::View::SettingTab
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef SETTINGTAB_H
#define SETTINGTAB_H

#include <RobotComponents/gui-plugins/RobotTrajectoryDesignerGuiPlugin/View/ui_SettingTab.h>
#include <QWidget>
#include <QListWidget>
#include <QComboBox>
#include <QPushButton>
#include <memory>

namespace Ui
{
    class SettingTab;
}

class SettingTab : public QWidget
{
    Q_OBJECT

public:
    explicit SettingTab(QWidget* parent = 0);
    ~SettingTab();

    Ui::SettingTab* getSettingTab();
    void setSettingTab(Ui::SettingTab* settingTab);

private:
    Ui::SettingTab* settingTab;
};

using SettingTabPtr = std::shared_ptr<SettingTab>;

#endif // SETTINGTAB_H
