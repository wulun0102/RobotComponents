/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::View::WaypointTab
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef WAYPOINTTAB_H
#define WAYPOINTTAB_H

#include <RobotComponents/gui-plugins/RobotTrajectoryDesignerGuiPlugin/ui_WaypointTab.h>
#include <QWidget>
#include <QListWidget>
#include <memory>

namespace Ui
{
    class WaypointTab;
}

class WaypointTab : public QWidget
{
    Q_OBJECT

public:
    explicit WaypointTab(QWidget* parent = 0);
    ~WaypointTab();

    Ui::WaypointTab* getWaypointTab();
    void setWaypointTab(Ui::WaypointTab* WaypointTab);

private:
    Ui::WaypointTab* waypointTab;
};

using WaypointTabPtr = std::shared_ptr<WaypointTab>;

#endif // WAYPOINTTAB_H
