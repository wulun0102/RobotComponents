/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::View::ToolBar
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef ToolBar_H
#define ToolBar_H

#include <RobotComponents/gui-plugins/RobotTrajectoryDesignerGuiPlugin/View/ui_ToolBar.h>
#include <QWidget>
#include <memory>

namespace Ui
{
    class ToolBar;
}

class ToolBar : public QWidget
{
    Q_OBJECT

public:
    explicit ToolBar(QWidget* parent = 0);
    ~ToolBar();

    Ui::ToolBar* getToolBar();
    void setToolBar(Ui::ToolBar* toolBar);


private:
    Ui::ToolBar* toolBar;
};

using ToolBarPtr = std::shared_ptr<ToolBar>;

#endif // ToolBar_H
