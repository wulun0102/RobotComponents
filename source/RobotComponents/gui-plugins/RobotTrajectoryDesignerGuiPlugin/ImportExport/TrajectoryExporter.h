/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::ImportExport
 * @author     Liran Dattner
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef TRAJECTORYEXPORTER_H
#define TRAJECTORYEXPORTER_H
#include <Ice/Ice.h>
#include "DesignerExporter.h"
#include "../Model/DesignerTrajectory.h"
namespace armarx
{
    class TrajectoryExporter;
    /**
     * @class TrajectoryExporter
     * @brief Exports instances of the DesignerTrajectory class to files as serialized instance of the Trajectory class.
     *        UserWaypoint information is not exported.
     */
    class TrajectoryExporter// : public DesignerExporter
    {
    public:
        /**
         * @brief Exports a trajectory to the target file as a serialized instance of the Trajectory class.
         * @param trajectory Trajectory to export.
         * @param fps fsequency of the measurements in measurements per second.
         * @param c The Ice::Current instance
         */
        void exportTrajectory(std::vector<DesignerTrajectoryPtr> trajectories, const std::string file);
    };

    using TrajectoryExporterPtr = std::shared_ptr<TrajectoryExporter>;
}
#endif // TRAJECTORYEXPORTER_H

