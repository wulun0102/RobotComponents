/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::ImportExport
 * @author     Liran Dattner
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef MMMIMPORTER_H
#define MMMIMPORTER_H

#include "../Model/DesignerTrajectory.h"
#include "../Environment.h"

namespace armarx
{
    class MMMImporter;
    /**
     * @class MMMImporter
     * @brief Imports instances of the DesignerTrajectory class from a file in MMM format.
     */
    class MMMImporter
    {
    private:
        EnvironmentPtr environment;

    public:
        MMMImporter(EnvironmentPtr environment);
        /**
         * @brief Import a trajectory from the target file in MMM format.
         */
        std::vector<DesignerTrajectoryPtr> importTrajectory(std::string file);
    };

    using MMMImporterPtr = std::shared_ptr<MMMImporter>;
}
#endif // MMMIMPORTER_H
