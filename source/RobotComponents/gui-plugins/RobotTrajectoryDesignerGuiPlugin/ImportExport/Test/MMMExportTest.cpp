/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::ImportExport
 * @author     Liran Dattner
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::MMMImporter
#define ARMARX_BOOST_TEST

#include <boost/test/unit_test.hpp>

#include "../MMMExporter.h"

using namespace armarx;

//Check if MMMExporter exports the user points correctly
BOOST_AUTO_TEST_CASE(MMMExportUserPointsTest)
{
    /*
    std::vector<std::vector<double>> data1 =
    {
        {1, 2, 3, 4, 5},
        {1, 2, 3, 4, 5},
        {1, 2, 3, 4, 5},
        {1, 2, 3, 4, 5},
        {1, 2, 3, 4, 5},
        {1, 2, 3, 4, 5},
        {1, 2, 3, 4, 5}
    };
    std::vector<std::vector<double>> data2 =
    {
        {5, 6, 7, 8, 9},
        {5, 6, 7, 8, 9},
        {5, 6, 7, 8, 9},
        {5, 6, 7, 8, 9},
        {5, 6, 7, 8, 9},
        {5, 6, 7, 8, 9},
        {5, 6, 7, 8, 9}
    };
    std::vector<std::vector<double>> data3 =
    {
        {9, 10, 11, 12, 13},
        {9, 10, 11, 12, 13},
        {9, 10, 11, 12, 13},
        {9, 10, 11, 12, 13},
        {9, 10, 11, 12, 13},
        {9, 10, 11, 12, 13},
        {9, 10, 11, 12, 13}
    };

    Ice::DoubleSeq timestamps1 = {0, 1, 2, 3, 4};
    Ice::DoubleSeq timestamps2 = {0, 1, 2, 3, 4};
    Ice::DoubleSeq timestamps3 = {0, 1, 2, 3, 4};
    Ice::StringSeq dimensionNames = {"a", "b", "c", "d", "e", "f", "g"};

    TrajectoryPtr traj1(new Trajectory(data1, timestamps1, dimensionNames));
    TrajectoryPtr traj2(new Trajectory(data2, timestamps2, dimensionNames));
    TrajectoryPtr traj3(new Trajectory(data3, timestamps3, dimensionNames));
    */
    //Cant test without being able to set DesignerTrajectories
    /*
    DesignerTrajectoryPtr d1 = new DesignerTrajectory();
    DesignerTrajectoryPtr d2 = new DesignerTrajectory();
    DesignerTrajectoryPtr d3 = new DesignerTrajectory();
    */

    BOOST_CHECK_EQUAL(1, 1);
}

//Check if MMMExporter exports the input Trajectory correctly
BOOST_AUTO_TEST_CASE(MMMExportTrajectoryTest)
{
    BOOST_CHECK_EQUAL(1, 1);
}

//Check if MMMExporter exports complete Trajectories correctly.
BOOST_AUTO_TEST_CASE(MMMExportCompleteTest)
{
    BOOST_CHECK_EQUAL(1, 1);
}
