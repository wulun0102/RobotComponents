/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::ImportExport
 * @author     Liran Dattner
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::TrajectoryExporter
#define ARMARX_BOOST_TEST

#include <boost/test/unit_test.hpp>

//Check if TrajectoryExporter exports Userpoints correctly
BOOST_AUTO_TEST_CASE(TrajectoryExportUserPointsTest)
{
    BOOST_CHECK_EQUAL(1, 1);
}

//Check if TrajectoryExporter exports the contained final trajectory correctly
BOOST_AUTO_TEST_CASE(TrajectoryExportTrajectoryTest)
{
    BOOST_CHECK_EQUAL(1, 1);
}

//Check if TrajectoryExporter exports complete DesignerTrajectories correctly
BOOST_AUTO_TEST_CASE(TrajectoryExportCompleteTest)
{
    BOOST_CHECK_EQUAL(1, 1);
}
