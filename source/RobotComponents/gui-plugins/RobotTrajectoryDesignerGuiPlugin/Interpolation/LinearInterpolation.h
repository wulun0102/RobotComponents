/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Interpolation
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef LINEARINTERPOLATION_H
#define LINEARINTERPOLATION_H

#include "AbstractInterpolation.h"


#include <Eigen/Eigen>

namespace armarx
{
    /**
     * @brief The LinearInterpolation class represents a linear Interpolation between a series of control points
     * Linear means that the position is calcualed by a function with the Form a + time * b (with a and b as three-dimensional Vectors)
     */
    class LinearInterpolation : public AbstractInterpolation
    {

    public:
        /**
         * @brief LinearInterpolation creates a new LinearInterPolation defined by controlPoints
         * @param controlPoints a vector of Poses that define the Interpolation-Function
         */
        LinearInterpolation(std::vector<PoseBasePtr> controlPoints);

        /**
         * @brief getPoseAt returns the Pose defined by f(time)
         * @param time a time between 0 and 1 with getPoseAt(0) being the startingPose and getPoseAt(1) being the ending Pose
         * @return the pose of the interpolation-function at time
         */
        PoseBasePtr getPoseAt(double time) override;

    private:
        std::vector<Eigen::Vector3f> connectingVector;
    };

    using LinearInterpolationPtr = std::shared_ptr <LinearInterpolation>;


}

#endif
