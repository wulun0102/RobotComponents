/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Interpolation
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "SplineInterpolation.h"
#include "SplineInterpolationSegment.h"

///ARMARX-INCLUDES
#include <RobotAPI/libraries/core/Pose.h>
///EXCEPTIONS
#include "../exceptions/InterpolationNotDefinedException.h"
#include "../exceptions/NoInterpolationPossibleException.h"

using namespace armarx;
using namespace alglib;

SplineInterpolation::SplineInterpolation(std::vector<PoseBasePtr> controlPoints)
{
    if (controlPoints.size() < static_cast<unsigned>(3))
    {
        throw exceptions::local::NoInterpolationPossibleException(2, controlPoints.size());
    }
    init(controlPoints);

    //Creating the Interpolation Functions for all parametric Splines (x,y,z -Coordinate)
    alglib::pspline2build(getCoordinateArray(0), controlPoints.size(), 2, 0, xInterpolation);
    alglib::pspline2build(getCoordinateArray(1), controlPoints.size(), 2, 0, yInterpolation);
    alglib::pspline2build(getCoordinateArray(2), controlPoints.size(), 2, 0, zInterpolation);
}
PoseBasePtr SplineInterpolation::getPoseAt(double time)
{
    if (time < 0 || time > 1)
    {
        throw new exceptions::local::InterpolationNotDefinedException(time);
    }
    if (time == 1)
    {
        return SplineInterpolation::deepCopy(controlPoints[controlPoints.size() - 1]);
    }
    double x = 0;
    double y = 0;
    double z = 0;
    double temp = 0;
    alglib::pspline2calc(xInterpolation, time, x, temp);
    alglib::pspline2calc(yInterpolation, time, y, temp);
    alglib::pspline2calc(zInterpolation, time, z, temp);

    Vector3BasePtr position = new Vector3(x, y, z);
    QuaternionBasePtr base = this->calculateOrientationAt(time);
    return *new PoseBasePtr(new Pose(position, base));

}

AbstractInterpolationPtr SplineInterpolation::getInterPolationSegment(PoseBasePtr start)
{
    int number = 0;
    for (unsigned int i = 0; i < controlPoints.size(); i++)
    {
        if (controlPoints.at(i) == start)
        {
            number = i;
            break;
        }
    }
    return  *new AbstractInterpolationPtr(new SplineInterpolationSegment(number, AbstractInterpolationPtr(this)));
}

AbstractInterpolationPtr SplineInterpolation::getInterPolationSegment(int segmentNumber)
{
    return  *new AbstractInterpolationPtr(new SplineInterpolationSegment(segmentNumber, AbstractInterpolationPtr(this)));
}



alglib::real_2d_array SplineInterpolation::getCoordinateArray(int coordinate)
{
    std::string transformation = "[";
    unsigned int i = 0;
    for (PoseBasePtr current : controlPoints)
    {
        int temp;
        switch (coordinate)
        {
            case (0):
                temp = current->position->x;
                break;
            case (1):
                temp = current->position->y;
                break;
            case (2):
                temp = current->position->z;
                break;
        }
        transformation = transformation + "[" + std::to_string(temp) + "," + std::to_string(i / this->controlPoints.size()) + "]";
        if (i != controlPoints.size() - 1)
        {
            transformation += ",";
        }
        i++;
    }
    transformation += "]";
    const char* c = transformation.c_str();
    real_2d_array input = *new real_2d_array(c);//TODO implement
    return input;
}

