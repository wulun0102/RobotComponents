#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::SplineInterpolation
#define ARMARX_BOOST_TEST


#include <RobotComponents/Test.h>


#include "../exceptions/NoInterpolationPossibleException.h"

#include "../../Interpolation/SplineInterpolation.h"
#include "../../Util/OrientationConversion.h"
#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;

BOOST_AUTO_TEST_CASE(basicTest)
{
    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    Vector3BasePtr pos2 = new Vector3(0, 1, 2);
    QuaternionBasePtr ori2 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose2 = new Pose(pos2, ori2);


    Vector3BasePtr pos3 = new Vector3(0, -5, -2);
    QuaternionBasePtr ori3 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose3 = new Pose(pos3, ori3);

    std::vector<PoseBasePtr> cp = {pose1, pose2, pose3};
    SplineInterpolationPtr ip = *new SplineInterpolationPtr(new SplineInterpolation(cp));

    BOOST_CHECK_EQUAL(ip->getNumberOfControlPoints(), 3);

    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->x, 2);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->y, 4);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->z, 6);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->x, 0);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->y, 1);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->z, 2);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->x, 0);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->y, -5);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->z, -2);
}

bool is_critical(exceptions::local::NoInterpolationPossibleException const& ex)
{
    return true;
}


BOOST_AUTO_TEST_CASE(rotateAtPlaceTest)
{
    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    Vector3BasePtr pos2 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori2 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose2 = new Pose(pos2, ori2);

    Vector3BasePtr pos3 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori3 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose3 = new Pose(pos3, ori3);

    Vector3BasePtr pos4 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori4 = OrientationConversion::toQuaternion(0, 1, 2);
    PoseBasePtr pose4 = new Pose(pos4, ori4);

    std::vector<PoseBasePtr> cp = {pose1, pose2, pose3, pose4};
    SplineInterpolationPtr ip = *new SplineInterpolationPtr(new SplineInterpolation(cp));

    QuaternionBasePtr q1 = OrientationConversion::toQuaternion(0, 0, 0);
    QuaternionBasePtr q2 = OrientationConversion::toQuaternion(1, 1, 1);
    QuaternionBasePtr q3 = OrientationConversion::toQuaternion(2, 0, -1);
    QuaternionBasePtr q4 = OrientationConversion::toQuaternion(0, 1, 2);

    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->orientation->qw, q1->qw);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->orientation->qx, q1->qx);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->orientation->qy, q1->qy);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->orientation->qz, q1->qz);

    BOOST_CHECK_CLOSE(ip->getPoseAt(0.333)->orientation->qw, q2->qw, 1);
    BOOST_CHECK_CLOSE(ip->getPoseAt(0.333)->orientation->qx, q2->qx, 1);
    BOOST_CHECK_CLOSE(ip->getPoseAt(0.333)->orientation->qy, q2->qy, 1);
    BOOST_CHECK_CLOSE(ip->getPoseAt(0.333)->orientation->qz, q2->qz, 1);

    BOOST_CHECK_CLOSE(ip->getPoseAt(0.666)->orientation->qw, q3->qw, 1);
    BOOST_CHECK_CLOSE(ip->getPoseAt(0.666)->orientation->qx, q3->qx, 1);
    BOOST_CHECK_CLOSE(ip->getPoseAt(0.666)->orientation->qy, q3->qy, 1);
    BOOST_CHECK_CLOSE(ip->getPoseAt(0.666)->orientation->qz, q3->qz, 1);

    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->orientation->qw, q4->qw);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->orientation->qx, q4->qx);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->orientation->qy, q4->qy);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->orientation->qz, q4->qz);


}


BOOST_AUTO_TEST_CASE(startIsEndTest)
{
    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    Vector3BasePtr pos2 = new Vector3(0, -2, -8);
    QuaternionBasePtr ori2 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose2 = new Pose(pos2, ori2);

    Vector3BasePtr pos3 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori3 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose3 = new Pose(pos3, ori3);

    std::vector<PoseBasePtr> cp = {pose1, pose2, pose3};
    SplineInterpolationPtr ip = *new SplineInterpolationPtr(new SplineInterpolation(cp));

    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->x, 2);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->y, 4);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->z, 6);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->x, 2);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->y, 4);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->z, 6);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->x, 0);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->y, -2);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->z, -8);
}

BOOST_AUTO_TEST_CASE(tenPointPositionTest)
{
    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    Vector3BasePtr pos2 = new Vector3(0, -2, -8);
    QuaternionBasePtr ori2 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose2 = new Pose(pos2, ori2);

    Vector3BasePtr pos3 = new Vector3(5, 8, 7);
    QuaternionBasePtr ori3 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose3 = new Pose(pos3, ori3);

    Vector3BasePtr pos4 = new Vector3(4, 23, 1);
    QuaternionBasePtr ori4 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose4 = new Pose(pos4, ori4);

    Vector3BasePtr pos5 = new Vector3(7, 12, 4);
    QuaternionBasePtr ori5 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose5 = new Pose(pos5, ori5);

    Vector3BasePtr pos6 = new Vector3(7, 44, -3);
    QuaternionBasePtr ori6 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose6 = new Pose(pos6, ori6);

    Vector3BasePtr pos7 = new Vector3(5, 4, -8);
    QuaternionBasePtr ori7 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose7 = new Pose(pos7, ori7);

    Vector3BasePtr pos8 = new Vector3(5, -4, -8);
    QuaternionBasePtr ori8 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose8 = new Pose(pos8, ori8);

    Vector3BasePtr pos9 = new Vector3(0, 0, 4);
    QuaternionBasePtr ori9 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose9 = new Pose(pos9, ori9);

    Vector3BasePtr pos10 = new Vector3(0, 1, 2);
    QuaternionBasePtr ori10 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose10 = new Pose(pos10, ori10);

    Vector3BasePtr pos11 = new Vector3(0, 0, 0);
    QuaternionBasePtr ori11 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose11 = new Pose(pos11, ori11);

    std::vector<PoseBasePtr> cp = {pose1, pose2, pose3, pose4, pose5, pose6, pose7, pose8, pose9, pose10, pose11};
    SplineInterpolationPtr ip = *new SplineInterpolationPtr(new SplineInterpolation(cp));

    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->x, 2);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->y, 4);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0)->position->z, 6);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.1)->position->x, 0);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.1)->position->y, -2);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.1)->position->z, -8);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.2)->position->x, 5);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.2)->position->y, 8);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.2)->position->z, 7);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.3)->position->x, 4);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.3)->position->y, 23);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.3)->position->z, 1);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.4)->position->x, 7);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.4)->position->y, 12);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.4)->position->z, 4);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->x, 7);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->y, 44);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.5)->position->z, -3);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.6)->position->x, 5);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.6)->position->y, 4);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.6)->position->z, -8);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.7)->position->x, 5);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.7)->position->y, -4);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.7)->position->z, -8);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.8)->position->x, 0);
    //BOOST_REQUIRE_CLOSE(ip->getPoseAt(0.8)->position->y, 0, 1);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.8)->position->z, 4);
    //BOOST_REQUIRE_CLOSE(ip->getPoseAt(0.9)->position->x, 0, 1);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.9)->position->y, 1);
    BOOST_CHECK_EQUAL(ip->getPoseAt(0.9)->position->z, 2);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->x, 0);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->y, 0);
    BOOST_CHECK_EQUAL(ip->getPoseAt(1)->position->z, 0);
}

