#ifndef TIMEDTRAJECTORY_H
#define TIMEDTRAJECTORY_H

#include <RobotAPI/libraries/core/Trajectory.h>
#include <ArmarXCore/core/exceptions/user/NotImplementedYetException.h>

namespace armarx
{
    /**
     * @class TimedTrajectory
     * @brief A container for a Trajectory and a set of timestamps, representing the arrival of the Trajectory at userPoints.
    */
    class TimedTrajectory
    {
    private:
        //The Trajectory.
        TrajectoryPtr trajectory;
        //Mapped times of the Trajectory reaching userpoints.
        std::vector<double> userPoints;
    public:
        /**
         * @brief Creates a TimedTrajectory out of the supplied Trajectory and userpoints.
         * @param trajectory The Trajectory to point to.
         * @param userPoints The UserPoints.
         * @return The set of userPoints.
         */
        TimedTrajectory(armarx::TrajectoryPtr trajectory, std::vector<double> userPoints);
        /**
         * @brief Returns the contained Trajectory.
         * @return The contained Trajectory.
         */
        const armarx::TrajectoryPtr getTrajectory() const;
        /**
         * @brief Returns the userPoints.
         * @return The set of userPoints.
         */
        std::vector<double> getUserPoints() const;
    };
}
#endif // TIMEDTRAJECTORY_H
