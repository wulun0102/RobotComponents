/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::RobotTrajectoryDesignerGuiPluginWidgetController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RobotTrajectoryDesignerGuiPluginWidgetController.h"

#include <string>
#include <QCoreApplication>
#include <VirtualRobot/XML/RobotIO.h>
#include <QShortcut>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <RobotAPI/interface/core/RobotState.h>
using namespace armarx;

RobotTrajectoryDesignerGuiPluginWidgetController::RobotTrajectoryDesignerGuiPluginWidgetController()
{
}


RobotTrajectoryDesignerGuiPluginWidgetController::~RobotTrajectoryDesignerGuiPluginWidgetController()
{

}


void RobotTrajectoryDesignerGuiPluginWidgetController::loadSettings(QSettings* settings)
{

}

void RobotTrajectoryDesignerGuiPluginWidgetController::saveSettings(QSettings* settings)
{

}


void RobotTrajectoryDesignerGuiPluginWidgetController::onInitComponent()
{
    QMetaObject::invokeMethod(this, "initWidget");
}


void RobotTrajectoryDesignerGuiPluginWidgetController::onConnectComponent()
{
    QMetaObject::invokeMethod(this, "connectWidget");
}

void RobotTrajectoryDesignerGuiPluginWidgetController::onDisconnectComponent()
{
    viewControllerPtr.reset();
    toolBarControllerPtr.reset();
    widget.robotVisualization->~QWidget();
    this->robotVisualizationControllerPtr.reset();
    mementoControllerPtr.reset();
    settingControllerPtr.reset();
    shortcutControllerPtr.reset();
    tcpInformationControllerPtr.reset();
    tcpSelectionControllerPtr.reset();
    trajectoryControllerPtr.reset();
    transitionControllerPtr.reset();
    waypointControllerPtr.reset();
    exportDialogControllerPtr.reset();
    importDialogControllerPtr.reset();
    robotVisualizationControllerPtr.reset();
    environmentControllerPtr.reset();
    KinematicSolver::reset();
}

void RobotTrajectoryDesignerGuiPluginWidgetController::initWidget()
{
    widget.setupUi(getWidget());
    // Create shared pointers for each member, initialize each controller
    this->exportDialogControllerPtr = std::make_shared<ExportDialogController>();
    this->importDialogControllerPtr = std::make_shared<ImportDialogController>();
    this->mementoControllerPtr = std::make_shared<MementoController>(
                                     widget.undoButton, widget.redoButton);
    this->robotVisualizationControllerPtr = std::make_shared<RobotVisualizationController>(widget.robotVisualization);
    this->robotVisualizationControllerPtr->addConnection(this->robotVisualizationControllerPtr);

    SettingTabPtr settingTabPtr(widget.settingTab);
    this->settingControllerPtr = std::make_shared<SettingController>(settingTabPtr);
    this->shortcutControllerPtr = std::make_shared<ShortcutController>(getWidget().data());
    TCPInformationPtr tcpInformationPtr(widget.tcpInformationTab);
    this->tcpInformationControllerPtr = std::make_shared<TCPInformationController>(
                                            tcpInformationPtr);
    this->tcpSelectionControllerPtr = std::make_shared<TCPSelectionController>(
                                          widget.currentTPComboBox);

    ToolBarPtr toolBarPtr(widget.toolBar);
    this->toolBarControllerPtr = std::make_shared<ToolBarController>(toolBarPtr);
    widget.toolBar->raise();
    this->trajectoryControllerPtr = std::make_shared<TrajectoryController>();

    TransitionTabPtr transitionTabPtr(widget.transitionTab);
    this->transitionControllerPtr = std::make_shared<TransitionController>(
                                        transitionTabPtr);
    PerspectivesPtr perspectivesPtr(widget.perspectives);
    this->viewControllerPtr = std::make_shared<ViewController>(perspectivesPtr);
    widget.perspectives->raise();

    WaypointTabPtr waypointTabPtr(widget.waypointTab);
    this->waypointControllerPtr = std::make_shared<WaypointController>(waypointTabPtr);
    this->environmentControllerPtr = std::make_shared<EnvironmentController>();
}

void RobotTrajectoryDesignerGuiPluginWidgetController::connectWidget()
{
    //=============================================================================================
    //EnvironmentController Signals

    QObject::connect(environmentControllerPtr.get(), SIGNAL(environmentChanged(EnvironmentPtr)),
                     exportDialogControllerPtr.get(), SLOT(environmentChanged(EnvironmentPtr)));
    QObject::connect(environmentControllerPtr.get(), SIGNAL(environmentChanged(EnvironmentPtr)),
                     importDialogControllerPtr.get(), SLOT(environmentChanged(EnvironmentPtr)));
    QObject::connect(environmentControllerPtr.get(), SIGNAL(environmentChanged(EnvironmentPtr)),
                     robotVisualizationControllerPtr.get(), SLOT(environmentChanged(EnvironmentPtr)));
    QObject::connect(environmentControllerPtr.get(), SIGNAL(environmentChanged(EnvironmentPtr)),
                     trajectoryControllerPtr.get(), SLOT(environmentChanged(EnvironmentPtr)));
    QObject::connect(environmentControllerPtr.get(), SIGNAL(environmentChanged(EnvironmentPtr)),
                     settingControllerPtr.get(), SLOT(environmentChanged(EnvironmentPtr)));

    //=============================================================================================
    //ExportDialogController Signals

    QObject::connect(exportDialogControllerPtr.get(), SIGNAL(exportDesignerTrajectory(int)),
                     trajectoryControllerPtr.get(), SLOT(exportTrajectory(int)));
    QObject::connect(exportDialogControllerPtr.get(), SIGNAL(exportDesignerTrajectory()),
                     trajectoryControllerPtr.get(), SLOT(exportTrajectory()));

    //=============================================================================================
    //ImportDialogController Signals

    QObject::connect(importDialogControllerPtr.get(), SIGNAL(import(DesignerTrajectoryPtr)),
                     trajectoryControllerPtr.get(), SLOT(import(DesignerTrajectoryPtr)));

    //=============================================================================================
    //MementoController Signals

    QObject::connect(mementoControllerPtr.get(), SIGNAL(undo()), trajectoryControllerPtr.get(), SLOT(undo()));
    QObject::connect(mementoControllerPtr.get(), SIGNAL(redo()), trajectoryControllerPtr.get(), SLOT(redo()));

    //=============================================================================================
    //RobotVisualizationController Signals
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(manipulatorChanged(Eigen::Matrix4f)),
                     tcpInformationControllerPtr.get(), SLOT(setDesiredPose(Eigen::Matrix4f)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(tcpPoseChanged(Eigen::Matrix4f)),
                     tcpInformationControllerPtr.get(), SLOT(setCurrentPose(Eigen::Matrix4f)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(poseReachable(bool)),
                     tcpInformationControllerPtr.get(), SLOT(setReachable(bool)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(wayPointSelected(int)),
                     waypointControllerPtr.get(), SLOT(updateSelectedWaypoint(int)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     waypointControllerPtr.get(), SLOT(enableDeleteButton(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     waypointControllerPtr.get(), SLOT(enableAddButton(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     waypointControllerPtr.get(), SLOT(enableWaypointListLineEdit(bool)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     waypointControllerPtr.get(), SLOT(enableAddButton(bool)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     toolBarControllerPtr.get(), SLOT(enableAddButton(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     toolBarControllerPtr.get(), SLOT(enableDeleteChangeButton(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     toolBarControllerPtr.get(), SLOT(enablePreviewAllButton(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     toolBarControllerPtr.get(), SLOT(enablePreviewButton(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerPlaying(bool)),
                     toolBarControllerPtr.get(), SLOT(enableStopButton(bool)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     settingControllerPtr.get(), SLOT(enableSelectRobotButton(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     settingControllerPtr.get(), SLOT(enableExportButtons(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     settingControllerPtr.get(), SLOT(enableImportTCPCollision(bool)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     mementoControllerPtr.get(), SLOT(enableRedoButtonVisualization(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     mementoControllerPtr.get(), SLOT(enableUndoButton(bool)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     shortcutControllerPtr.get(), SLOT(enableAddShortcut(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     shortcutControllerPtr.get(), SLOT(enableDeleteChangeShortcut(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     shortcutControllerPtr.get(), SLOT(enablePreviewAllShortcut(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     shortcutControllerPtr.get(), SLOT(enablePreviewShortcut(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     shortcutControllerPtr.get(), SLOT(enableRedoShortcut(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     shortcutControllerPtr.get(), SLOT(enableUndoShortcut(bool)));
    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerPlaying(bool)),
                     shortcutControllerPtr.get(), SLOT(enableStopPreviewShortcut(bool)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     tcpSelectionControllerPtr.get(), SLOT(enableSelectedTCP(bool)));

    QObject::connect(robotVisualizationControllerPtr.get(), SIGNAL(trajectoryPlayerNotPlaying(bool)),
                     transitionControllerPtr.get(), SLOT(enableAll(bool)));

    //=============================================================================================
    //SettingController Signals

    QObject::connect(settingControllerPtr.get(), SIGNAL(changedTCP(QString)),
                     trajectoryControllerPtr.get(), SLOT(updateTCP(QString)));
    QObject::connect(settingControllerPtr.get(), SIGNAL(setActiveColModelName(QString)),
                     trajectoryControllerPtr.get(), SLOT(setActiveColModelName(QString)));
    QObject::connect(settingControllerPtr.get(), SIGNAL(setBodyColModelsNames(QStringList)),
                     trajectoryControllerPtr.get(), SLOT(setBodyColModelsNames(QStringList)));

    QObject::connect(settingControllerPtr.get(), SIGNAL(openRobotSelection()),
                     environmentControllerPtr.get(), SLOT(openRobotFileDialog()));

    QObject::connect(settingControllerPtr.get(), SIGNAL(openShortcut()),
                     shortcutControllerPtr.get(), SLOT(open()));

    QObject::connect(settingControllerPtr.get(), SIGNAL(convertToMMM()),
                     exportDialogControllerPtr.get(), SLOT(exportMMM()));

    QObject::connect(settingControllerPtr.get(), SIGNAL(openImport()),
                     importDialogControllerPtr.get(), SLOT(open()));

    //=============================================================================================
    //ShortcutController Signals

    QObject::connect(shortcutControllerPtr.get(), SIGNAL(addedWaypoint(int, bool)),
                     trajectoryControllerPtr.get(), SLOT(addWaypoint(int, bool)));
    QObject::connect(shortcutControllerPtr.get(), SIGNAL(deletedWaypoint(int)),
                     trajectoryControllerPtr.get(), SLOT(deleteWaypoint(int)));
    QObject::connect(shortcutControllerPtr.get(), SIGNAL(changeWaypoint(int)),
                     trajectoryControllerPtr.get(), SLOT(updateWaypoint(int)));
    QObject::connect(shortcutControllerPtr.get(), SIGNAL(undo()),
                     trajectoryControllerPtr.get(), SLOT(undo()));
    QObject::connect(shortcutControllerPtr.get(), SIGNAL(redo()),
                     trajectoryControllerPtr.get(), SLOT(redo()));
    QObject::connect(shortcutControllerPtr.get(), SIGNAL(playPreviewAllSignal()),
                     trajectoryControllerPtr.get(), SLOT(playTrajectories()));

    QObject::connect(shortcutControllerPtr.get(), SIGNAL(playPreviewSignal()),
                     robotVisualizationControllerPtr.get(), SLOT(playTrajectory()));
    QObject::connect(shortcutControllerPtr.get(), SIGNAL(changedPerspective(int)),
                     robotVisualizationControllerPtr.get(), SLOT(setCamera(int)));

    QObject::connect(shortcutControllerPtr.get(), SIGNAL(addedWaypointPressed(bool)),
                     robotVisualizationControllerPtr.get(), SLOT(enableLeftMouse(bool)));
    QObject::connect(shortcutControllerPtr.get(), SIGNAL(stopPreviewSignal()),
                     robotVisualizationControllerPtr.get(), SLOT(trajectoryPlayerStopped()));

    //=============================================================================================
    //TCPSelectionController Signals

    QObject::connect(tcpSelectionControllerPtr.get(), SIGNAL(changedSelectedTCP(QString)),
                     trajectoryControllerPtr.get(), SLOT(updateTCP(QString)));
    QObject::connect(tcpSelectionControllerPtr.get(), SIGNAL(changedSelectedTCP(QString)),
                     settingControllerPtr.get(), SLOT(selectTCP(QString)));


    //=============================================================================================
    //ToolBarController Signals

    QObject::connect(toolBarControllerPtr.get(), SIGNAL(addedWaypoint(int, bool)),
                     trajectoryControllerPtr.get(), SLOT(addWaypoint(int, bool)));
    QObject::connect(toolBarControllerPtr.get(), SIGNAL(deletedWaypoint(int)),
                     trajectoryControllerPtr.get(), SLOT(deleteWaypoint(int)));
    QObject::connect(toolBarControllerPtr.get(), SIGNAL(changeWaypoint(int)),
                     trajectoryControllerPtr.get(), SLOT(updateWaypoint(int)));

    QObject::connect(toolBarControllerPtr.get(), SIGNAL(notifyAllPreview()),
                     trajectoryControllerPtr.get(), SLOT(playTrajectories()));

    QObject::connect(toolBarControllerPtr.get(), SIGNAL(notifyPreview()),
                     robotVisualizationControllerPtr.get(), SLOT(playTrajectory()));
    QObject::connect(toolBarControllerPtr.get(), SIGNAL(notifyStopPreview()),
                     robotVisualizationControllerPtr.get(), SLOT(trajectoryPlayerStopped()));

    //=============================================================================================
    //TrajectroyController Signals

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(addWaypointGui(int, std::vector<double>, int, bool)),
                     waypointControllerPtr.get(), SLOT(addWaypoint(int, std::vector<double>, int, bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(changeWaypointGui(int, std::vector<double>, int, bool)),
                     waypointControllerPtr.get(), SLOT(setWaypointData(int, std::vector<double>, int, bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(removeWaypointGui(int)),
                     waypointControllerPtr.get(), SLOT(removeWaypoint(int)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(removeTransitionWaypointGui()),
                     waypointControllerPtr.get(), SLOT(clearWaypointList()));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableDeleteChange(bool)),
                     waypointControllerPtr.get(), SLOT(enableDeleteButton(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableAdd(bool)),
                     waypointControllerPtr.get(), SLOT(enableAddButton(bool)));

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(addTransitionGui(int, double, double, int)),
                     transitionControllerPtr.get(), SLOT(addTransition(int, double, double, int)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(changeTransitionGui(int, double, double, int)),
                     transitionControllerPtr.get(), SLOT(setTransitionData(int, double, double, int)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(removeTransitionGui(int)),
                     transitionControllerPtr.get(), SLOT(removeTransition(int)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(removeTransitionWaypointGui()),
                     transitionControllerPtr.get(), SLOT(clearTransitionList()));

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(newTrajectory(QString)),
                     tcpSelectionControllerPtr.get(), SLOT(addTrajectory(QString)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(updateSelectedTCP(QString)),
                     tcpSelectionControllerPtr.get(), SLOT(updateSelectedTCP(QString)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(removeTrajectory(QString)),
                     tcpSelectionControllerPtr.get(), SLOT(removeTrajectory(QString)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(removeAllTrajectories()),
                     tcpSelectionControllerPtr.get(), SLOT(removeAllTrajectories()));

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(updateSelectedTCP(QString)),
                     settingControllerPtr.get(), SLOT(selectTCP(QString)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableIKSolutionButton(bool)),
                     settingControllerPtr.get(), SLOT(enableIKSolutionButton(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableExportButtons(bool)),
                     settingControllerPtr.get(), SLOT(enableExportButtons(bool)));

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableRedo(bool)),
                     mementoControllerPtr.get(), SLOT(enableRedoButton(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableUndo(bool)),
                     mementoControllerPtr.get(), SLOT(enableUndoButton(bool)));

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(exportTrajectory(std::vector<DesignerTrajectoryPtr>)),
                     exportDialogControllerPtr.get(), SLOT(exportTrajectory(std::vector<DesignerTrajectoryPtr>)));

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(showTrajectory(DesignerTrajectoryPtr)),
                     robotVisualizationControllerPtr.get(), SLOT(updateViews(DesignerTrajectoryPtr)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(removeTransitionWaypointGui()),
                     robotVisualizationControllerPtr.get(), SLOT(clearView()));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(cartesianSelectionChanged(VirtualRobot::IKSolver::CartesianSelection())),
                     robotVisualizationControllerPtr.get(), SLOT(cartesianSelectionChanged(VirtualRobot::IKSolver::CartesianSelection())));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(playTrajectories(std::vector<DesignerTrajectoryPtr>)),
                     robotVisualizationControllerPtr.get(), SLOT(playTrajectories(std::vector<DesignerTrajectoryPtr>)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(rnsChanged(VirtualRobot::RobotNodeSetPtr)),
                     robotVisualizationControllerPtr.get(), SLOT(kinematicChainChanged(VirtualRobot::RobotNodeSetPtr)));

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableDeleteChange(bool)),
                     toolBarControllerPtr.get(), SLOT(enableDeleteChangeButton(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableAdd(bool)),
                     toolBarControllerPtr.get(), SLOT(enableAddButton(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enablePreview(bool)),
                     toolBarControllerPtr.get(), SLOT(enablePreviewButton(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enablePreviewAll(bool)),
                     toolBarControllerPtr.get(), SLOT(enablePreviewAllButton(bool)));

    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableDeleteChange(bool)),
                     shortcutControllerPtr.get(), SLOT(enableDeleteChangeShortcut(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableAdd(bool)),
                     shortcutControllerPtr.get(), SLOT(enableAddShortcut(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enablePreview(bool)),
                     shortcutControllerPtr.get(), SLOT(enablePreviewShortcut(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enablePreviewAll(bool)),
                     shortcutControllerPtr.get(), SLOT(enablePreviewAllShortcut(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableRedo(bool)),
                     shortcutControllerPtr.get(), SLOT(enableRedoShortcut(bool)));
    QObject::connect(trajectoryControllerPtr.get(), SIGNAL(enableUndo(bool)),
                     shortcutControllerPtr.get(), SLOT(enableUnoShortcut(bool)));

    //=============================================================================================
    //TransitionController Signals

    QObject::connect(transitionControllerPtr.get(), SIGNAL(changedDuration(int, double)),
                     trajectoryControllerPtr.get(), SLOT(updateTransition(int, double)));
    QObject::connect(transitionControllerPtr.get(), SIGNAL(changedInterpolation(int, int)),
                     trajectoryControllerPtr.get(), SLOT(updateTransition(int, int)));

    QObject::connect(transitionControllerPtr.get(), SIGNAL(selectedTransitionChanged(int)),
                     robotVisualizationControllerPtr.get(), SLOT(selectedTransitionChanged(int)));

    //=============================================================================================
    //ViewController Signals

    QObject::connect(viewControllerPtr.get(), SIGNAL(changedPerspective(int)),
                     robotVisualizationControllerPtr.get(), SLOT(setCamera(int)));
    QObject::connect(viewControllerPtr.get(), SIGNAL(addView()),
                     robotVisualizationControllerPtr.get(), SLOT(addView()));
    QObject::connect(viewControllerPtr.get(), SIGNAL(removeView()),
                     robotVisualizationControllerPtr.get(), SLOT(removeView()));


    //=============================================================================================
    //WaypointController Signals

    QObject::connect(waypointControllerPtr.get(), SIGNAL(changedWaypoint(int, std::vector<double>)),
                     trajectoryControllerPtr.get(), SLOT(updateWaypoint(int, std::vector<double>)));
    QObject::connect(waypointControllerPtr.get(), SIGNAL(changedWaypoint(int, int)),
                     trajectoryControllerPtr.get(), SLOT(updateWaypoint(int, int)));
    QObject::connect(waypointControllerPtr.get(), SIGNAL(changedWaypoint(int, bool)),
                     trajectoryControllerPtr.get(), SLOT(updateWaypoint(int, bool)));
    QObject::connect(waypointControllerPtr.get(), SIGNAL(addedWaypoint(int, bool)),
                     trajectoryControllerPtr.get(), SLOT(addWaypoint(int, bool)));
    QObject::connect(waypointControllerPtr.get(), SIGNAL(deletedWaypoint(int)),
                     trajectoryControllerPtr.get(), SLOT(deleteWaypoint(int)));
    QObject::connect(waypointControllerPtr.get(), SIGNAL(enableIKSolutionButton(bool)),
                     settingControllerPtr.get(), SLOT(enableIKSolutionButton(bool)));
    QObject::connect(waypointControllerPtr.get(), SIGNAL(setCurrentIndex(int)),
                     toolBarControllerPtr.get(), SLOT(setCurrentWaypoint(int)));
    QObject::connect(waypointControllerPtr.get(), SIGNAL(setCurrentIndex(int)),
                     shortcutControllerPtr.get(), SLOT(setCurrentWaypoint(int)));

    QObject::connect(waypointControllerPtr.get(), SIGNAL(setCurrentIndexRobotVisualization(int)),
                     robotVisualizationControllerPtr.get(), SLOT(selectedWayPointChanged(int)));

    //=============================================================================================
    //Load Robot
    RobotStateComponentInterfacePrx robotStateComponent = getProxy<RobotStateComponentInterfacePrx>("RobotStateComponent", false, "", false);
    if (robotStateComponent)
    {
        environmentControllerPtr->loadRobotFromProxy(robotStateComponent->getRobotFilename());
    }
    else
    {
        //Laods a Robot in the application. Default Armar3.
        environmentControllerPtr->loadArmar3();
    }
}
