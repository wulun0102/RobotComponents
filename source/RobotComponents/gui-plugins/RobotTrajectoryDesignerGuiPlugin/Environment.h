#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Scene.h>


namespace armarx
{
    class Environment
    {
        VirtualRobot::RobotPtr robot;
        VirtualRobot::RobotPtr cdRobot; // copy of the actual robot for collision detection
        VirtualRobot::ScenePtr scene;

    public:
        Environment();

        VirtualRobot::RobotPtr getRobot();

        VirtualRobot::RobotPtr getCDRobot();

        void setRobot(const VirtualRobot::RobotPtr& value);

        VirtualRobot::ScenePtr getScene();

        void setScene(const VirtualRobot::ScenePtr& value);
    };
    using EnvironmentPtr = std::shared_ptr<Environment>;
}

#endif // ENVIRONMENT_H
