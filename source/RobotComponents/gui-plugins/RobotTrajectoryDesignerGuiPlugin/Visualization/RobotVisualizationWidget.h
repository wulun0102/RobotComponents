/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef RobotVisualizationWidget_h
#define RobotVisualizationWidget_h

/* Qt headers */
#include <QWidget>
#include <QSplitter>

/* boost headers */
#include <boost/smart_ptr/shared_ptr.hpp>

#include "RobotVisualization.h"

namespace armarx
{
    /**
     * @brief The RobotVisualizationWidget class
     * Holds the original viewer and reproduces it tosupport parallel views.
     * Automatically splits the viewers.
     */
    class RobotVisualizationWidget : public QSplitter
    {
        Q_OBJECT

    public:
        /**
        * Constructor.
        * Constructs a robot viewer widget.
        * Expects a controller::ControllerPtr.
        *
        * @param    control     shared pointer to controller::controller
        * @param    parent      parent widget
        *
        */
        explicit RobotVisualizationWidget(QWidget* parent = 0, QWidget* viewerWidget = 0, RobotVisualizationPtr viewer = 0);

        /**
        * Destructor.
        *
        */
        ~RobotVisualizationWidget();
        /**
         * @brief getRobotViewer the RobotVisualizationPtr that is held by this widget
         * @return he RobotVisualizationPtr that is held by this widget
         */
        RobotVisualizationPtr getRobotViewer();
        /**
         * @brief addWidget adds a new View by reproducing the original viewer
         */
        void addWidget();
        /**
         * @brief removeWidget removes a view from this Widget
         */
        void removeWidget();
        /**
         * @brief setCameraOfFocused sets the camera of the viewer that is currently focused (the viewer that was last clicked on or zoomed in)
         * @param position the position of the camera
         * @param pointAtA the lower point to point the camera at
         * @param pointAtB the upper point to point the camera at
         */
        void setCameraOfFocused(Eigen::Vector3f position, Eigen::Vector3f pointAtA, Eigen::Vector3f pointAtB);


    private slots:
        void activeWidgetChanged(QWidget* old, QWidget* now);

    private:
        RobotVisualizationPtr viewer;
        QWidget* viewerWidget;
        QSplitter* leftSplitter;
        QSplitter* rightSplitter;
        int childrenCounter;
        int selectedViewer;
        std::vector<RobotVisualizationPtr> clones;
        std::vector<QWidget*> cloneWidgets;
    };

    using RobotVisualizationWidgetPtr = std::shared_ptr<RobotVisualizationWidget>;
}

#endif
