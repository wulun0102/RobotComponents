/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef ManipulatorVisualization_H
#define ManipulatorVisualization_H

//Coin includes
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/manips/SoTransformerManip.h>
#include <Inventor/nodes/SoMaterial.h>

//VirtualRobot
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

//Boost includes
#include <boost/smart_ptr/intrusive_ptr.hpp>


//Let boost use referencing of Inventor to manage objects memory
//and not its own shared pointer referencing
using SoTransformerManipPtr = boost::intrusive_ptr<SoTransformerManip>;
inline void intrusive_ptr_add_ref(SoTransformerManip* obj)
{
    obj->ref();
}
inline void intrusive_ptr_release(SoTransformerManip* obj)
{
    obj->unref();
}

using CoinVisualizationPtr = boost::shared_ptr<VirtualRobot::CoinVisualization>;

class ManipulatorVisualization : SoSeparator
{
public:
    ManipulatorVisualization();
    ~ManipulatorVisualization();

    void setVisualization(VirtualRobot::RobotPtr robot, VirtualRobot::RobotNodeSetPtr nodeSet);
    void removeVisualization();

    void setColor(float r, float g, float b);

    void addManipFinishCallback(SoDraggerCB* func, void* data);
    void addManipMovedCallback(SoDraggerCB* func, void* data);

    Eigen::Matrix4f getUserDesiredPose();
    std::string getUserDesiredPoseString();

    bool getIsVisualizing()
    {
        return isVisualizing;
    }

private:
    SoTransformerManipPtr manip;
    SoMaterial* material;
    bool isVisualizing;
    bool hasEndEffectorVisualizer;
    Eigen::Matrix4f localTransformation;
};

#endif
