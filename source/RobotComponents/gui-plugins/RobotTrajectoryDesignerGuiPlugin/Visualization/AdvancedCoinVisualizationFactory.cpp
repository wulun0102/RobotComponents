#include "AdvancedCoinVisualizationFactory.h"

///ARMARX_INCLUDES
#include "ArmarXCore/core/logging/Logging.h"
#include "RobotAPI/libraries/core/Pose.h"

///VIRTUALROBOT_INCLUDES
#include "VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h"
#include "VirtualRobot/Visualization/CoinVisualization/CoinVisualizationNode.h"

///COIN3D INCLUDES
#include <Inventor/nodes/SoSeparator.h>

using namespace armarx;
using namespace VirtualRobot;


AdvancedCoinVisualizationFactory::AdvancedCoinVisualizationFactory()
{
    component = CoinVisualizationFactory();
}
VisualizationNodePtr AdvancedCoinVisualizationFactory::createCurve(const std::vector<PoseBasePtr> points, bool highlight)
{
    std::vector<VirtualRobot::VisualizationNodePtr> visualizations = std::vector<VirtualRobot::VisualizationNodePtr>();
    CoinVisualizationNodePtr parent = CoinVisualizationNodePtr(new CoinVisualizationNode(new SoSeparator()));
    for (unsigned int i = 0; i < points.size() - 1; i++)
    {
        PosePtr pose1 = PosePtr(new Pose(points[i]->position, points[i]->orientation));
        PosePtr pose2 = PosePtr(new Pose(points[i + 1]->position, points[i + 1]->orientation));
        VisualizationNodePtr node;
        if (!highlight)
        {
            node = CoinVisualizationFactory().createLine(pose1->toEigen(), pose2->toEigen(), 7.375, 254.0, 1, 1); //blue transition
        }
        else
        {
            node = CoinVisualizationFactory().createLine(pose1->toEigen(), pose2->toEigen(), 7.375, 0, 254.0, 0); //red transition
        }
        parent->attachVisualization("", node);
    }
    return parent;
}

/*CoinManipulatorVisualizationAdapterPtr AdvancedCoinVisualizationFactory::createManipulator()
{
    CoinManipulatorVisualizationAdapterPtr s;
    return *new CoinManipulatorVisualizationAdapterPtr(new CoinManipulatorVisualizationAdapter());
}*/
