/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef COINMANIPULATORVISUALIZATIONADAPTER_H
#define COINMANIPULATORVISUALIZATIONADAPTER_H

#include "AbstractManipulatorVisualization.h"

#include "ManipulatorVisualization.h"

#include <Eigen/Eigen>

#include <VirtualRobot/Robot.h>

#include <VirtualRobot/RobotNodeSet.h>

#include <Inventor/nodes/SoSeparator.h>

#include <memory>





namespace armarx
{
    /**
         * @brief The CoinManipulatorVisualizationAdapter class
         * Info: currently not in use as abstraction of manipulator doesn't make that much sense
         */
    class CoinManipulatorVisualizationAdapter : public AbstractManipulatorVisualization
    {

    public:
        CoinManipulatorVisualizationAdapter();
        ~CoinManipulatorVisualizationAdapter();
        Eigen::MatrixXf getUserDesiredPose();
        SoSeparator* getVisualization();
        void setVisualization(VirtualRobot::RobotPtr robot, VirtualRobot::RobotNodeSetPtr nodeSet);
        void attachCallback(SoDraggerCB* finish, SoDraggerCB* moved, void* data);

    private:
        ManipulatorVisualization* adaptedManipulation;
    };

    using CoinManipulatorVisualizationAdapterPtr = std::shared_ptr<CoinManipulatorVisualizationAdapter>;
}

#endif
