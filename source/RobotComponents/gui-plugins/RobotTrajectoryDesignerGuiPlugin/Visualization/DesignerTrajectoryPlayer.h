/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef DESIGNERTRAJECTORYPLAYER_H
#define DESIGNERTRAJECTORYPLAYER_H

//this include needs to be here so there is not problem with a Q_FOREACH macro
#include "../Controller/AbstractController.h"

#include <memory>

#include "RobotVisualization.h"
#include "../Model/DesignerTrajectory.h"



namespace armarx
{
    class DesignerTrajectoryPlayer : public QObject
    {
        Q_OBJECT

    public:
        /**
         * @brief DesignerTrajectoryPlayer construct a new DesignerTrajectoryPlayer that can be started to play a Trajectory
         * @param viewer the viewers to run the player on
         * @param robot the robot to execute the trajectory on
         */
        DesignerTrajectoryPlayer(RobotVisualizationPtr viewer, VirtualRobot::RobotPtr robot);
        /**
         * @brief addTrajectory inserts a trajetory to the DesignerTrajectory player
         * All added Trajectories will be played when calling the startPlayback method
         * @param trajectory the trajectory to add
         */
        void addTrajectory(DesignerTrajectoryPtr trajectory);
        /**
         * @brief startPlayback starts the actual visualization of the trajectory
         */
        void startPlayback();
        /**
         * @brief setFPS sets the refresh rate of the player (the amount of updates of the visualization) to fps
         * @param fps the amount of frames that the player shows per second (see Hz)
         */
        void setFPS(int fps);


    private slots:
        void updateLoop();

    signals:
        /**
         * @brief finishedPlayback tells all relevant controllers that the trajectory playback has stopped
         */
        void finishedPlayback();

    private:
        std::shared_ptr<QTimer> timer;
        VirtualRobot::RobotPtr robot;
        RobotVisualizationPtr viewer;
        std::vector<DesignerTrajectoryPtr> trajectories;
        std::vector<TrajectoryPtr> timeOptimalTrajectories;
        double currentTime;
        int fps;


    };
    using DesignerTrajectoryPlayerPtr = std::shared_ptr<DesignerTrajectoryPlayer>;
}




#endif
