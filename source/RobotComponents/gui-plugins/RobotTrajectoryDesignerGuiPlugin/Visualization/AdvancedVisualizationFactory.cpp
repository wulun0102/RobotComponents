#include "AdvancedVisualizationFactory.h"

#include "ArmarXCore/core/logging/Logging.h"

#include "RobotAPI/libraries/core/Pose.h"


using namespace armarx;
using namespace VirtualRobot;


VisualizationNodePtr AdvancedVisualizationFactory::createCurve(std::vector<PoseBasePtr> points, bool highlight)
{
    std::vector<VirtualRobot::VisualizationNodePtr> visualizations = std::vector<VirtualRobot::VisualizationNodePtr>();
    VisualizationNodePtr parent = VisualizationNodePtr(new VisualizationNode());
    for (unsigned int i = 0; i < points.size() - 1; i++)
    {
        PosePtr pose1 = PosePtr(new Pose(points[i]->position, points[i]->orientation));
        PosePtr pose2 = PosePtr(new Pose(points[i + 1]->position, points[i + 1]->orientation));
        VisualizationNodePtr node = this->createLine(pose1->toEigen(), pose2->toEigen());
        parent->attachVisualization("", node);
    }
    return parent;
}

