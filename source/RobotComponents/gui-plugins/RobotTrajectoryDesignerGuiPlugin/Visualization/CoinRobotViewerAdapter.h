/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef COINROBOTVIEWERADAPTER_H
#define COINROBOTVIEWERADAPTER_H

#include "RobotVisualization.h"
#include "AdvancedCoinVisualizationFactory.h"
#include "RobotViewer.h"
#include "ManipulatorVisualization.h"

///COIN3D-INCLUDES
#include "Inventor/draggers/SoDragger.h"
#include "Inventor/SoPath.h"
#include "Inventor/nodes/SoSelection.h"
#include "Inventor/SoPickedPoint.h"

namespace armarx
{
    /**
         * @brief The CoinRobotViewerAdapter class
         */
    class CoinRobotViewerAdapter : public RobotVisualization
    {
    public:

        /**
         * @brief CoinRobotViewerAdapter
         * @param widget the parent widget on the gui
         */
        CoinRobotViewerAdapter(QWidget* widget);
        ~CoinRobotViewerAdapter();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// METHODS FOR VISUALIZATION SETUP
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void addRobotVisualization(VirtualRobot::RobotPtr robot, QString selectedChain) override;
        void addSceneVisualization(VirtualRobot::ScenePtr scene) override;
        void setCamera(const Eigen::VectorXf position, const Eigen::VectorXf pointAtA, const Eigen::VectorXf pointAtB) override;
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// METHODS FOR TRANSITIONS
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void addTransitionVisualization(int index, std::vector<PoseBasePtr> transition) override;
        void highlightTransitionVisualization(int index, std::vector<PoseBasePtr> transition) override;
        void removeTransitionVisualization(int index) override;
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// METHODS FOR WAYPOINTS
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void addWaypointVisualization(int index, PoseBasePtr waypoint, VirtualRobot::EndEffectorPtr tcp) override;
        void removeWaypointVisualization(int index) override;
        void removeAllWaypointVisualizations() override;
        void setSelectedWaypoint(int index) override;

        void clearTrajectory() override;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// SETTING MANIPULATOR
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void setManipulator(VirtualRobot::RobotNodeSetPtr kc, std::vector<float>jointAngles) override;
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// UPDATING OF VISUALIZATION
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        RobotVisualizationPtr reproduce(QWidget* parent) override;

        void updateRobotVisualization() override;
        void enableVisualization() override;
        void disableVisualization() override;


        //inherited by VisualizationSubject
        Eigen::Matrix4f getManipulatorPose() override;
        int getSelectedWaypoint() override;

    protected:
        AdvancedVisualizationFactoryPtr createAdvancedVisualizationFactory() override;

    private:

        AdvancedCoinVisualizationFactoryPtr factory; //produces Visualizations
        std::shared_ptr<RobotViewer> viewer; //adaptedClass
        SoDragger* dragger;//muss vor manipCallback initialisiert werden
        void refreshSelectedPoint();
        SoNodeList transitions;
        SoNodeList wayPoints;
        int selectedWaypoint;
        int wayPointCounter;

        //Manipulator Utils
        static void manipFinishCallback(void* data, SoDragger* dragger);
        static void manipMovedCallback(void* data, SoDragger* dragger);
        static void autoFollowSensorTimerCB(void* data, SoSensor* sensor);
        ManipulatorVisualization* manipulator;
        SoTimerSensor* autoFollowSensor;
        bool manipulatorMoved;


        //Visualization Update
        SoTimerSensor* robotUpdateSensor;
        static void robotUpdateTimerCB(void* data, SoSensor* sensor);
        bool startUpCameraPositioningFlag = true;
        SoCamera* camera;//pointer to camera of viewer to change perspective easyly

        //Selection utils
        SoSelection* selected;
        static void made_selection(void* data, SoPath* path);
        static void unmade_selection(void* data, SoPath* path);
        static SoPath* pickFilterCB(void* data, const SoPickedPoint* pick);
    };
}


#endif
