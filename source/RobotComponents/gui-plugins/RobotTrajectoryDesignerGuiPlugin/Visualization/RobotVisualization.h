/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef ROBOTVISUALIZATION_H
#define ROBOTVISUALIZATION_H

#include <RobotAPI/interface/core/PoseBase.h>

#include <VirtualRobot/RobotNodeSet.h>

#include "AdvancedVisualizationFactory.h"

#include <QWidget>

#include "VisualizationSubject.h"

namespace armarx
{


    class RobotVisualization : public VisualizationSubject
    {

    public:
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// METHODS FOR VISUALIZATION SETUP
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief addRobotVisualization adds a robot to the robotviewer
         * @param robot the robot to be visualized
         * @param selectedChain the standard kinematic chain of the robot to use
         */
        virtual void addRobotVisualization(VirtualRobot::RobotPtr robot, QString selectedChain) = 0;

        /**
         * @brief addSceneVisualization visualizes the whole scene where the robot is placed in
         * @param scene
         */
        virtual void addSceneVisualization(VirtualRobot::ScenePtr scene) = 0;

        /**
         * @brief setCamera sets the camera at a certain position
         * @param position the 3D Position of the camera
         * @param pointAtA the highest point the camera points at
         * @param pointAtB the lowest point the camera looks at
         */
        virtual void setCamera(const Eigen::VectorXf position, const Eigen::VectorXf pointAtA, const Eigen::VectorXf pointAtB) = 0;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// METHODS FOR TRANSITIONS
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief addTransitionVisualization visualizes a transition that means an array of points in 3d space
         * @param transition the transition to be visualized
         */
        virtual void addTransitionVisualization(int index, std::vector<PoseBasePtr> transition) = 0;

        /**
         * @brief highlightTransitionVisualization highlights the transition with index i by changing the color of all points belonging to that transition
         * @param index the index of the transition to be highlighted starting with 0
         * @param transition the transition to be highlighted
         */
        virtual void highlightTransitionVisualization(int index, std::vector<PoseBasePtr> transition) = 0;

        /**
         * @brief removeTransitionVisualization removes visualization of a certain transition
         * @param index the index of the transition to be removed starting with 0
         */
        virtual void removeTransitionVisualization(int index) = 0;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// METHODS FOR WAYPOINTS
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief addWaypointVisualization visualizes a certain waypoint by putting a visual representaiton of a toolcenterpoint at a predefined pose
         * @param index the index of the waypoint to be added
         * @param waypoint Pose that should be realized by the WaypointVisualization
         * @param tcp the Endeffector that should be visualized
         */
        virtual void addWaypointVisualization(int index, PoseBasePtr waypoint, VirtualRobot::EndEffectorPtr tcp) = 0;

        /**
         * @brief removeWaypointVisualization removes the Waypoint with index so that it is no longer visualized
         * @param index the index of the waypoint to be removed starting with 0
         */
        virtual void removeWaypointVisualization(int index) = 0;

        /**
         * @brief removeAllWaypointVisualizations removes all waypoints that are currently visualized
         */
        virtual void removeAllWaypointVisualizations() = 0;

        /**
         * @brief removeWaypointVisualization removes the Waypoint with index so that it is no longer visualized
         * @param index the index of the waypoint to be removed starting with 0
         */
        virtual void setSelectedWaypoint(int index) = 0;


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// SETTING MANIPULATOR
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief setManipulator sets a new endeffector at a predefined pose that is calculated by using forward kinematic on the joint angles
         * @param kc the kinematic chain to use the endeffector of as the manipulator
         * @param jointAngles the joint angles of the robot to reach the manipulator
         */
        virtual void setManipulator(VirtualRobot::RobotNodeSetPtr kc, std::vector<float>jointAngles) = 0;

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// UPDATING OF VISUALIZATION
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief updateRobotVisualization refreshes the viewer so it visualizes the new robot state
         */
        virtual void updateRobotVisualization() = 0;
        /**
         * @brief enableVisualization shows the viewer
         */
        virtual void enableVisualization() = 0;
        /**
         * @brief disableVisualization hides the viewer
         */
        virtual void disableVisualization() = 0;
        /**
         * @brief clearTrajectory removes all visualization of waypoints and transitions but keeps the robot and the maipulator visualized
         */
        virtual void clearTrajectory() = 0;
        /**
         * @brief reproduce creates a  new view with thesame visualization nodes but an independent camera
         * @param parent the widget to attach the newly created viewer to
         * @return the reproduced viewer
         */
        virtual std::shared_ptr<RobotVisualization> reproduce(QWidget* parent) = 0;

    protected:
        VirtualRobot::ScenePtr scene;
        VirtualRobot::RobotPtr robot;
        virtual AdvancedVisualizationFactoryPtr createAdvancedVisualizationFactory() = 0;

    };

    using RobotVisualizationPtr = std::shared_ptr<RobotVisualization>;
}


#endif
