/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "CoinManipulatorVisualizationAdapter.h"

#include "../KinematicSolver.h"



using namespace armarx;
using namespace VirtualRobot;


CoinManipulatorVisualizationAdapter::CoinManipulatorVisualizationAdapter()
{
    adaptedManipulation = new ManipulatorVisualization();
}

CoinManipulatorVisualizationAdapter::~CoinManipulatorVisualizationAdapter()
{

}

void CoinManipulatorVisualizationAdapter::setVisualization(RobotPtr robot, RobotNodeSetPtr nodeSet)
{
    adaptedManipulation->setVisualization(robot, nodeSet);
}

Eigen::MatrixXf CoinManipulatorVisualizationAdapter::getUserDesiredPose()
{
    return adaptedManipulation->getUserDesiredPose();
}

SoSeparator* CoinManipulatorVisualizationAdapter::getVisualization()
{
    return (SoSeparator*) adaptedManipulation;
}

void CoinManipulatorVisualizationAdapter::attachCallback(SoDraggerCB* finish, SoDraggerCB* moved, void* data)
{
    adaptedManipulation->addManipFinishCallback(finish, data);
    adaptedManipulation->addManipMovedCallback(moved, data);
}

