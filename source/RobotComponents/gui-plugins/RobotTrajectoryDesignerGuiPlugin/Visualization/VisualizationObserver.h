
/* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Visualization
* @author     Timo Birr
* @date       2018
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef VisualizationObserver_H
#define VisualizationObserver_H

#include <memory>


namespace armarx
{
    /**
     * @brief The AdvancedVisualizationFactory class is the abstract decorator of the Decorator-Pattern and decorates the VisualizationFactoy in Simox
     *
     * The additional Functionality is that it can also generate the Visualization for a Manipulator
     */
    class VisualizationObserver
    {
    public:
        /**
         * @brief refresh gets all relevant data from subject an updates itself accoringly
         * should be called by subject but can also be called from elsewhere
         */
        virtual void refresh() = 0;
    };

    using VisualizationObserverPtr = std::shared_ptr<VisualizationObserver>;
}

#endif
