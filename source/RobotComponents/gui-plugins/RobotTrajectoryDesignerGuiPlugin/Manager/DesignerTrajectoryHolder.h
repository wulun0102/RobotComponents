/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Manager
 * @author     Luca Quaer
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef DESIGNERTRAJECTORYHOLDER_H
#define DESIGNERTRAJECTORYHOLDER_H

#include <map>

#include <VirtualRobot/RobotNodeSet.h>

#include "DesignerTrajectoryManager.h"
#include "../Model/DesignerTrajectory.h"


//using DesignerTrajectoryPtr = std::shared_ptr<armarx::DesignerTrajectory>;

namespace armarx
{
    /**
     * @brief The DesignerTrajectoryHolder class enables creating, deleting and getting
     *        DesignerTrajectorManagers, which are unambiguously assigned to a RobotNodeSet.
     *
     * @see DesignerTrajectoryManager
     */

    class DesignerTrajectoryHolder
    {
    private:
        ////////////////////////////////////////////////////////////////////////////////////////////
        /// Attributes
        ////////////////////////////////////////////////////////////////////////////////////////////
        /*struct mapComparator
        {
            bool operator()(VirtualRobot::RobotNodeSetPtr lhs, VirtualRobot::RobotNodeSetPtr rhs) const
            {
                return lhs->getName().compare(rhs->getName());
            }
        };*/

        std::map < std::string, DesignerTrajectoryManagerPtr>
        designerTrajectories;
        EnvironmentPtr environment;


        TrajectoryPtr mergeTrajectories(unsigned int fps);
    public:
        ////////////////////////////////////////////////////////////////////////////////////////////
        /// Constructor(s)
        ////////////////////////////////////////////////////////////////////////////////////////////
        DesignerTrajectoryHolder(EnvironmentPtr environment);

        ////////////////////////////////////////////////////////////////////////////////////////////
        /// Public functions
        ////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief If the given RobotNodeSet is not already assigned to a DesignerTrajectoryManager
         *        a DesignerTrajectoryManager is created and assigned to this RobotNodeSet,
         *        else nothing happens.
         * @param rns RobotNodeSet
         * @return true, if the given RobotNodeSet was not already assigned to a
         *         DesignerTrajectoryManager;
         *         false, if the given RobotNodeSet was already assigned to a
         *         DesignerTrajectoryManager.
         */
        bool createDesignerTrajectoryManager(std::string rnsName);

        /**
         * @brief Deletes the DesignerTrajectoryManager assigned to the given RobotNodeSet.
         * @param rns RobotNodeSet
         * @return true, if DesignerTrajectoryManager existed and was successfully deleted;
         *         false, if no DesignerTrajectoryManager was assigned to the given RobotNodeSet
         */
        bool deleteDesignerTrajectoryManager(std::string rnsName);

        /**
         * @brief Returns the DesignerTrajectoryManager assigned to the given RobotNodeSet.
         * @param rns RobotNodeSet
         * @return DesignerTrajectoryManager assigned to the given RobotNodeSet.
         */
        DesignerTrajectoryManagerPtr getTrajectoryManager(std::string rnsName);

        //bool import(const DesignerTrajectory& designerTrajectory, bool force);
        /**
         * @brief Imports the given DesignerTrajectory.
         *
         * The DesignerTrajectory holds a RobotNodeSet. If this RobotNodeSet is not assigned to
         * a DesignerTrajectoryManager then a new DesignerTrajectoryManager is created with the
         * given DesignerTrajectory (return true).
         * If the DesignerTrajectory's RobotNodeSet is already assigned to a
         * DesignerTrajectoryManager there are two options:
         * - If force is true (default), the existing DesignerTrajectoryManager gets overridden by a
         *   new DesignerTrajectoryManager created with the given DesignerTrajectory (return true).
         * - If force is false, nothing happens (return false).
         *
         * @param designerTrajectory new DesignerTrajectory
         * @param force Boolean indicating wheather to force the import or not
         * @return (see detailed function description)
         */
        bool import(DesignerTrajectoryPtr designerTrajectory, bool force);

        /**
         * @brief Checks if there is already a DesignerTrajectoryManager assigned to the given
         *        RobotNodeSet.
         * @param rns RobotNodeSet
         * @return true, if a DesignerTrajectoryManager assigned to the given RobotNodeSet exists;
         *         false, if no DesignerTrajectoryManager is assigned to the given RobotNodeSet
         */
        bool rnsExists(std::string rnsName);

        /**
         * @brief Checks if the given RobotNodeSet is part of a RobotNodeSet to which a
         *        DesignerTrajectoryManager is assigned to
         * @param rns RobotNodeSet
         * @return true, if the given RobotNodeSet is part of a existing one;
         *         false, if the given RobotNodeSet is not part of a existing one
         */
        bool rnsIsPartOfExistingRns(std::string rnsName);

        /**
         * @brief isInCollision Checks if the given collision models (active and body) collide
         *        for the jointangle configurations given in the trajectory
         * @param t trajectory combining all trajectories represented by the managers
         * @param activeColModelName the collision model of the RobotNodeSet which gets manipulated
         * @param bodyColModelsNames the collision models to check against
         * @param fps resolution for collision checking in frames per second
         * @return true in case of a collision
         */
        bool isInCollision(std::string activeColModelName,
                           std::vector<std::string> bodyColModelsNames,
                           unsigned int fps);

    };

    ////////////////////////////////////////////////////////////////////////////////////////////////
    /// Typedefinitions
    ////////////////////////////////////////////////////////////////////////////////////////////////
    using DesignerTrajectoryHolderPtr = std::shared_ptr<armarx::DesignerTrajectoryHolder>;
}

#endif // DESIGNERTRAJECTORYHOLDER_H
