/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotTrajectoryDesigner::gui-plugins::RobotTrajectoryDesignerGuiPluginWidgetController
 * @author     Max Beddies
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_RobotTrajectoryDesigner_RobotTrajectoryDesignerGuiPlugin_WidgetController_H
#define _ARMARX_RobotTrajectoryDesigner_RobotTrajectoryDesignerGuiPlugin_WidgetController_H

#include <RobotComponents/gui-plugins/RobotTrajectoryDesignerGuiPlugin/ui_RobotTrajectoryDesignerGuiPluginWidget.h>
#include "View/Perspectives.h"
#include "View/SettingTab.h"
#include "View/TCPInformationTab.h"
#include "View/ToolBar.h"
#include "View/TransitionTab.h"
#include "View/WaypointTab.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

//#include "Controller/CollisionDialogController.h"
//#include "Controller/LanguageController.h"
#include "Controller/MementoController.h"
//#include "Controller/PopUpController.h"
#include "Controller/RobotVisualizationController.h"
//#include "Controller/ScenarioDialogController.h"
#include "Controller/SettingController.h"
#include "Controller/ShortcutController.h"
#include "Controller/TCPInformationController.h"
#include "Controller/TCPSelectionController.h"
#include "Controller/ToolBarController.h"
#include "Controller/TrajectoryController.h"
#include "Controller/TransitionController.h"
#include "Controller/ViewController.h"
#include "Controller/WaypointController.h"
#include "Controller/ExportDialogController.h"
#include "Controller/ImportDialogController.h"
#include "Controller/EnvironmentController.h"


namespace armarx
{
    /**
    \page ArmarXGui-GuiPlugins-RobotTrajectoryDesigner RobotTrajectoryDesigner
    \brief The RobotTrajectoryDesigner gui plugin allows the generation of trajectories
            for a given robot.

    API Documentation \ref RobotTrajectoryDesignerGuiPluginWidgetController

    \see RobotTrajectoryDesignerGuiPluginGuiPlugin
    */

    /**
     * \class RobotTrajectoryDesignerGuiPluginWidgetController
     * \brief Main controller of the GUI plugin, handling all other subcontrollers.
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        RobotTrajectoryDesignerGuiPluginWidgetController:
        public armarx::ArmarXComponentWidgetController
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit RobotTrajectoryDesignerGuiPluginWidgetController();

        /**
         * Controller destructor
         */
        virtual ~RobotTrajectoryDesignerGuiPluginWidgetController();

        /**
         * @see ArmarXWidgetController::RSettings()
         */
        virtual void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        virtual void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        virtual QString getWidgetName() const override
        {
            return "RobotTrajectoryDesigner";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        virtual void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        virtual void onConnectComponent() override;

        virtual void onDisconnectComponent() override;

    public slots:
        void initWidget();
        void connectWidget();

    signals:
        /* QT signal declarations */

    private:
        Ui::RobotTrajectoryDesignerGuiPluginWidget widget;

        //CollisionDialogControllerPtr collisionDialogControllerPtr;
        //LanguageControllerPtr languageControllerPtr;
        MementoControllerPtr mementoControllerPtr;
        //PopUpControllerPtr popUpControllerPtr;
        //ScenarioDialogControllerPtr scenarioDialogControllerPtr;
        SettingControllerPtr settingControllerPtr;
        ShortcutControllerPtr shortcutControllerPtr;
        TCPInformationControllerPtr tcpInformationControllerPtr;
        TCPSelectionControllerPtr tcpSelectionControllerPtr;
        ToolBarControllerPtr toolBarControllerPtr;
        TrajectoryControllerPtr trajectoryControllerPtr;
        TransitionControllerPtr transitionControllerPtr;
        ViewControllerPtr viewControllerPtr;
        WaypointControllerPtr waypointControllerPtr;
        ExportDialogControllerPtr exportDialogControllerPtr;
        ImportDialogControllerPtr importDialogControllerPtr;
        RobotVisualizationControllerPtr robotVisualizationControllerPtr;
        EnvironmentControllerPtr environmentControllerPtr;

        VirtualRobot::RobotPtr requestRobot(std::string robotFile);
    };
}

#endif
