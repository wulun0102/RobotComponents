#include <RobotTrajectoryDesigner/gui-plugins/RobotTrajectoryDesignerGuiPlugin/RobotTrajectoryDesignerGuiPluginGuiPlugin.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <VirtualRobot/XML/RobotIO.h>

class RobotTrajectoryDesignerTestEnvironment
{
public:
    RobotTrajectoryDesignerTestEnvironment(const std::string& testName, int registryPort = 11220, bool addObjects = true)
    {
        Ice::PropertiesPtr properties = Ice::createProperties();
        armarx::Application::LoadDefaultConfig(properties);
        const std::string project = "RobotAPI";
        armarx::CMakePackageFinder finder(project);

        if (!finder.packageFound())
        {
            ARMARX_ERROR << "ArmarX Package " << project << " has not been found!";
        }
        else
        {
            armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
        }

        std::string fn = "RobotAPI/robots/Armar3/ArmarIII.xml";
        ArmarXDataPath::getAbsolutePath(fn, fn);
        std::string robotFilename = fn;

        _spaceSavePath = test::getCmakeValue("PROJECT_BINARY_DIR") + "/Testing/Temporary/spaces/";

        if (loadSpaces)
        {
            properties->setProperty("ArmarX.RobotIK.ReachabilitySpacesFolder", _spaceSavePath);
            properties->setProperty("ArmarX.RobotIK.InitialReachabilitySpaces", "testReachabilitySpace.bin");
        }

        _iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
        _iceTestHelper->startEnvironment();

        // The manager allows you to create new ArmarX components
        _manager = new TestArmarXManager(testName, _iceTestHelper->getCommunicator(), properties);
        if (addObjects)
        {
            // This is how you create components.
            _robotTrajectoryDesigner = _manager->createComponentAndRun<YourComponent, YourComponentInterfacePrx>("ArmarX", "RobotTrajectoryDesigner");
        }

        _robotModel = VirtualRobot::RobotIO::loadRobot(robotFilename, VirtualRobot::RobotIO::eStructure);
    }
    ~RobotTrajectoryDesignerTestEnvironment()
    {
        _manager->shutdown();
    }
    // In your tests, you can access your component through this proxy
    RobotTrajectoryDesignerInterfacePrx _robotTrajectoryDesigner;
    TestArmarXManagerPtr _manager;
    IceTestHelperPtr _iceTestHelper;
    VirtualRobot::RobotPtr _robotModel;

};
using RobotTrajectoryDesignerTestEnvironmentPtr = std::shared_ptr<RobotTrajectoryDesignerTestEnvironment>;
