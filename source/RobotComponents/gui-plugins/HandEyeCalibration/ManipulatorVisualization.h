/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::ManipulatorVisualization
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

//Coin includes
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/manips/SoTransformerManip.h>
#include <Inventor/nodes/SoMaterial.h>

//VirtualRobot
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

//Boost includes
#include <boost/smart_ptr/intrusive_ptr.hpp>

namespace armarx
{
    //Let boost use referencing of Inventor to manage objects memory
    //and not its own shared pointer referencing
    using SoTransformerManipPtr = boost::intrusive_ptr<SoTransformerManip>;
    inline void intrusive_ptr_add_ref(SoTransformerManip* obj)
    {
        obj->ref();
    }
    inline void intrusive_ptr_release(SoTransformerManip* obj)
    {
        obj->unref();
    }

    class ManipulatorVisualization : SoSeparator
    {
    public:
        ManipulatorVisualization();
        ~ManipulatorVisualization() override;

        void setVisualization(VirtualRobot::EndEffectorPtr endEffector);
        void removeVisualization();

        void setColor(float r, float g, float b);

        Eigen::Matrix4f getUserDesiredPose();
        std::string getUserDesiredPoseString();
        void setUserDesiredPose(Eigen::Matrix4f globalPose);

        bool getIsVisualizing() const
        {
            return isVisualizing;
        }

    private:
        SoTransformerManipPtr manip;
        SoMaterial* material;
        bool isVisualizing;
        bool hasEndEffectorVisualizer;
        Eigen::Matrix4f localTransformation;
    };
}


