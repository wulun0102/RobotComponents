armarx_set_target("HandEyeCalibrationGuiPlugin")

armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

set(SOURCES
./HandEyeCalibrationGuiPlugin.cpp 
./HandEyeCalibrationWidgetController.cpp
./ManipulatorVisualization.cpp
./PointCloudVisualization.cpp
./DisplayWidget.cpp
)

set(HEADERS
./HandEyeCalibrationGuiPlugin.h 
./HandEyeCalibrationWidgetController.h
./ManipulatorVisualization.h
./PointCloudVisualization.h
./DisplayWidget.h
)

set(GUI_MOC_HDRS ${HEADERS})
set(GUI_UIS HandEyeCalibrationWidget.ui)

set(COMPONENT_LIBS
    VisionXPointCloud
    VisionXCore
    RobotAPICore
    SimpleConfigDialog
    SimpleJsonLogger
    ${PCL_FILTERS_LIBRARY}
)

if(ArmarXGui_FOUND)
    armarx_gui_library(HandEyeCalibrationGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
