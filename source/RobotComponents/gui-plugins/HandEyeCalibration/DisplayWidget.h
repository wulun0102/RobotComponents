/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::DisplayWidget
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

/* Qt headers */
#include <QWidget>

/* Boost headers */
#include <boost/smart_ptr/shared_ptr.hpp>

/* Coin headers */
#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

namespace armarx
{
    class Display : public SoQtExaminerViewer
    {
    public:
        Display(QWidget* widget);
        ~Display() override;
        SoSeparator* getRootNode();
        void cameraViewAll();
        void cameraViewNode(SoNode* node, const float slack = 1.0);

    private:
        //        virtual SbBool processSoEvent(const SoEvent* const event);
        SoSeparator* sceneRootNode;
        SoSeparator* contentRootNode;
        SoPerspectiveCamera* camera;
    };

    class DisplayWidget : public QWidget
    {
        Q_OBJECT

    public:
        explicit DisplayWidget(QWidget* parent = 0);
        ~DisplayWidget() override;

        Display* getDisplay();

    private:
        Display* display;
    };
}
