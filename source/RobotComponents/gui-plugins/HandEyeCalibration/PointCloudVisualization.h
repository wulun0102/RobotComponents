/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::PointCloudVisualization
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

//PCL
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/filters/filter.h>

//Coin includes
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/SbColor.h>

namespace armarx
{
    using PointT = pcl::PointXYZRGBA;

    class PointCloudVisualization : public SoSeparator
    {
    public:
        PointCloudVisualization();
        ~PointCloudVisualization() override;

        void setVisualization(pcl::PointCloud<PointT>::ConstPtr cloud);
        void setDrawColor(SbColor color);
        void setPointSize(int size);
        void resetDrawColor();
    private:
        SbColor color;
        bool useOriginalColors = true;
        int pointSize = 1;
    };
}
