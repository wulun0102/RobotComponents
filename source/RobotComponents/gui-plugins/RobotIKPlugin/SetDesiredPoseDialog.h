#ifndef SETDESIREDPOSEDIALOG_H
#define SETDESIREDPOSEDIALOG_H

#include <QDialog>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>

namespace Ui
{
    class SetDesiredPoseDialog;
}

class SetDesiredPoseDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetDesiredPoseDialog(QWidget* parent = 0);
    ~SetDesiredPoseDialog();

    armarx::FramedPosePtr getDesiredPose();

private:
    Ui::SetDesiredPoseDialog* ui;

    bool stringToJSON(std::string string, armarx::JsonObjectPtr& result) const;

    armarx::FramedPosePtr result;

private slots:
    void checkJSON();
    void formatInput();
    void parseInputAndSetPose();
};

#endif // SETDESIREDPOSEDIALOG_H
