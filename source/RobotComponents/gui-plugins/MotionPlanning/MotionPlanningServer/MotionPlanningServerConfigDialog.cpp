/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#include <IceUtil/UUID.h>

#include "MotionPlanningServerConfigDialog.h"


namespace armarx
{
    MotionPlanningServerConfigDialog::MotionPlanningServerConfigDialog(QWidget* parent) :
        QDialog(parent),
        uuid(IceUtil::generateUUID())
    {
        ui.setupUi(this);
        finder = new armarx::IceProxyFinder<MotionPlanningServerInterfacePrx>(this);
        finder->setSearchMask("*Server");
        ui.horizontalLayout->addWidget(finder);
    }

    void MotionPlanningServerConfigDialog::onInitComponent()
    {
        finder->setIceManager(getIceManager());
    }

    void MotionPlanningServerConfigDialog::onConnectComponent()
    {
    }

    std::string MotionPlanningServerConfigDialog::getDefaultName() const
    {
        return "MotionPlanningServerConfigDialog" + uuid;
    }
}
