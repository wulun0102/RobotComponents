/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <QDialog>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <RobotComponents/components/MotionPlanning/MotionPlanningServer.h>

#include <RobotComponents/gui-plugins/MotionPlanning/MotionPlanningServer/ui_MotionPlanningServerConfigDialog.h>

namespace armarx
{
    /**
     * @brief The config dialog of the palnning server gui.
     * It has a proxy finder for a planning server
     */
    class MotionPlanningServerConfigDialog :
        public QDialog,
        virtual public armarx::ManagedIceObject
    {
        Q_OBJECT

    public:
        /**
         * @brief ctor
         * @param parent the dialog's parent
         */
        explicit MotionPlanningServerConfigDialog(QWidget* parent = nullptr);
        /**
         * @brief dtor
         */
        ~MotionPlanningServerConfigDialog() override = default;

    private:
        /**
         * @brief Proxy finder for a PlanningServerInterfacePrx
         */
        armarx::IceProxyFinder<MotionPlanningServerInterfacePrx>* finder;
        /**
         * @brief The internal ui.
         */
        Ui::MotionPlanningServerConfigDialog ui;
        /**
         * @brief The used uuid.
         */
        std::string uuid;

        friend class MotionPlanningServerWidgetController;

        // ManagedIceObject interface
    protected:
        /**
         * @brief Initializes the proxy finder
         */
        void onInitComponent() override;
        /**
         * @brief noop
         */
        void onConnectComponent() override;
        /**
         * @brief Returns the dialog's default name.
         * @return The dialog's default name.
         */
        std::string getDefaultName() const override;
    };
}
