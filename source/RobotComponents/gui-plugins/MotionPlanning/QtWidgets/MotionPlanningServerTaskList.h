/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>

#include <QModelIndex>
#include <QWidget>

#include <boost/optional.hpp>

namespace Ui
{
    class MotionPlanningServerTaskList;
}

namespace armarx
{
    class MotionPlanningServerTaskList : public QWidget
    {
        Q_OBJECT

    public:
        explicit MotionPlanningServerTaskList(QWidget* parent = nullptr);
        ~MotionPlanningServerTaskList() override;

        boost::optional<Ice::Long> getSelectedId();

        void setMotionPlanningServer(const MotionPlanningServerInterfacePrx& planningServerPrx)
        {
            planningServerProxy = planningServerPrx;
        }

    public slots:
        void updateList();
        void clearList();
        void enableAutoUpdate(bool enabled);

    protected:
        void timerEvent(QTimerEvent*) override;

    private:
        Ui::MotionPlanningServerTaskList* ui;
        MotionPlanningServerInterfacePrx planningServerProxy;
    };
}
