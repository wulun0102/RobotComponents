/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#include "MotionPlanningServerTaskList.h"
#include <RobotComponents/gui-plugins/MotionPlanning/QtWidgets/ui_MotionPlanningServerTaskList.h>

#include <Ice/LocalException.h>

#include <RobotComponents/components/MotionPlanning/util/PlanningUtil.h>

using namespace armarx;

MotionPlanningServerTaskList::MotionPlanningServerTaskList(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::MotionPlanningServerTaskList)
{
    ui->setupUi(this);
    startTimer(100);
    connect(ui->pushButtonRefresh, SIGNAL(clicked()), this, SLOT(updateList()));
}

MotionPlanningServerTaskList::~MotionPlanningServerTaskList()
{
    delete ui;
}

boost::optional<Ice::Long> MotionPlanningServerTaskList::getSelectedId()
{
    const auto&& rowsSelection = ui->tableWidget->selectionModel()->selectedRows();
    if (rowsSelection.empty())
    {
        return boost::none;
    }
    const auto rowIndex = rowsSelection.first().row();
    return ui->tableWidget->item(rowIndex, 4)->text().toLong();
}

void MotionPlanningServerTaskList::updateList()
{
    auto selected = getSelectedId();
    clearList();
    if (!planningServerProxy)
    {
        return;
    }
    try
    {
        //get data
        const auto data = planningServerProxy->getAllTaskInfo();
        ui->tableWidget->setRowCount(data.size());
        std::size_t rowToSelect = 0;
        //fill table
        for (std::size_t row = 0; row < data.size(); ++row)
        {
            const auto& datum = data.at(row);
            ui->tableWidget->setItem(row, 0, new QTableWidgetItem {QString::fromStdString(datum.taskName)});
            ui->tableWidget->setItem(row, 1, new QTableWidgetItem {QString::fromStdString(toString(datum.status))});
            ui->tableWidget->setItem(row, 2, new QTableWidgetItem
            {
                datum.taskIdent.category.empty() ?
                QString::fromStdString(datum.taskIdent.name) :
                QString::fromStdString(datum.taskIdent.category) + '\n' + QString::fromStdString(datum.taskIdent.name)
            });
            ui->tableWidget->setItem(row, 3, new QTableWidgetItem {QString::fromStdString(datum.typeId)});
            ui->tableWidget->setItem(row, 4, new QTableWidgetItem {QString::number(datum.internalId)});
            if (selected.is_initialized() && selected.get() == datum.internalId)
            {
                rowToSelect = row;
            }
        }
        ui->tableWidget->selectRow(rowToSelect);
    }
    catch (Ice::UserException&)
    {
        //don't spamm on disconnect
        return;
    }
    catch (Ice::NotRegisteredException&)
    {
        //don't spamm on disconnect
        return;
    }
    catch (Ice::ConnectionRefusedException&)
    {
        //don't spamm on disconnect
        return;
    }
}

void MotionPlanningServerTaskList::clearList()
{
    ui->tableWidget->setRowCount(0);
}

void MotionPlanningServerTaskList::enableAutoUpdate(bool enabled)
{
    ui->checkBoxAutoRefresh->setChecked(enabled);
}

void MotionPlanningServerTaskList::timerEvent(QTimerEvent*)
{
    if (ui->checkBoxAutoRefresh->isChecked())
    {
        updateList();
    }
}
