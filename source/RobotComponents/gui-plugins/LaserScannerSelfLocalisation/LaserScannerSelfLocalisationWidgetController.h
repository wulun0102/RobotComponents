/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::gui-plugins::LaserScannerSelfLocalisationWidgetController
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotComponents/gui-plugins/LaserScannerSelfLocalisation/ui_LaserScannerSelfLocalisationWidget.h>
#include <RobotComponents/gui-plugins/LaserScannerSelfLocalisation/QPainterWidget.h>
#include <RobotComponents/interface/components/LaserScannerSelfLocalisation.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <ArmarXCore/interface/core/ArmarXManagerInterface.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/core/system/Synchronization.h>

#include <Eigen/Eigen>
#include <atomic>

namespace armarx
{
    /**
    \page RobotComponents-GuiPlugins-LaserScannerSelfLocalisation LaserScannerSelfLocalisation
    \brief The LaserScannerSelfLocalisation allows visualizing and configuring the self localisation process

    API Documentation \ref LaserScannerSelfLocalisationWidgetController

    \see LaserScannerSelfLocalisationGuiPlugin
    */

    /**
     * \class LaserScannerSelfLocalisationWidgetController
     * \brief LaserScannerSelfLocalisationWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT LaserScannerSelfLocalisationWidgetController :
        public armarx::ArmarXComponentWidgetController,
        public armarx::LaserScannerSelfLocalisationListener
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit LaserScannerSelfLocalisationWidgetController();

        /**
         * Controller destructor
         */
        ~LaserScannerSelfLocalisationWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        QString getWidgetName() const override
        {
            return "RobotControl.LaserScannerSelfLocalisation";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;

        void configured() override;

        void onPaintCanvas(QPainter& painter, QSize size);

        // LaserScannerSelfLocalisationListener interface
        void reportCorrectedPose(Ice::Float x, Ice::Float y, Ice::Float theta, const Ice::Current&) override;
        void reportPoseUncertainty(Ice::Float x, Ice::Float y, Ice::Float theta, const Ice::Current&) override;

        //void reportExtractedEdges(const LineSegment2DSeq& edges, const Ice::Current&) override;
        void reportLaserScanPoints(const Vector2fSeq& globalPoints, const Ice::Current&) override;
        void reportExtractedEdges(const LineSegment2DSeq&, const Ice::Current&) override;

    public slots:
        void onSpinBoxChanged(int value);
        void onSpinBoxChanged(double value);
        void onNewDataReported();
        void onSetPoseClick();
        void onCanvasClick(QPoint p);

    signals:
        /* QT signal declarations */
        void newDataReported();

    private:
        void updateProperties();
        void readProperties();

        enum class SetPoseState
        {
            None,
            Position,
            Wait,
            Orientation,
            Finished,
        };

    protected:
        void timerEvent(QTimerEvent* event) override;

    private:
        Ui::LaserScannerSelfLocalisationWidget widget;
        QPointer<SimpleConfigDialog> dialog;
        QPainterWidget* painterWidget;
        int paintTimerId;

        std::string localisationName;
        LaserScannerSelfLocalisationInterfacePrx localisation;
        ArmarXManagerInterfacePrx localisationManager;
        Ice::PropertiesAdminPrx localisationAdmin;
        LineSegment2DSeq map;

        std::atomic<float> poseX;
        std::atomic<float> poseY;
        std::atomic<float> poseTheta;

        std::atomic<float> poseUncertaintyX;
        std::atomic<float> poseUncertaintyY;
        std::atomic<float> poseUncertaintyTheta;


        Mutex pointsMutex;
        Vector2fSeq points;
        LineSegment2DSeq edges;

        std::vector<QLine> lines;
        std::vector<QPoint> qpoints;
        std::vector<QLine> qedges;

        SetPoseState setPoseState;
        QPoint setPosition;
        QPoint setOrientation;
    };
}

