/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents
* @author     Valerij Wittenbeck (valerij dot wittenbeck at student dot kit dot edu)
* @copyright  2016 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <RobotComponents/interface/components/GraspingManager/GraspGeneratorInterface.ice>
#include <RobotComponents/interface/components/GraspingManager/RobotPlacementInterface.ice>

#include <ArmarXCore/interface/observers/Timestamp.ice>

module armarx
{

    interface GraspSelectionCriterionInterface
    {
        GeneratedGraspList filterGrasps(GeneratedGraspList grasps);
        GraspingPlacementList filterPlacements(GraspingPlacementList placements);
        ["cpp:const"] idempotent int getHash();
    };

    sequence<GraspSelectionCriterionInterface*> GraspSelectionCriterionInterfaceList;

};




