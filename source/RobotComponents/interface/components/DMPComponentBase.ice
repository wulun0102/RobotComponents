/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotCOMPONENTS::DMPComponent
* @author     You
* @date       2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

//#include <MemoryX/interface/memorytypes/MemoryEntities.ice>



module armarx
{
    struct cState{
        double pos;
        double vel;
        double acc;
    };

    dictionary<string, Ice::DoubleSeq> DMPConfigMap;

    sequence<cState> cStateVec;

    sequence<string> SVector;

    sequence<double> DVector;

    sequence<Ice::DoubleSeq> Vec2D;

    struct nStateValues{
        Ice::DoubleSeq canonicalValues;
        Vec2D nextState;
    };

    ["cpp:virtual"]
    class DMPInstanceBase
    {
        Vec2D calcTrajectory(double startTime, double timestep, double endTime,
                             Ice::DoubleSeq goal,
                             cStateVec currentStates,
                             Ice::DoubleSeq canonicalValues, double temporalFactor) throws InvalidArgumentException;

		
		// DMP parameters setter
        void setParameter(string paraId, Ice::DoubleSeq value) throws InvalidArgumentException;
        void setGoal(Ice::DoubleSeq value) throws InvalidArgumentException;
        void setStartPosition(Ice::DoubleSeq value) throws InvalidArgumentException;
        void setDMPState(cStateVec state) throws InvalidArgumentException;
        void setConfigurationMap(DMPConfigMap value) throws InvalidArgumentException;
        void setAmpl(int dim, double value);
        void setCanonicalValues(Ice::DoubleSeq value) throws InvalidArgumentException;
        void setTemporalFactor(double value);

		// DMP parameters getter
		double getAmpl(int dim);
        double getForceTerm(Ice::DoubleSeq value, int dim);
        double getTemporalFactor();
        cStateVec getNextState(cStateVec states) throws InvalidArgumentException;
        cStateVec getNextStateWithCurrentState();
        cStateVec getCurrentState();
        Ice::DoubleSeq getCanonicalValues();
		double getDampingFactor();
        double getSpringFactor();
		string getDMPType();
        int getDMPDim();
        Ice::DoubleSeq getStartPosition();

		// Trajectory analyzer
        Ice::DoubleSeq getTrajGoal();
        cStateVec getTrajStartState();
        void readTrajectoryFromFile(string file, double times);
        
		// DMP tranining
		void trainDMP();

		// merge from DMPRefactoring
 		nStateValues calcNextState(double t, Ice::DoubleSeq goal, double currentT, Vec2D currentStates, Ice::DoubleSeq canonicalValues, double temporalFactor) throws InvalidArgumentException;
        nStateValues calcNextStateFromConfig(double t, double currentT, Vec2D currentStates, Ice::DoubleSeq canonicalValues) throws InvalidArgumentException;
//        nStateValues calcNextStateFromConfigGlobalTimestep(Vec2D currentStates, Ice::DoubleSeq canonicalValues) throws InvalidArgumentException;
        Vec2D calcTrajectoryV2(Ice::DoubleSeq timestamps, Ice::DoubleSeq goal, Vec2D currentStates, Ice::DoubleSeq canonicalValues, double temporalFactor) throws InvalidArgumentException;
        Vec2D calcTrajectoryFromConfig(Ice::DoubleSeq timestamps, Vec2D currentStates, Ice::DoubleSeq canonicalValues) throws InvalidArgumentException;
//        void setTimeStamps(Ice::DoubleSeq value) throws InvalidArgumentException;

    };

    ["cpp:virtual"]
    class DMPComponentBase
    {
        DMPInstanceBase* getDMP(string dmpName);
        DMPInstanceBase* instantiateDMP(string name, string dmptype, int kernelSize) throws InvalidArgumentException;
        DMPInstanceBase* getDMPFromDatabase(string name, string entity) throws InvalidArgumentException;
        DMPInstanceBase* getDMPFromDatabaseById(string dbId) throws InvalidArgumentException;
        DMPInstanceBase* getDMPFromFile(string name, string dmpName) throws InvalidArgumentException;
        void removeDMPFromDatabase(string dmpname);
        void removeDMPFromDatabaseById(string dbId) throws InvalidArgumentException;
        void storeDMPInDatabase(string name, string cname) throws InvalidArgumentException;
        void storeDMPInFile(string fileName, string dmpName) throws InvalidArgumentException;

		// convenient functions
        Vec2D calcTrajectory(string name, double startTime, double timestep, double endTime, Ice::DoubleSeq goal, cStateVec currentStates, Ice::DoubleSeq canonicalValues, double temporalFactor) throws InvalidArgumentException;
        void setDMPState(string name, cStateVec state) throws InvalidArgumentException;
        void setGoal(string name, Ice::DoubleSeq value) throws InvalidArgumentException;
        void setStartPosition(string name, Ice::DoubleSeq value) throws InvalidArgumentException;
        void setCanonicalValues(string name, Ice::DoubleSeq value) throws InvalidArgumentException;
        cStateVec getNextState(string name, cStateVec states) throws InvalidArgumentException;
        cStateVec getCurrentState(string name);
        void trainDMP(string name) throws InvalidArgumentException;
        void readTrajectoryFromFile(string name, string file, double times);
        double getForceTerm(string name, Ice::DoubleSeq value, int dim);
     	double getTemporalFactor(string name);
        void setTemporalFactor(string name, double tau);
		Ice::DoubleSeq getCanonicalValues(string name);
        Ice::DoubleSeq getTrajGoal(string name);
        cStateVec getTrajStartState(string name);
        Ice::DoubleSeq getStartPosition(string name);
        void setAmpl(string name, int dim, double value);
        double getAmpl(string name, int dim);
        double getDampingFactor(string name);
        double getSpringFactor(string name);
        string getDMPType(string name);
        int getDMPDim(string name);

		// DMP manager
        SVector getDMPNameList();
        void eraseDMP(string name);
        bool isDMPExist(string name);

		// time manager
        double getTimeStep();
        void setTimeStep(double timestep);
        double getCurrentTime();
        void resetTime();
        void resetCanonicalValues();
    };
};

