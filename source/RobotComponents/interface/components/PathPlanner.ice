/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotSkills::PathPlanner
* @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
* @date       2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <RobotAPI/interface/core/PoseBase.ice>


module armarx
{

    /**
     * @brief Holds information about an object used for collision checks (class and pose)
     */
    struct ObjectPositionBase
    {
        /**
         * @brief The object
         */
        memoryx::ObjectClassBase objectClassBase;
        /**
         * @brief The pose used for the object
         */
        PoseBase objectPose;
    };

    sequence<ObjectPositionBase> ObjectPositionBaseList;



    ["cpp:virtual"]
    /**
     * @brief Superclass for all path planners.
     *
     * Does administration work. (setting of agent and collision object)
     */
    class PathPlannerBase
    {
        /**
         * @brief Sets the collision objects.
         * @param objects The collision objects to set.
         */
        void setCollisionObjects(ObjectPositionBaseList objects) throws InvalidArgumentException;

        /**
         * @brief Adds collision objects.
         * @param objects The collision objects to add.
         */
        void addCollisionObjects(ObjectPositionBaseList objects) throws InvalidArgumentException;

        /**
         * @brief Removes all collision objects.
         */
        void clearCollisionObjects();

        /**
         * @brief Sets the agent.
         * @param agent The agent.
         * @param agentColModelName The agent's collision model.
         */
        void setAgent(memoryx::AgentInstanceBase agent, string agentColModelName) throws InvalidArgumentException;

        /**
         * @brief Sets the safety margin. (the distance agent <-> any object will always be >= or no path will be found)
         * @param minDistance
         */
        void setSafetyMargin(float minDistance) throws InvalidArgumentException;

        /**
         * @brief Planns a path from from to to.
         * The z coordinate is interpreted as rotation around the z-axis (in rad).
         * @param from The starting point.
         * @param to The destination point.
         * @return Returns the path. If no path was found an empty list is returned.
         */
        ["cpp:const"]
        Vector3BaseList getPath(Vector3Base from, Vector3Base to);
    };

    /**
     * @brief An 3 dimensional integer vector.
     */
    struct Vector3IBase
    {
        int x = 0;
        int y = 0;
        int z = 0;
    };

    sequence<Vector3IBase> Vector3IBaseList;

    /**
     * @brief Implements path planning using A*.
     */
    ["cpp:virtual"]
    class AStarPathPlannerBase extends PathPlannerBase
    {
        /**
         * @brief Sets the translation step size. (size of steps along x- or y-axis)
         * @param tStepSize The translation step size.
         */
        void setTranslationtStepSize(float tStepSize) throws InvalidArgumentException;

        /**
         * @brief Sets the rotation step size. (size of rotation steps in rad around the z-axis)
         * @param rStepSize The rotation step size.
         */
        void setRotationStepSize(float rStepSize);

        /**
         * @brief Sets the bounding range for the x-coordinate. (No path with a node <min or >max will be found)
         * @param xBoundingRange The bounding range for the x-coordinate.
         */
        void setBoundingXRange(FloatRange xBoundingRange) throws InvalidArgumentException;

        /**
         * @brief setBoundingYRange Sets the bounding range for the y-coordinate. (No path with a node <min or >max will be found)
         * @param yBoundingRange The bounding range for the y-coordinate.
         */
        void setBoundingYRange(FloatRange yBoundingRange) throws InvalidArgumentException;

        /**
         * @brief Sets allowed neighbours.
         * x/y/z are the number of steps in x/y/z direction.
         *
         * Used to calculate neighbours of a found node.
         *
         * @param neighbours Allowed neighbours.
         */
        void setNeighbourSteps(Vector3IBaseList neighbours);

        /**
         * @brief Sets the rotation factor for the heuristic used by A*.
         *
         * The heuristic is: rotationFactor * rotationDelta + distanceFactor * sqrt(xTranslationDelta^2 + yTranslationDelta^2)
         * @param factor The rotation factor.
         */
        void setRotationFactorForHeuristic(float factor);

        /**
         * @brief Sets the distance factor for the heuristic used by A*.
         *
         * The heuristic is: rotationFactor * rotationDelta + distanceFactor * sqrt(xTranslationDelta^2 + yTranslationDelta^2)
         * @param factor The distance factor.
         */
        void setDistanceFactorForHeuristic(float factor);

        ["protected"] float tStepSize;
        ["protected"] FloatRange xBoundingRange;
        ["protected"] FloatRange yBoundingRange;
        ["protected"] Vector3IBaseList neighbourDeltas;
        ["protected"] float rotationFactorForHeuristic;
        ["protected"] float distanceFactorForHeuristic;
    };
};

