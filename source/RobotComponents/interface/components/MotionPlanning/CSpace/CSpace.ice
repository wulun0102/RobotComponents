/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/interface/components/MotionPlanning/DataStructures.ice>

module armarx
{
    /**
     * @brief Describes a planning cspace. This cspace contains everything
     * specific for a planning task:
     *     - The cspace's bounds for all dimensions. (The cspace has bounds.size() dimensions)
     *     - A function to check whether a configuration is a collision.
     * All functions accepting a configuration assume one value for each dimension is passed!
     *     - If less values than the cspace's dimensionality are passed the behavior may be undefined.
     *     - If more values than the cspace's dimensionality are passed the additional values are ignored.
     */
    ["cpp:virtual"]
    class CSpaceBase
    {
        /**
         * @param configuration The values to check for collision.
         * Call initCollisionTest prior to calls to this function.
         * @return Whether the configuration is collision free.
         */
        bool isCollisionFree(["cpp:array"] VectorXf config);

        /**
         * @brief Returns whether the configuration is valid.
         * @param config The configuration to check.
         * @return  Whether for all dimensions d_i following evaluates to true:
         *     d_i.periodical || config[i] <= d.low && config[i] >= d.high
         */
        ["cpp:const"] idempotent bool isValidConfiguration(["cpp:array"] VectorXf configuration);

        /**
         * @brief Makes a deep copy.
         * It should be possible to call this function without locking, while the cspace is
         * used to check collisions.
         * @return Returns the deep copy.
         */
        CSpaceBase clone();

        /**
         * @brief Initializes implementation specific local structures used for collision testing.
         * Has to be called before any call to isCollisionFree.
         */
        void initCollisionTest();

        /**
         * @return The bounds of this cspace's dimensions.
         */
        ["cpp:const"] FloatRangeSeq getDimensionsBounds();
        /**
         * @return The number of this cspace's dimensions.
         */
        ["cpp:const"] long getDimensionality();
    };

    ["cpp:virtual"]
    class CSpaceAdaptorBase extends CSpaceBase
    {
        ["cpp:const"] idempotent CSpaceBase getOriginalCSpace();
        ["protected"] CSpaceBase originalCSpace;
    };

    struct CSpaceAndPaths
    {
        PathSeq paths;
        CSpaceBase cspace;
    };
};
