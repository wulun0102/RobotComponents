/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/core/util/distributed/RemoteHandle/ClientSideRemoteHandleControlBlock.ice>

#include <RobotComponents/interface/components/MotionPlanning/CSpace/CSpace.ice>
#include <RobotComponents/interface/components/MotionPlanning/DataStructures.ice>
#include <RobotComponents/interface/components/MotionPlanning/Tasks/MotionPlanningTask.ice>

module armarx
{
//    interface MotionPlanningTaskControlInterface;
//    class MotionPlanningTaskBase;

    /**
     * @brief The Server responsible for:
     *     - accepting new and aborting running/queued tasks
     *     - dispatching tasks for execution
     *     - global configuration for algorithms (e.g. limits for execution power
     */
    interface MotionPlanningServerInterface
    {
        /**
         * @brief Adds a task for planning.
         * @param task The task to add
         * @return Returns the task's proxy. (null if the server is in destruction)
         */
        armarx::ClientSideRemoteHandleControlBlockBase enqueueTask(MotionPlanningTaskBase task) throws armarx::ServerShuttingDown;

        /**
         * @return Returns the number tasks on the server. (queued and completed)
         */
        ["cpp:const"] idempotent long getNumberOfTasks();
        /**
         * @return Returns the number of queued tasks.
         */
        ["cpp:const"] idempotent long getNumberOfQueuedTasks();
        /**
         * @return Information about all current tasks.
         */
        ["cpp:const"] idempotent TaskInfoSeq getAllTaskInfo();

        armarx::ClientSideRemoteHandleControlBlockBase getCurrentTaskHandle();

        CSpaceAndPaths getTaskCSpaceAndPathsById(long taskId);

        bool loadLibFromPath(string path);
        bool loadLibFromPackage(string package, string lib);
    };
};
