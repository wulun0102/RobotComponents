/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/interface/components/MotionPlanning/Tasks/MotionPlanningTask.ice>

module armarx
{
module rngshortcut
{
    ["cpp:virtual"]
    class TaskBase extends PostprocessingMotionPlanningTaskBase implements MotionPlanningMultiPathWithCostTaskControlInterface
    {
            ["protected"] float dcdStep;
            ["protected"] long maxTries;
            ["protected"] long maxTimeForPostprocessingInSeconds;
            /**
             * @brief Minimal ratio the shortcut has to be shorter than the path replaced by the shortcut.
             *
             * E.g.: If 0.1 a shortcut has to have a length <= 0.9 * length of the improved path segment.
             */
            ["protected"] float minShortcutImprovementRatio = 0.1;
            /**
             * @brief Minimal ratio the improved path has to be shorter than the current path.
             *
             * E.g.: If 0.01 a shortcut has reduce the path length by >= 0.01 * length of the current path.
             */
            ["protected"] float minPathImprovementRatio = 0.01;

    };
};
};
