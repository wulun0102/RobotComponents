/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/interface/components/MotionPlanning/CSpace/CSpace.ice>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/DataStructures.ice>
#include <RobotComponents/interface/components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/ManagerNode.ice>

module armarx
{
module addirrtstar
{
    /**
     * @brief The worker used by addirrt*.
     */
    ["cpp:virtual"]
    class WorkerNodeBase implements TreeUpdateInterface
    {
        /**
         * @brief Requests the worker to kill its working task.
         */
        idempotent void killWorker();

        /**
          * @brief
          */
        idempotent void pauseWorker(bool pauseFlag);

        /**
         * @brief Requests an already send update again.
         */
        ["cpp:const"] idempotent Update getUpdate(long updateId);

        //problem
        /**
         * @brief The used planning cspace.
         */
        CSpaceBase cspace;
        /**
         * @brief The start config.
         */
        VectorXf startCfg;
        /**
         * @brief The goal config.
         */
        VectorXf goalCfg;

        //parameters
        /**
         * @brief The dcd stepsize.
         */
        float DCDStepSize;

        /**
         * @brief The parameters used for add.
         */
        AdaptiveDynamicDomainParameters addParams;

        //management
        /**
         * @brief The workers manager.
         */
        ManagerNodeBase* manager;
        /**
         * @brief The workers id.
         */
        long workerId;
        /**
         * @brief The batch size before an update is send and other updates are integrated.
         */
        long batchSize;

        /**
         * @brief Number of nodes created (by a worker) before a connect to the goal node is tried (by this worker).
         */
        long nodeCountDeltaForGoalConnectionTries;

        /**
         * @brief The update topics prefix
         */
        string topicPrefix;

        /**
         * @brief The precalculated rotation matrix used by the informed sampler.
         */
        Ice::FloatSeq rotationMatrix;
    };
};
};
