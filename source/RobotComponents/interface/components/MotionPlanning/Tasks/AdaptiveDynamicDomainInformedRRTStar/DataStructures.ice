/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>

#include <RobotComponents/interface/components/MotionPlanning/DataStructures.ice>

module armarx
{
module addirrtstar
{
    /**
     * @brief Structure containing the worker's id that created the node and the number of nodes created by this worker.
     */
    struct NodeId
    {
            /**
             * @brief The worker's id that created the node.
             */
            long workerId;
            /**
             * @brief The number of nodes created by this worker.
             */
            long numberOfNode;
    };
    sequence<NodeId> NodeIdList;

    /**
     * @brief Update for the creation of a node.
     */
    struct NodeCreationUpdate
    {
        /**
         * @brief The node's config.
         */
        VectorXf config;
        /**
         * @brief The node's parent.
         */
        NodeId parent;
//        float radius; //on creation the radius always is +inf
        /**
         * @brief The cost from the node's parent to the node.
         */
        float fromParentCost;
//        float fromStartCost; //gets calculated at worker
    };
    sequence<NodeCreationUpdate> NodeCreationUpdateList;

    /**
     * @brief Update for the rewiring of a node.
     */
    struct RewireUpdate
    {
        /**
         * @brief The rewired node.
         */
        NodeId child;
        /**
         * @brief The node's new parent.
         */
        NodeId newParent;
        /**
         * @brief The cost from the node's new parent to the node.
         */
        float fromNewParentCost;
    };
    sequence<RewireUpdate> RewireUpdateList;

    /**
     * @brief Update for a changed radius.
     */
    struct RadiusUpdate
    {
        /**
         * @brief The node affected by this update.
         */
        NodeId node;
        /**
         * @brief Whether the radius increased or decreased.
         */
        bool increase;
    };
    sequence<RadiusUpdate> RadiusUpdateList;

    /**
     * @brief Update for a new goal node.
     */
    struct GoalInfo
    {
        /**
         * @brief The new goal node.
         */
        NodeId node;
        /**
         * @brief The cost from the node to the goal.
         */
        float costToGoToGoal;
    };
    sequence<GoalInfo> GoalInfoList;

    /**
     * @brief Compound update structure containing all updates of a batch of iterations.
     */
    struct Update
    {
        /**
         * @brief The worker's id causing the update.
         */
        long workerId;

        /**
         * @brief The updates of workers prior to this update.
         * These ids are used to apply updares in correct order, detect missing updates and request missing updates again.
         * dependetOnUpdateIds[i] == -1 means no updates from worker i are required
         */
        Ice::LongSeq dependetOnUpdateIds;

        //the node ids for all new nodes are implicit created by the position in the list in combination with the workerId and oldNodeCounts[workerId]
        /**
         * @brief Node creation updates.
         */
        NodeCreationUpdateList nodes;
        /**
         * @brief Updates about rewired nodes.
         */
        RewireUpdateList rewires;
        /**
         * @brief Updates about changed radii.
         */
        RadiusUpdateList radii;
        /**
         * @brief Updates about new goal nodes.
         */
        GoalInfoList newGoalNodes;
    };
    sequence<Update> UpdateList;
    sequence<UpdateList> UpdateListList;

    /**
     * @brief Interface used to transmit tree updates.
     */
    interface TreeUpdateInterface
    {
        /**
         * @brief Queues the update for integration into the tree
         * @param u The update data
         */
        void updateTree(Update u);
    };

    /**
     * @brief All information about a node.
     */
    struct FullNodeData
    {
        /**
         * @brief The node's config.
         */
        VectorXf config;
        /**
         * @brief The node's parent.
         */
        NodeId parent;
        /**
         * @brief The node's radius.
         */
        float radius;
        /**
         * @brief The cost from the node's parent to the node.
         */
        float fromParentCost;
    };
    sequence<FullNodeData> FullNodeDataList;
    sequence<FullNodeDataList> FullNodeDataListList;

    /**
     * @brief Struct containing all nodes of a tree and the update ids of the tree.
     */
    struct FullIceTree
    {
        /**
         * @brief All nodes per worker.
         */
        FullNodeDataListList nodes;
        /**
         * @brief The applyed updates
         */
        Ice::LongSeq updateIds;
    };

    /**
     * @brief Parameters used by adaptive dynamic domain.
     */
    struct AdaptiveDynamicDomainParameters
    {
        /**
         * @brief The value used to increase/decrease the radius of a node.
         *
         * "A small value for ? (usually a few percents) is sufficient to increase the robustness of the algorithm"
         * (doi> 10.1109/IROS.2005.1545607)
         */
        float alpha;
        /**
         * @brief The initial radius for boundary nodes
         *
         * "The gain is particularly significant when the initial value is an
         *   overestimation of the optimal value compared to an under estimation."
         * (doi> 10.1109/IROS.2005.1545607)
         */
        float initialBorderRadius; //20*DCDStepSize

        /**
         * @brief adaptiveDynamicDomainMinimalRadius
         *
         * "To keep the probabilistic completeness of the algorithm we
         *   always ensure the possibility for a node to be extended. To
         *   do so, we put a lower bound on the possible radius values
         *   of the nodes (not shown in the algorithm of Figure 6). This
         *   bound is a multiple of the interpolation step used to check
         *   the collisions when we attempt to expand a node."
         * (doi> 10.1109/IROS.2005.1545607)
         *
         */
        float minimalRadius;//5*DCDStepSize
    };
};
};
