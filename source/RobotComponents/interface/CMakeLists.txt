###
### CMakeLists.txt file for RobotComponents Interfaces
###

# Dependencies on interface libraries to other ArmarX Packages must be specified
# in the following variable separated by whitespaces
set(RobotComponents_INTERFACE_DEPEND ArmarXCore VisionX RobotAPI MemoryX)

# List of slice files to include in the interface library
set(SLICE_FILES
    components/PathPlanner.ice
    components/DMPComponentBase.ice
    components/RobotIK.ice
    components/MMMPlayerInterface.ice    
    components/FunctionApproximatorBase.ice
    components/CollisionCheckerInterface.ice
    components/PlannedMotionProviderInterface.ice
    components/ViconMarkerProviderInterface.ice
    GazeStabilizationInterface.ice
    OptokineticReflexInterface.ice
    #KinematicUnitAndPlatformUnitListenerInterface.ice
    components/GraspingManager/GraspingManagerInterface.ice
    components/GraspingManager/GraspGeneratorInterface.ice
    components/GraspingManager/GraspSelectionManagerInterface.ice
    components/GraspingManager/GraspSelectionCriterionInterface.ice
    components/GraspingManager/RobotPlacementInterface.ice
    #motion planning
        components/MotionPlanning/DataStructures.ice
        components/MotionPlanning/MotionPlanningServer.ice
        #cspace
            components/MotionPlanning/CSpace/CSpace.ice
            components/MotionPlanning/CSpace/ScaledCSpace.ice
            components/MotionPlanning/CSpace/SimoxCSpace.ice
            components/MotionPlanning/CSpace/VoxelGridCSpaceBase.ice
        #resource request strategies
            components/MotionPlanning/ResourceRequestStrategies/ComputingPowerRequestStrategy.ice
        #tasks
            components/MotionPlanning/Tasks/MotionPlanningTask.ice
            components/MotionPlanning/Tasks/CPRSAwareMotionPlanningTask.ice
            components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/DataStructures.ice
            components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/ManagerNode.ice
            components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/WorkerNode.ice
            components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/Task.ice
            components/MotionPlanning/Tasks/RRTConnect/DataStructures.ice
            components/MotionPlanning/Tasks/RRTConnect/WorkerNode.ice
            components/MotionPlanning/Tasks/RRTConnect/Task.ice
            components/MotionPlanning/Tasks/AStar/Task.ice
            components/MotionPlanning/Tasks/BiRRT/Task.ice
            components/MotionPlanning/Tasks/RandomShortcutPostprocessor/Task.ice
            components/MotionPlanning/Tasks/PathCollection/Task.ice
            components/MotionPlanning/Tasks/CSpaceVisualizerTask.ice

    components/LaserScannerSelfLocalisation.ice
    components/DHParameterOptimizationLoggerInterface.ice
    
    components/ObstacleDetection/HumanObstacleDetectionInterface.ice
    components/ObstacleDetection/LaserScannerObstacleDetectionInterface.ice
)

# generate the interface library
armarx_interfaces_generate_library(RobotComponents "${RobotComponents_INTERFACE_DEPEND}")
