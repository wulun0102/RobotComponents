set(LIB_NAME       MMMHelper)
armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

armarx_build_if(MMMCore_FOUND "MMMCORE not available")

set(LIBS
    RobotAPICore
    ArmarXCoreObservers
    MMMCore
    KinematicSensor
    ModelPoseSensor
)

set(LIB_FILES   MotionFileWrapper.cpp)
set(LIB_HEADERS MotionFileWrapper.h)


armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

if (MMMCore_FOUND)
    target_include_directories(MMMPlayer PUBLIC ${MMMCORE_INCLUDE_DIRS})
endif()


