/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2020, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::Libraries::MMM
 * @author     Andre Meixner
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <RobotAPI/libraries/core/Trajectory.h>

#ifndef _ARMARX_COMPONENT_RobotComponents_MotionFileWrapper_H
#define _ARMARX_COMPONENT_RobotComponents_MotionFileWrapper_H

namespace armarx
{
    class MotionData
    {
    public:
        MotionData() : scaling(1.0)
        {
        }

        void setScaling(float value) {
            double factor = value / scaling;
            for (auto &data : poseTrajData) {
                data.second[0] *= factor;
                data.second[1] *= factor;
                data.second[2] *= factor;
            }
            scaling = value;
        }

        ::Ice::StringSeq jointNames;
        DoubleSeqSeqSeq jointTrajData;
        ::Ice::DoubleSeq timestamp;
        std::map<double, Ice::DoubleSeq> poseTrajData;

        TrajectoryBasePtr getPoseTrajectory() {
            return TrajectoryBasePtr(new Trajectory(poseTrajData));
        }

        TrajectoryBasePtr getJointTrajectory() {
            return TrajectoryBasePtr(new Trajectory(jointTrajData, timestamp, jointNames));
        }

        std::string modelPath;
        unsigned int numberOfFrames;
        float scaling;
    };

    typedef std::shared_ptr<MotionData> MotionDataPtr;

    class MotionFileWrapper;

    typedef std::shared_ptr<MotionFileWrapper> MotionFileWrapperPtr;

    class MotionFileWrapper
    {
    public:
        static MotionFileWrapperPtr create(const std::string& motionFilePath, double butterworthFilterCutOffFreq = 0.0, const std::string relativeModelRoot = "mmm");

        MotionDataPtr getFirstMotionData();
        MotionDataPtr getMotionDataByModel(const std::string& modelName);
        MotionDataPtr getMotionData(const std::string& motionName);

        ::Ice::StringSeq getModelNames();
        ::Ice::StringSeq getMotionNames();

    private:
        MotionFileWrapper(int butterworthFilterCutOffFreq = 0);

        bool loadMotion(const std::string& motionFilePath, const std::string relativeModelRoot = "mmm");
        bool loadLegacyMotion(const std::string& motionFilePath, const std::string relativeModelRoot = "mmm");

        ::Ice::DoubleSeq getTrajData(const Eigen::Matrix4f& rootPose);
        DoubleSeqSeq getTrajData(const Eigen::VectorXf& jointAngles);

        std::map<std::string, MotionDataPtr> motionData;
        std::vector<std::string> motionNames;
        double butterworthFilterCutOffFreq;
    };
}

#endif // _ARMARX_COMPONENT_RobotComponents_MotionFileWrapper_H
