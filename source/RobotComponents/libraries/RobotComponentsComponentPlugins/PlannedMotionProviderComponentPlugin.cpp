#include "PlannedMotionProviderComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void PlannedMotionProviderComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void PlannedMotionProviderComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_plannedMotionProvider, PROPERTY_NAME);
        }

        void PlannedMotionProviderComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "PlannedMotionProvider",
                    "Name of the PlannedMotionProvider");
            }
        }

        PlannedMotionProviderInterfacePrx PlannedMotionProviderComponentPlugin::getPlannedMotionProvider()
        {
            return _plannedMotionProvider;
        }


    }
}

namespace armarx
{

    PlannedMotionProviderComponentPluginUser::PlannedMotionProviderComponentPluginUser()
    {
        addPlugin(plugin);
    }

    PlannedMotionProviderInterfacePrx PlannedMotionProviderComponentPluginUser::getPlannedMotionProvider()
    {
        return plugin->getPlannedMotionProvider();
    }


}