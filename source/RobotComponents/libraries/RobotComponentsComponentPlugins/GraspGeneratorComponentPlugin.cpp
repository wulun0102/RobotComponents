#include "GraspGeneratorComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void GraspGeneratorComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void GraspGeneratorComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_graspGenerator, PROPERTY_NAME);
        }

        void GraspGeneratorComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "GraspGenerator",
                    "Name of the GraspGenerator");
            }
        }

        GraspGeneratorInterfacePrx GraspGeneratorComponentPlugin::getGraspGenerator()
        {
            return _graspGenerator;
        }


    }
}

namespace armarx
{

    GraspGeneratorComponentPluginUser::GraspGeneratorComponentPluginUser()
    {
        addPlugin(plugin);
    }

    GraspGeneratorInterfacePrx GraspGeneratorComponentPluginUser::getGraspGenerator()
    {
        return plugin->getGraspGenerator();
    }


}