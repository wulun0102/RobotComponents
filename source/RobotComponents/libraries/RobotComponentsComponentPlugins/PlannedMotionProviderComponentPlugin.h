#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotComponents/interface/components/PlannedMotionProviderInterface.h>


namespace armarx
{
    namespace plugins
    {

        class PlannedMotionProviderComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            PlannedMotionProviderInterfacePrx getPlannedMotionProvider();

        private:
            static constexpr const char* PROPERTY_NAME = "PlannedMotionProviderName";
            PlannedMotionProviderInterfacePrx _plannedMotionProvider;
        };

    }
}

namespace armarx
{
    /**
     * @brief Provides a ready-to-use PlannedMotionProvider.
     */
    class PlannedMotionProviderComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        PlannedMotionProviderComponentPluginUser();
        PlannedMotionProviderInterfacePrx getPlannedMotionProvider();

    private:
        armarx::plugins::PlannedMotionProviderComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using PlannedMotionProviderComponentPluginUser = armarx::PlannedMotionProviderComponentPluginUser;
    }
}
