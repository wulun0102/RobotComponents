#include "GraspSelectionManagerComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void GraspSelectionManagerComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void GraspSelectionManagerComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_graspSelectionManager, PROPERTY_NAME);
        }

        void GraspSelectionManagerComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "GraspSelectionManager",
                    "Name of the GraspSelectionManager");
            }
        }

        GraspSelectionManagerInterfacePrx GraspSelectionManagerComponentPlugin::getGraspSelectionManager()
        {
            return _graspSelectionManager;
        }


    }
}

namespace armarx
{

    GraspSelectionManagerComponentPluginUser::GraspSelectionManagerComponentPluginUser()
    {
        addPlugin(plugin);
    }

    GraspSelectionManagerInterfacePrx GraspSelectionManagerComponentPluginUser::getGraspSelectionManager()
    {
        return plugin->getGraspSelectionManager();
    }


}