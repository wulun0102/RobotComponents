#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotComponents/interface/components/GraspingManager/RobotPlacementInterface.h>


namespace armarx
{
    namespace plugins
    {

        class RobotPlacementComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            RobotPlacementInterfacePrx getRobotPlacement();

        private:
            static constexpr const char* PROPERTY_NAME = "RobotPlacementName";
            RobotPlacementInterfacePrx _robotPlacement;
        };

    }
}

namespace armarx
{
    /**
     * @brief Provides a ready-to-use RobotPlacement.
     */
    class RobotPlacementComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        RobotPlacementComponentPluginUser();
        RobotPlacementInterfacePrx getRobotPlacement();

    private:
        armarx::plugins::RobotPlacementComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using RobotPlacementComponentPluginUser = armarx::RobotPlacementComponentPluginUser;
    }
}
