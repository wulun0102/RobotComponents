#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotComponents/interface/components/GraspingManager/GraspSelectionManagerInterface.h>


namespace armarx
{
    namespace plugins
    {

        class GraspSelectionManagerComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            GraspSelectionManagerInterfacePrx getGraspSelectionManager();

        private:
            static constexpr const char* PROPERTY_NAME = "GraspSelectionManagerName";
            GraspSelectionManagerInterfacePrx _graspSelectionManager;
        };

    }
}

namespace armarx
{
    /**
     * @brief Provides a ready-to-use GraspSelectionManager.
     */
    class GraspSelectionManagerComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        GraspSelectionManagerComponentPluginUser();
        GraspSelectionManagerInterfacePrx getGraspSelectionManager();

    private:
        armarx::plugins::GraspSelectionManagerComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using GraspSelectionManagerComponentPluginUser = armarx::GraspSelectionManagerComponentPluginUser;
    }
}
