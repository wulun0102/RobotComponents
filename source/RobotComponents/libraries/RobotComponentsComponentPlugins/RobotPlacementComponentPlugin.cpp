#include "RobotPlacementComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void RobotPlacementComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void RobotPlacementComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_robotPlacement, PROPERTY_NAME);
        }

        void RobotPlacementComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "RobotPlacement",
                    "Name of the RobotPlacement");
            }
        }

        RobotPlacementInterfacePrx RobotPlacementComponentPlugin::getRobotPlacement()
        {
            return _robotPlacement;
        }


    }
}

namespace armarx
{

    RobotPlacementComponentPluginUser::RobotPlacementComponentPluginUser()
    {
        addPlugin(plugin);
    }

    RobotPlacementInterfacePrx RobotPlacementComponentPluginUser::getRobotPlacement()
    {
        return plugin->getRobotPlacement();
    }


}


