#!/usr/bin/env python

from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split

import pickle, sys,os
import numpy as np

import time
import logging
from armarx.interface_helper import load_armarx_slice, ice_helper
from armarx.interface_helper import get_proxy, register_object, using_topic

load_armarx_slice('VisionX', 'components/OpenPoseEstimationInterface.ice')
from armarx import OpenPose2DListener


logger = logging.getLogger(__name__)


class PoseBasedActionRecognizerImpl(OpenPose2DListener):

    def __init__(self, label, existingDataPath):
        super(OpenPose2DListener, self).__init__()
        self.label = label
        self.existingDataPath = existingDataPath
        if len(self.existingDataPath) > 0 and os.path.exists(self.existingDataPath):
            self.recorded_data = pickle.load(open(self.existingDataPath, "rb"))
            print("found keys: " + str(self.recorded_data.keys()))
        else:
            self.recorded_data = {}
        self.clf = None


    def report2DKeypoints(self, data, timestamp, current=None):
        logger.info('called for ' + str(len(data)) + " person")
        if len(data) != 1:
            return
        positions = []
        for key in data[0]:
            keypoint = data[0][key]
            positions.append([keypoint.x, keypoint.y])
        if self.label not in self.recorded_data or self.recorded_data[self.label] is None:
            self.recorded_data[self.label] = [positions]
        else:
            self.recorded_data[self.label].append(positions)

    def train(self):
        self.clf = SVC()
        self.clf = MLPClassifier(alpha=1, max_iter=1000, hidden_layer_sizes=(200), verbose=True)
        X = []
        y = []
        for key in self.recorded_data:
            for pose in self.recorded_data[key]:
                np_positions = np.array(pose)
                average = np.average(np_positions, 0)
                average_positions = np_positions - average[None, :]
                #print(average, np_positions, average_positions)

                average_positions = np.reshape(average_positions, (36,))
                X.append(average_positions)
                y.append(key)
        print("Loaded " + str(len(X)) + " pose samples")
        X = np.array(X)
        X_train, X_test, y_train, y_test = train_test_split(
                    X, y, test_size = 0.2, random_state = int(time.time()))

        self.clf.fit(X_train,y_train)
        print(self.clf.score(X_train, y_train))
        print(self.clf.score(X_test, y_test))
        for i in range(len(X_test)):
            x = X_test[i].reshape(1, -1)
            prediction = self.clf.predict(x)
            if(prediction[0] != y_test[i]):
                print(str(y_test[i]) + " <-> " + str(prediction[0]))


    def dumpToFile(self, path):
        print("Dumping to " + path)
        pickle.dump(self.recorded_data, open( path, "wb" ) )

def main():
    label = sys.argv[2] if len(sys.argv) > 2 else ""
    path = sys.argv[3] if len(sys.argv) > 3 else ""
    ice_object = PoseBasedActionRecognizerImpl(label, path)
    if sys.argv[1] != "train":
        proxy = register_object(ice_object, 'PoseBasedActionRecognizer')
        topic = using_topic(proxy, 'OpenPoseEstimation2D')

    if sys.argv[1] == "train":
        ice_object.train()
    try:
        while not ice_helper.iceCommunicator.isShutdown():
            logger.debug('still alive')
            time.sleep(1)
    except KeyboardInterrupt:
            logger.info('shutting down')
    if sys.argv[1] != "train":
        topic.unsubscribe(proxy)

    if sys.argv[1] == "record":
        i = 0
        prefix = "trainingdata/" + ice_object.label
        path =  prefix + str(i) + ".pickle"
        while os.path.exists(path):
            i+=1
            path = prefix + str(i) + ".pickle"
        print("Saving to " + path)
        ice_object.dumpToFile(path)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()