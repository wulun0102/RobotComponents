#!/usr/bin/env python

from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split

import pickle, sys,os
import numpy as np

import time
import logging
import argparse
from armarx.interface_helper import load_armarx_slice, ice_helper
from armarx.interface_helper import get_proxy, register_object, using_topic, get_topic

load_armarx_slice('VisionX', 'components/OpenPoseEstimationInterface.ice')
from armarx import OpenPose2DListener, OpenPoseCombinedListener
from armarx import OpenPose3DListener
from armarx import PoseBasedActionRecognitionListenerPrx
from armarx import PoseBasedActionRecognitionInterface
from armarx import ActionRecognitionResult
logger = logging.getLogger(__name__)


# class which offers an interface to armarx
class PoseBasedActionRecognitionControlImpl(PoseBasedActionRecognitionInterface):
    def __init__(self):
        self.enabled = False
        self.timestamp = 0
        self.label = " "

    def startRecognition(self, current=None):
        self.enabled = True
        logger.info('started component')

    def stopRecognition(self, current=None):
        self.enabled = False
        logger.info('stopped component')

    def setLatestRecognition(self, timestamp, label, current=None):
        self.enabled = True
        self.timestamp = timestamp
        self.label = label
        print("Label: ", label, " timestamp: ", timestamp)
		

    def getLatestRecognition(self, current=None):
        result = ActionRecognitionResult()
        result.label = self.label
        result.timestamp = self.timestamp
        return result


# class for training, testing and classification of the pose tracking
class PoseBasedActionRecognizerImpl(OpenPoseCombinedListener):
    def __init__(self, label, existingDataPath ):
        super(OpenPose2DListener, self).__init__()
        self.label = label
        self.existingDataPath = existingDataPath
        if len(self.existingDataPath) > 0 and os.path.exists(self.existingDataPath):
            self.recorded_data = pickle.load(open(self.existingDataPath, "rb"))
            print("found keys: " + str(self.recorded_data.keys()))
        else:
            self.recorded_data = {}
        self.clf = None
        self.positions = []
        self.classification_proxy = get_topic(PoseBasedActionRecognitionListenerPrx, 'PoseClassification')
        self.p = PoseBasedActionRecognitionControlImpl()
        self.proxy = register_object(self.p, 'PoseBasedActionRecognition')

    # incoming 2D points of component: OpenPoseEstimation
    def report2DKeypoints(self, data, timestamp, current=None):
        return
        if len(data) != 1:
            print("got multiple or zero persons")
            return		
        positions = []
        print("Got new human posedata")
        for key in data[0]:
            keypoint = data[0][key]
            positions.append([keypoint.x, keypoint.y])

        if args.classify:
            averagePositions = self.averagePosition(positions)
            prediction = self.clf.predict(averagePositions.reshape(1, -1))
            self.classification_proxy.reportPoseClassified(str(prediction))
            self.p.setLatestRecognition(timestamp, prediction[0])

        else:
            if self.label not in self.recorded_data or self.recorded_data[self.label] is None:
                self.recorded_data[self.label] = [positions]
            else:
                self.recorded_data[self.label].append(positions)
		

    def report2DKeypointsNormalized(self, data, timestamp, current=None):
        return
    
    def report3DKeypoints(self, data, timestamp, current=None):
        return

    def averagePosition(self, pose):
        np_positions = np.array(pose)
        average = np.average(np_positions, 0)
        average_positions = np_positions - average[None, :]
        average_positions = np.reshape(average_positions, (36,))

        return average_positions

    # train and tests the given data
    def train(self):
        # all training data has to be in this folder
        path = './trainingdata/'
        print (path)
        self.clf = SVC()
        self.clf = MLPClassifier(max_iter=1000, hidden_layer_sizes=(100,100), verbose=True)
        X = []
        y = []

        # three possible labels: guardsupport, idle (random) motions, requestobject
        for fileName in sorted(os.listdir(path)):
            print (fileName)
            if fileName.startswith('g'):
                label = 'guardsupport'
                print (label)
            elif fileName.startswith('r'):
                label = 'requestobject'
                print (label)
            elif fileName.startswith('i'):
                label = 'idleMotion'
                print (label)
            else:
                print ("Wrong file in folder")

            with open(path + fileName, "rb") as f:
                data = pickle.load(f)

            for key in data:
                for pose in data[key]:
                    average_positions = self.averagePosition(pose)
                    X.append(average_positions)
                    y.append(label)


        print("Loaded " + str(len(X)) + " pose samples")
        X = np.array(X)
        X_train = X
        y_train = y

        X_test = []
        y_test = []

        # all testing data has to be in this folder
        pathTest = './testdata/'
        for fileName in sorted(os.listdir(pathTest)):
            print (fileName)
            if fileName.startswith('g'):
                label = 'guardsupport'
                print (label)
            elif fileName.startswith('r'):
                label = 'requestobject'
                print (label)
            elif fileName.startswith('i'):
                label = 'idleMotion'
                print (label)
            else:
                print ("Wrong file in folder")

            data_test = pickle.load(open(pathTest + fileName, 'rb'))
            for key in data_test:
                for pose in data_test[key]:
                    average_positions = self.averagePosition(pose)
                    X_test.append(average_positions)
                    y_test.append(label)

        self.clf.fit(X_train, y_train)
        print(self.clf.score(X_train, y_train))
        print(self.clf.score(X_test, y_test))
        pickle.dump(self.clf, open('trained_clf.pickle', "wb"))

        # calculation of accuracy
        number_wrong = 0
        number_right = 0
        for i in range(len(X_test)):
            x = X_test[i].reshape(1, -1)
            prediction = self.clf.predict(x)
            if (prediction[0] != y_test[i]):
                print(str(y_test[i]) + " <-> " + str(prediction[0]))
                number_wrong +=1
            else:
                number_right +=1

        accuracy = number_right / float(number_right+number_wrong) * 100
        print (accuracy)

        print ("Finished Training")

    # load the file with the training data
    def loadClassifier(self):       
        self.clf = pickle.load(open('trained_clf.pickle', 'rb'))

    # saves the new recorded data into the given path
    def dumpToFile(self, path):
        print("Dumping to " + path)
        pickle.dump(self.recorded_data, open( path, "wb" ) )


def parseArgs(args):
    """ Parse command-line arguments. """
    parser = argparse.ArgumentParser()
    # add relevant arguments to the parser
    parser.add_argument(
        '-t',
        '--train',
        action='store_true',
        help='Trains the given data.'
    )

    parser.add_argument(
        '-r',
        '--record',
        action='store_true',
        help='Records the current data stream.'
    )

    parser.add_argument(
        '-p',
        '--path',
        #default="",
        help='Folder where all the training data lies.'
    )

    parser.add_argument(
        '-l',
        '--label',
        #default="",
        help='Label of new motion type.'
    )

    parser.add_argument(
        '-c',
        '--classify',
        action = 'store_true',
        help='Classify new incoming data.'
    )

    return parser.parse_args(args)
    
def main():
    if args.classify:
        label = ""
        path = ""
        ice_object = PoseBasedActionRecognizerImpl(label, path)
        ice_object.loadClassifier()
        proxy = register_object(ice_object, 'PoseBasedActionRecognizerListener')
        topic = using_topic(proxy, 'OpenPoseEstimation2D')
        topic_3D = using_topic(proxy, 'OpenPoseEstimation3D')

        try:
            while not ice_helper.iceCommunicator.isShutdown():
                logger.debug('still alive')
                time.sleep(1)
        except KeyboardInterrupt:
            logger.info('shutting down')
        if not args.train:
            topic.unsubscribe(proxy)
            topic_3D.unsubscribe(proxy)

    if args.train:
        print("Training Modus")
        label = ""
        path = ""
        ice_object = PoseBasedActionRecognizerImpl(label, path)
        ice_object.train()

    if args.record:
        if args.label:
            label = args.label
        else:
            print("You need to set a label.")
            sys.exit()
        if args.path:
            path = args.path
        else:
            print("You need to set a data path.")
            sys.exit()
        ice_object = PoseBasedActionRecognizerImpl(label, path)
        proxy = register_object(ice_object, 'PoseBasedActionRecognizerListener')
        topic = using_topic(proxy, 'OpenPoseEstimation2D')

        try:
            while not ice_helper.iceCommunicator.isShutdown():
                logger.debug('still alive')
                time.sleep(1)
        except KeyboardInterrupt:
            logger.info('shutting down')
        if not args.train:
            topic.unsubscribe(proxy)

        print("Recording Modus")
        i = 0
        prefix = "trainingdata_new/" + ice_object.label
        path =  prefix + str(i) + ".pickle"
        while os.path.exists(path):
            i+=1
            path = prefix + str(i) + ".pickle"
        print("Saving to " + path)
        ice_object.dumpToFile(path)

if __name__ == '__main__':
    args = parseArgs(sys.argv[1:])
    logging.basicConfig(level=logging.INFO)
    main()
